#!/bin/bash
#

# run from directory where this script is
cd `echo $0 | sed 's/\(.*\)\/.*/\1/'` # extract pathname
EXAMPLE_DIR=`pwd`

# check whether ECHO has the -e option
if test "`echo -e`" = "-e" ; then ECHO=echo ; else ECHO="echo -e" ; fi
                                                                                
$ECHO
$ECHO "$EXAMPLE_DIR : starting"
$ECHO
$ECHO "This example shows how to use pw.x and acfdt.x to"
$ECHO "compute the RPA + exchange kernel from LDA/GGA orbitals"
$ECHO "produced in a scf run."
$ECHO "This example is for a H2 molecule at equilibrium"

# set the needed environment variables
. ../../../environment_variables

# required executables and pseudopotentials
PSEUDO_DIR="../pseudo/"
BIN_LIST="pw.x exx.x acfdt.x"
PSEUDO_LIST="H.pbe-mt_fhi.UPF"

$ECHO
$ECHO "  executables directory: $BIN_DIR"
$ECHO "  pseudo directory:      $PSEUDO_DIR"
$ECHO "  temporary directory:   $TMP_DIR"
$ECHO "  checking that needed directories and files exist...\c"

for DIR in "$TMP_DIR" "$EXAMPLE_DIR/results" ; do
    if test ! -d $DIR ; then
        mkdir $DIR
    fi
done
cd $EXAMPLE_DIR/results

# check for directories
for DIR in "$BIN_DIR" "$PSEUDO_DIR" ; do
    if test ! -d $DIR ; then
        $ECHO
        $ECHO "ERROR: $DIR not existent or not a directory"
        $ECHO "Aborting"
        exit 1
    fi
done
 
# check for pseudopotentials
for FILE in $PSEUDO_LIST ; do
    if test ! -r $PSEUDO_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $PSEUDO_DIR/$FILE not existent or not readable"
        $ECHO "Aborting"
        exit 1
    fi
done
$ECHO " done"

# check for executables
for FILE in $BIN_LIST ; do
    if test ! -x $BIN_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $BIN_DIR/$FILE not existent or not executable"
        $ECHO "Aborting"
        exit 1
    fi
done

# how to run executables
PW_COMMAND="$PARA_PREFIX $BIN_DIR/pw.x $PARA_POSTFIX"
EXX_COMMAND="$PARA_PREFIX $BIN_DIR/exx.x $PARA_POSTFIX"
ACFDT_COMMAND="$PARA_PREFIX $BIN_DIR/acfdt.x $PARA_IMAGE_POSTFIX"
$ECHO
$ECHO "  running pw.x as:    $PW_COMMAND"
$ECHO "  running exx.x as:   $EXX_COMMAND"
$ECHO "  running acfdt.x as: $ACFDT_COMMAND"
$ECHO

# clean TMP_DIR
$ECHO "  cleaning $TMP_DIR ... \c"
rm -rf $TMP_DIR/*
$ECHO "  DONE"

dd=0.76  # Equilibrium distance
pos=$(echo "scale=4; $dd/sqrt(3)/2" | bc -l)

# self-consistent calculation for H2
cat > H2.scf.in << EOF
&CONTROL
  calculation = "scf",
  pseudo_dir  = "$PSEUDO_DIR",
  outdir      = "$TMP_DIR",
  prefix='H2'
  lecrpa = .true.,
  tstress = .false.,
  tprnfor = .false.,
  wf_collect = .true.,
/
&SYSTEM
  ibrav=  1,
  celldm(1) =15,
  nat=  2, 
  ntyp= 1,
  ecutwfc = 35,
  nbnd = 8,
/
&ELECTRONS
  diagonalization='david'
  mixing_mode = 'plain'
  mixing_beta = 0.7
  conv_thr =  0.5d-12
/
ATOMIC_SPECIES
  H  1.0079  H.pbe-mt_fhi.UPF
ATOMIC_POSITIONS {angstrom}
  H -$pos -$pos -$pos 
  H  $pos  $pos  $pos
K_POINTS {automatic}
  1 1 1 0 0 0
EOF
$ECHO "  running the scf calculation for H2 ...\c"
$PW_COMMAND < H2.scf.in > H2.scf.out
$ECHO " done"

# compute exact-exchange energy  
cat > H2.exx.in << EOF
EXX of H2 
 &inputexx
  prefix = 'H2'
  outdir = '$TMP_DIR',
  nq1   = 1,
  nq2   = 1,
  nq3   = 1,
  exx_fraction = 1.d0
  exxdiv_treatment   = 'g-b'
  x_gamma_extrapolation = .TRUE.
 /
EOF
$ECHO "  running exx.x to compute exact-exchange energy of H2 ...\c"
$EXX_COMMAND < H2.exx.in > H2.exx.out
$ECHO " done"


# compute RPA correlation energy
cat > H2.acfdt.in << EOF
EC-RPA of H2 
 &inputec
  tr2_ph=1.0d-20,
  thr_ec = 1.d-8,
  prefix='H2'
  outdir='$TMP_DIR',
  oep_method = 0
  idiag = 3,
  iumax = 10,
  nf_ec = 10,
  xqq(1)= 0.0, xqq(2)= 0.0, xqq(3)= 0.0, wq=1.00
  u0_ec = 30,
  neigv = 20,
  !
  rpax=.true.
  thr_oep = 3.d-6
  max_oep_space = 20  
 /
EOF
$ECHO "  running acfdt.x to compute RPAx correlation energy for H2 ...\c"
$ACFDT_COMMAND < H2.acfdt.in > H2.acfdt.out
$ECHO " done"
