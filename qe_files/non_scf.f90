!
! Copyright (C) 2001-2013 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!  
!
!-----------------------------------------------------------------------
  SUBROUTINE non_scf ( )
  !-----------------------------------------------------------------------
  !
  ! ... diagonalization of the KS hamiltonian in the non-scf case
  !
  USE kinds,                ONLY : DP
  USE bp,                   ONLY : lelfield, lberry, lorbm
  USE check_stop,           ONLY : stopped_by_user
  USE control_flags,        ONLY : io_level, conv_elec, lbands
  USE ener,                 ONLY : ef
  USE io_global,            ONLY : stdout, ionode
  USE io_files,             ONLY : iunwfc, nwordwfc, iunefield
  USE buffers,              ONLY : save_buffer
  USE klist,                ONLY : xk, wk, nks, nkstot
  USE lsda_mod,             ONLY : lsda, nspin
  USE wvfct,                ONLY : nbnd, et, npwx
  USE wavefunctions_module, ONLY : evc
  USE acfdt_ener,           ONLY : acfdt_in_pw
  !
  IMPLICIT NONE
  !
  ! ... local variables
  !
  INTEGER :: iter, i
  REAL(DP), EXTERNAL :: get_clock
  !
  !
  CALL start_clock( 'electrons' )
  iter = 1
  !
  WRITE( stdout, 9002 )
  FLUSH( stdout )
  !
  IF ( lelfield) THEN
     !
     CALL c_bands_efield ( iter )
     !
  ELSE
     !
     CALL c_bands_nscf ( )
     !
  END IF
  !
  ! ... check if calculation was stopped in c_bands
  !
  IF ( stopped_by_user ) THEN
     conv_elec=.FALSE.
     RETURN
  END IF
  !
  ! ... xk, wk, isk, et, wg are distributed across pools;
  ! ... the first node has a complete copy of xk, wk, isk,
  ! ... while eigenvalues et and weights wg must be
  ! ... explicitly collected to the first node
  ! ... this is done here for et, in weights () for wg
  !
  CALL poolrecover( et, nbnd, nkstot, nks )
  !
  ! ... calculate weights of Kohn-Sham orbitals (only weights, not Ef,
  ! ... for a "bands" calculation where Ef is read from data file)
  ! ... may be needed in further calculations such as phonon
  !
  IF ( lbands ) THEN
     CALL weights_only  ( )
  ELSE
     CALL weights  ( )
  END IF
  !
  ! ... Here is ACFDT working
  !
  IF( acfdt_in_pw ) THEN
    !
    CALL sum_band()
    !
    CALL print_acfdt_pw()
    !
  ENDIF 
  !  
  ! .....................
  !
  ! ... Note that if you want to use more k-points for the phonon
  ! ... calculation then those needed for self-consistency, you can,
  ! ... by performing a scf with less k-points, followed by a non-scf
  ! ... one with additional k-points, whose weight on input is set to zero
  !
  WRITE( stdout, 9000 ) get_clock( 'PWSCF' )
  !
  WRITE( stdout, 9102 )
  !
  ! ... write band eigenvalues (conv_elec is used in print_ks_energies)
  !
  conv_elec = .true.
  CALL print_ks_energies ( ) 
  !
  ! ... save converged wfc if they have not been written previously
  ! ... FIXME: it shouldn't be necessary to do this here
  !
  IF ( nks == 1 .AND. (io_level < 2) .AND. (io_level > -1) ) &
        CALL save_buffer ( evc, nwordwfc, iunwfc, nks )
  !
  ! ... do a Berry phase polarization calculation if required
  !
  IF ( lberry ) CALL c_phase()
  !
  ! ... do an orbital magnetization (Kubo terms) calculation
  !
  IF ( lorbm ) CALL orbm_kubo()
  !
  CALL stop_clock( 'electrons' )
  !
9000 FORMAT(/'     total cpu time spent up to now is ',F10.1,' secs' )
9002 FORMAT(/'     Band Structure Calculation' )
9102 FORMAT(/'     End of band structure calculation' )
  !
END SUBROUTINE non_scf
!
!----------------------------------------------------------------------------
SUBROUTINE print_acfdt_pw()
  !----------------------------------------------------------------------------
  !
  ! ... This routine is a driver of the non-self-consistent.
  ! ... It continues by the routine c_bands_nscf for computing the bands 
  ! ... Then, it recalculate some energies with new wavefucntions.
  !
  USE kinds,                ONLY : DP
  USE constants,            ONLY : eps8, pi
  USE io_global,            ONLY : stdout, ionode
  USE cell_base,            ONLY : at, bg, alat, omega, tpiba2
  USE ions_base,            ONLY : zv, nat, nsp, ityp, tau, compute_eextfor
  USE basis,                ONLY : starting_pot
  USE bp,                   ONLY : lelfield
  USE fft_base,             ONLY : dfftp, dffts
  USE gvect,                ONLY : ngm, gstart, nl, nlm, g, gg, gcutm
  USE gvecs,                ONLY : doublegrid, ngms
  USE gvecw,                ONLY : ecutwfc
  USE klist,                ONLY : xk, wk, nelec, ngk, nks, nkstot, lgauss
  USE lsda_mod,             ONLY : lsda, nspin, magtot, absmag, isk
  USE vlocal,               ONLY : strf
  USE wvfct,                ONLY : nbnd, et, npwx
  USE ener,                 ONLY : etot, hwf_energy, eband, deband, ehart, &
                                   vtxc, etxc, etxcc, ewld, demet, epaw, &
                                   elondon
  USE scf,                  ONLY : scf_type, scf_type_COPY, &
                                   create_scf_type, destroy_scf_type, &
                                   rho, rho_core, rhog_core, &
                                   v, vltot, vrs, kedtau, vnew
  USE control_flags,        ONLY : mixing_beta, tr2, ethr, niter, nmix, &
                                   iprint, istep, lscf, lmd, conv_elec, &
                                   restart, io_level, do_makov_payne,  &
                                   gamma_only, iverbosity, textfor,     &
                                   llondon
  USE io_files,             ONLY : iunwfc, nwordwfc, output_drho, &
                                   iunefield 
  USE buffers,              ONLY : save_buffer
  USE ldaU,                 ONLY : eth, Hubbard_U, Hubbard_lmax, &
                                   niter_with_fixed_ns, lda_plus_u
  USE extfield,             ONLY : tefield, etotefield
  USE wavefunctions_module, ONLY : evc, psic
  USE noncollin_module,     ONLY : noncolin, magtot_nc, i_cons,  bfield, &
                                   lambda, report
  USE spin_orb,             ONLY : domag
  USE uspp,                 ONLY : okvan
!#if defined (EXX)
  USE exx,                  ONLY : exxinit, exxenergy2, &
                                   fock0, fock1, fock2, dexx
  USE funct,                ONLY : dft_is_hybrid, exx_is_active
  USE control_flags,        ONLY : adapt_thr, tr2_init, &
                                  &tr2_multi
!#endif
  USE funct,                ONLY : dft_is_meta
  USE mp_global,            ONLY : intra_pool_comm, npool
  USE mp,                   ONLY : mp_sum
  !
  USE london_module,        ONLY : energy_london
  !
  USE paw_variables,        ONLY : okpaw, ddd_paw, total_core_energy, only_paw
  USE paw_onecenter,        ONLY : PAW_potential
  USE paw_symmetry,         ONLY : PAW_symmetrize_ddd
  USE uspp_param,           ONLY : nh, nhm ! used for PAW
  USE dfunct,               ONLY : newd
  USE esm,                  ONLY : do_comp_esm, esm_printpot
  !
  USE acfdt_ener,           ONLY : acfdt_eband
  !
  IMPLICIT NONE
  !
  ! ... a few local variables
  !
  REAL(DP) :: &
      dr2,          &! the norm of the diffence between potential
      charge,       &! the total charge
      deband_hwf,   &! deband for the Harris-Weinert-Foulkes functional
      mag            ! local magnetization
  INTEGER :: &
      is,           &! counter on polarization
      idum,         &! dummy counter on iterations
      iter,         &! counter on iterations
      ik_,          &! used to read ik from restart file
      kilobytes
  REAL(DP) :: &
      tr2_min,     &! estimated error on energy coming from diagonalization
      descf,       &! correction for variational energy
      en_el=0.0_DP  ! electric field contribution to the total energy
  LOGICAL :: &
      first
  !
  ! ... auxiliary variables for calculating and storing temporary copies of
  ! ... the charge density and of the HXC-potential
  !
  type (scf_type), save :: rhoin ! used to store rho_in of current/next iteration
  !
  ! ... external functions
  REAL(DP), EXTERNAL :: ewald, get_clock
  !
  REAL(DP) :: vltot_tmp(dfftp%nnr,nspin)
  !
  DO is = 1, nspin
     vltot_tmp(:,is) = vltot(:) 
  ENDDO
  !
  ewld = ewald( alat, nat, nsp, ityp, zv, at, bg, tau, &
                omega, g, gg, ngm, gcutm, gstart, gamma_only, strf )
  !
  CALL v_of_rho( rho, rho_core, rhog_core, &
                  ehart, etxc, vtxc, eth, etotefield, charge, v)
  !
  ! ... estimate correction needed to have variational energy:
  ! ... T + E_ion (eband + deband) are calculated in sum_band
  ! ... and delta_e using the output charge density rho;
  ! ... E_H (ehart) and E_xc (etxc) are calculated in v_of_rho
  ! ... above, using the mixed charge density rhoin%of_r.
  ! ... delta_escf corrects for this difference at first order
  !
  deband = delta_e() 
  !
  ! ... total energy include E_xc energy (if any)
  !
  etot = eband + ( etxc - etxcc ) + ewld + ehart + deband !+ demet + descf +en_el
  !
  ! ... total energy without E_xc energy 
  !
  acfdt_eband = eband + deband + ehart + ewld
  !
  ! ... print out the components energy
  !
  WRITE( stdout, 9060 ) eband, deband, ehart, ewld, etxc
  WRITE( stdout, 9080 ) etot, acfdt_eband
  !
  FLUSH(stdout)
  !
  RETURN
  !
  ! ... formats
  !
9000 FORMAT(/'     total cpu time spent up to now is ',F10.1,' secs' )
9060 FORMAT(/'     The total energy is the sum of the following terms:',/,&
            /'     sum of ks eigenvalues     =',F17.8,' Ry' &
            /'     sum of local potentials   =',F17.8,' Ry' &
            /'     hartree contribution      =',F17.8,' Ry' &
            /'     ewald contribution        =',F17.8,' Ry' &
            /'     lda (pbe) xc contribution =',F17.8,' Ry' )

9080 FORMAT(/'     Etot with lda(pbe) xc     =',0PF17.8,' Ry' &
            /'     Etot with lda without xc  =',0PF17.8,' Ry' )
  !
  CONTAINS
     !
     !-----------------------------------------------------------------------
     FUNCTION delta_e()
       !-----------------------------------------------------------------------
       ! ... delta_e = - \int rho%of_r(r)  v%of_r(r)
       !               - \int rho%kin_r(r) v%kin_r(r) [for Meta-GGA]
       !               - \sum rho%ns       v%ns       [for LDA+U]
       !               - \sum becsum       D1_Hxc     [for PAW]
       IMPLICIT NONE
       REAL(DP) :: delta_e, delta_e_hub
       !
       delta_e = - SUM( rho%of_r(:,:)*(vrs(:,:)-vltot_tmp(:,:)) )
       !
       IF ( dft_is_meta() ) &
          delta_e = delta_e - SUM( rho%kin_r(:,:)*v%kin_r(:,:) )
       !
       delta_e = omega * delta_e /( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
       !
       CALL mp_sum( delta_e, intra_pool_comm )
       !
       if (lda_plus_u) then
          delta_e_hub = - SUM (rho%ns(:,:,:,:)*v%ns(:,:,:,:))
          if (nspin==1) delta_e_hub = 2.d0 * delta_e_hub
          delta_e = delta_e + delta_e_hub
       end if
       !
       IF (okpaw) delta_e = delta_e - SUM(ddd_paw(:,:,:)*rho%bec(:,:,:))
       !
       RETURN
       !
     END FUNCTION delta_e
     !
END SUBROUTINE print_acfdt_pw
