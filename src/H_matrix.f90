
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!#define DEBUG
!
!----------------------------------------------------------------------
SUBROUTINE H_matrix (uu, nevl, dv, H, avg_iter)
  !--------------------------------------------------------------------
  !
  !	This subroutine calculates Matrix elements of  Xo(fx)Xo 
  !     on the basis set of the RPA eigenvectors at the same frquency
  !     
  !
  USE kinds,                ONLY : DP
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE mp_global,            ONLY : intra_pool_comm, inter_pot_comm => inter_image_comm, npot => nimage
  USE gvect,                ONLY : ngm, nl, ngm_g, gstart
  USE mp,                   ONLY : mp_sum
  USE qpoint,               ONLY : xq
  USE control_lr,           ONLY : lgamma
  USE fft_base,             ONLY : dfftp
  USE fft_interfaces,       ONLY : fwfft, invfft
  USE control_acfdt,        ONLY : thr_ec, lepsi
  !
  IMPLICIT NONE
  !
  INTEGER, PARAMETER :: kterx=50
  !
  ! I/O variables
  !
  REAL(DP), INTENT(IN) :: uu
  ! input: imaginary frequency 
  INTEGER, INTENT(IN) :: nevl
  COMPLEX(DP), INTENT(IN) :: dv(ngm,nevl)
  ! input: number of eigenmodes to be computed
  ! in: eigenvalues of auxiliary Problem
  ! output: eigenvalues
  COMPLEX(DP), INTENT(INOUT) ::  H(nevl,nevl)
  ! input: eigenpotentials of auxiliary problem
  ! output: converged eigenpotentials
  REAL(DP), INTENT(OUT) :: avg_iter
  ! output: average number of iteration per mode in Davidson diagonalization
  ! output: <dv|chi0 vc chi0|dv>
  !
  ! local variables
  REAL(DP) :: u
  !
  COMPLEX(DP), ALLOCATABLE :: dvg(:,:), dng(:,:), dngX(:,:)
  ! potentials and density response in G-space
  COMPLEX(DP), ALLOCATABLE :: chi0(:,:)  
  ! Matrix to diagonalize
  ! Overlap matrix 
  ! Estimated eigenpotentials
  ! Identity Matrix 
  REAL(DP), ALLOCATABLE :: ew(:), ethr(:)
  ! estimated eigenvalues 
  ! threshold for eigenvalues
  COMPLEX(DP), ALLOCATABLE :: gs(:), aux(:)
  ! working space
  INTEGER :: notcnv
  ! integer  number of iterations performed
  LOGICAL, ALLOCATABLE :: conv(:), comp_dv(:)
  ! true if the root is converged
  INTEGER :: nkter, kter, kter0, kter1, nbase, n, m, nb1, np, nbn, ios, info
  ! counter on iterations
  ! dimension of the reduced basis
  ! counter on the reduced basis vectors
  ! do-loop counters
  !
  ! Ortho_base useless, I always need to compute overlap   
  LOGICAL :: exst, opnd, ortho_base = .false.  
  !LOGICAL :: exst, opnd, ortho_base = .true.

  REAL(DP) :: norm, epsi00, epstsr(3,3)
  !
  INTEGER :: ig, ir, i, j, ip, nvecx
  ! counters
  REAL(DP), EXTERNAL :: DDOT
  COMPLEX(DP), EXTERNAL :: ZDOTC
  !
  COMPLEX(DP) sum1, sum2
  ! 
  INTEGER, EXTERNAL :: find_free_unit
  !
  nvecx = nevl
  !
  !WRITE(stdout,'(7x,''Davidson diagonalization'')')
  !
  ! various checkings
  !
  IF (ngm_g .LT. nvecx) &
     CALL errore('diagnlres_david', 'nvecx TOO LARGE', 1)
  !IF ( lgamma ) call errore('diagnlres_david', &
  !              'Implementation at q=0 not completed yet', 1)
  !
  ! local arrays  
  !
  ALLOCATE ( dvg(ngm,nvecx), dng(ngm,nvecx),  dngX(ngm,nvecx) ) 
  ALLOCATE ( chi0(nvecx,nvecx)  )
  ALLOCATE ( ew(nvecx), ethr(nevl), conv(nevl), comp_dv(nevl) )
  ALLOCATE ( gs(nvecx) )
  IF (lgamma)  ALLOCATE ( aux(dfftp%nnr) )
  !
  CALL start_clock ('diagnlres_david')
  !
  ethr(:) = thr_ec; avg_iter = 0.d0
  !
  dvg(:,:) = ZERO; dng(:,:) = ZERO;  dngX(:,:) = ZERO
  chi0(:,:) = ZERO
  ew(:) = 0.d0; kter0 = 0; ios = 0; nkter = 0
  comp_dv(:) = .TRUE. ! must be set to .TRUE. for the code work in case NPOT=1
  !
  !
  dvg(:,1:nevl) = dv(:,1:nevl)
  !
  ! if q=(0,0,0) make sure that eigenpotentials are real in R-space
  !
  IF ( lgamma ) THEN
     DO n = 1, nevl
        ! put dvg on the FFT grid
        aux(:) = ZERO; aux(nl(1:ngm)) = dvg(1:ngm,n)
        ! go to R-space
        CALL invfft ('Dense', aux, dfftp)
        ! keep only the real part
        aux(:) = CMPLX( DBLE(aux(:)), 0.d0, kind=DP )
        ! go back to G-space
        CALL fwfft ('Dense', aux, dfftp)
        ! put back to dvg
        dvg(1:ngm,n) = aux(nl(1:ngm))
     ENDDO
     !
     ! for the case of diagonalizing the dielectric matrix, 
     ! the head element must be computed separately first
     !
     IF ( lepsi ) THEN
        !call solve_head( uu, epstsr )
        !epsi00 = epstsr(1,1)
        !WRITE(stdout,'(9x,"Head element of Vc*Xo: ",3F12.6)') (epstsr(n,n), n=1,3)
        !WRITE(stdout,'(/,9x,"epsilon_RPA(0,0)   : ",3F12.6)') (1.d0-epstsr(n,n), n=1,3)
        epsi00 = 0.d0
     ENDIF
     !
  ENDIF
  !
  ! orthogonalize if required
  !
  IF (ortho_base) THEN
     CALL start_clock ('ortho')
     DO i = 1, nevl
        CALL ZGEMM( 'C', 'N', i-1, 1, ngm, ONE, dvg, ngm, &
                    dvg(1,i), ngm, ZERO, gs(1), nvecx )
        CALL mp_sum( gs(1:i-1), intra_pool_comm )
        CALL ZGEMM( 'N', 'N', ngm, 1, i-1, -ONE, dvg(1,1), ngm, &
                    gs(1), nvecx, ONE, dvg(1,i), ngm )
        ! and normalize
        norm = DDOT(2*ngm, dvg(1,i), 1, dvg(1,i), 1)
        CALL mp_sum( norm, intra_pool_comm ); norm = 1.d0/SQRT(norm)
        CALL DSCAL (2*ngm, norm, dvg(1,i), 1 )
     ENDDO
     CALL stop_clock ('ortho')
  ENDIF
  !
  ! compute matrix elements in the trial basis set 


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#if defined DEBUG
dngX(:,:) = dvg(:,:)
dng(:,:) = dvg(:,:)
IF ( lgamma .AND. lepsi .AND. (gstart==2) ) THEN
  dngX(1,:) = (0.d0,0.d0)
  dng(1,:) =  (0.d0,0.d0)
ENDIF
CALL dchi_by_dv_rpax (uu, ngm, 1, dngX(1,1) )
CALL dchi_by_dv_rpax (uu, ngm, 1, dngX(1,2) )
!
CALL drhoiu_by_dv( uu, ngm, 1, dng(1,1) )
CALL vc12_dvg( ngm, 1, xq, dng(1,1) )
CALL vc12_dvg( ngm, 1, xq, dng(1,1) )
CALL drhoiu_by_dv( uu, ngm, 1, dng(1,1) )
!
CALL drhoiu_by_dv( uu, ngm, 1, dng(1,2) )
CALL vc12_dvg( ngm, 1, xq, dng(1,2) )
CALL vc12_dvg( ngm, 1, xq, dng(1,2) )
CALL drhoiu_by_dv( uu, ngm, 1, dng(1,2) )
!!
DO i=1,ngm
write(27,'( 4(E18.10), 1x)') DBLE(dng(i,1)), AIMAG(dng(i,1)) , DBLE(dngX(i,1)), AIMAG(dngX(i,1))
ENDDO
!
!!
sum1 = ZERO; sum2 = ZERO
DO i = 1, ngm
  sum1 = sum1 + CONJG(dvg(i,2))*dngX(i,1)
  sum2 = sum2 + CONJG(dvg(i,1))*dngX(i,2)
ENDDO
#if defined __MPI
  CALL mp_sum( sum1, intra_pool_comm )
  CALL mp_sum( sum2, intra_pool_comm )
#endif
WRITE(stdout,'(/,1X,"CHECK IF THE MATRIX XofxXoIS HERMITIAN: <1|M|2> = <2|M|1>*",/)' ) 
WRITE(stdout,*)  '<1|M|2>' , REAL(sum1), AIMAG(sum1)
WRITE(stdout,*)  '<2|M|1>' , REAL(sum2), AIMAG(sum2)
!
sum1 = ZERO; sum2 = ZERO
DO i = 1, ngm
  sum1 = sum1 + CONJG(dvg(i,2))*dng(i,1)
  sum2 = sum2 + CONJG(dvg(i,1))*dng(i,2)
ENDDO
#if defined __MPI
  CALL mp_sum( sum1, intra_pool_comm )
  CALL mp_sum( sum2, intra_pool_comm )
#endif
WRITE(stdout,'(/,1X,"CHECK IF THE MATRIX XovcXoIS HERMITIAN: <1|M|2> = <2|M|1>*",/)' )
WRITE(stdout,*)  '<1|M|2>' , REAL(sum1), AIMAG(sum1)
WRITE(stdout,*)  '<2|M|1>' , REAL(sum2), AIMAG(sum2)
!
!sum1 = DDOT(2*ngm, dvg(1,2), 1, dngX(1,1), 1)
!sum2 = DDOT(2*ngm, dvg(1,1), 1, dngX(1,2), 1)
!#if defined __MPI
!CALL mp_sum( sum1, intra_pool_comm )
!CALL mp_sum( sum2, intra_pool_comm )
!#endif
!WRITE(stdout,*) REAL(sum1), REAL(sum2)
!
sum1 = ZERO; sum2 = ZERO
WRITE(stdout,'(/,1X,"CHECK FOR TWO ELECTRON SYSTEM: fx = -vc/2",/)')
sum1 = ZERO; sum2 = ZERO
DO i = 1, ngm
  sum1 = sum1 - 2.D0 * CONJG(dvg(i,1))*dngX(i,1)
  sum2 = sum2 + 1.D0 * CONJG(dvg(i,1))*dng(i,1)
ENDDO
!sum1 =  2.D0 * DDOT(2*ngm, dvg(1,2), 1,  dngX(1,1), 1)
!sum2 = -1.D0 * DDOT(2*ngm, dvg(1,2), 1, dng(1,1), 1)
#if defined __MPI
  CALL mp_sum( sum1, intra_pool_comm )
  CALL mp_sum( sum2, intra_pool_comm )
#endif
WRITE(stdout,*) '- 2*<1|XofxXo|1>      ', sum1
WRITE(stdout,*) '<1|XovcXo|1>          ', sum2
!!WRITE(26,*) REAL( dngX(:,1)/dng(:,1) ), AIMAG(dngX(:,1)/dng(:,1) )
WRITE(stdout,*)
dng(:,:) = ZERO
dngX(:,:) = ZERO

!
#endif


  !
  ! determine image that actually compute dng first
  IF ( npot.GT.1 ) CALL distribute_dv ( nevl, comp_dv )
  !
  DO i = 1, nevl
     !
     IF ( comp_dv(i) ) THEN 
        !  Compute |dng> = chi0*(fx+vc)*chi0|dvg>
        dng(:,i) = dvg(:,i)
        dngX(:,i) = dvg(:,i)
        IF ( lgamma .AND. lepsi .AND. (gstart==2) ) dng(1,i) = (0.d0,0.d0) 
        IF ( lgamma .AND. lepsi .AND. (gstart==2) ) dngX(1,i) = (0.d0,0.d0)
        !
        !   Compute |dngX> = chi0*fx*chi0|dvg>
        !
        CALL dchi_by_dv_rpax (uu, ngm, 1, dng(1,i) )
        !
        !
        ! special treatment for q+G=0 component
        !
        IF ( lgamma .AND. lepsi .AND. (gstart==2) ) &
           dng(1,i) = CMPLX(epsi00, 0.d0, kind=DP) * dvg(1,i)
        !
        ! if q=(0,0,0) make sure that screened potentials are real in R-space
        ! ( remove numerical noise which makes imaginary part non-zero )
        !
        IF ( lgamma ) THEN
           ! put dvg on the FFT grid
            aux(:) = ZERO;  aux(nl(1:ngm)) = dng(1:ngm,i)
           ! go to R-space
           CALL invfft ('Dense', aux, dfftp)
           ! keep only the real part
           aux(:) = CMPLX( DBLE(aux(:)), 0.d0, kind=DP )
           ! go back to G-space
           CALL fwfft ('Dense', aux, dfftp)
           ! put back to dvg
           dng(1:ngm,i) = aux(nl(1:ngm))
        ENDIF
        !
     ELSE
        !
        dng(:,i) = (0.d0, 0.d0)
        !
     ENDIF
     !
  ENDDO
  !
#if defined __MPI
  ! mp_sum is used as a trick to circulate dng
  IF ( npot.GT.1 ) CALL mp_sum ( dng(:,1:nevl), inter_pot_comm ) 
#endif
  !
  !
  ! chi0 = -<dvg|dng> REMEMBER |dng> = chi0*(vc+fx)*chi0|dvg> but we need -chi0*(vc+fx)*chi0|dvg>=-|dng>
  !
  CALL ZGEMM( 'C', 'N', nevl, nevl, ngm, -ONE, dvg(1,1), ngm,&    
               dng(1,1), ngm, ZERO, chi0(1,1), nvecx )
#if defined __MPI
  CALL mp_sum( chi0(:,1:nevl), intra_pool_comm )
#endif
  !
  !
  ! arrange them in the right order
  !
  DO n = 1, nevl
     !
     chi0(n,n) = CMPLX( REAL(chi0(n,n)), 0.D0, kind=DP )
     !
     DO m = n + 1, nevl
        !
        IF ( lgamma ) THEN
           chi0(n,m) = CMPLX( REAL(chi0(n,m)), 0.D0, kind=DP )
           chi0(m,n) = CONJG( chi0(n,m) )
        ELSE
           chi0(m,n) = CONJG( chi0(n,m) )
        ENDIF
        !
     END DO
     !
  ENDDO
  !
  H(:,:) = chi0(:,:)
  !
  !
  CALL stop_clock ('diagnlres_david')
  !
  DEALLOCATE ( dvg, dng, dngX )
  DEALLOCATE ( chi0 )
  DEALLOCATE ( ew, ethr, conv, comp_dv )
  DEALLOCATE ( gs )
  IF (lgamma)  DEALLOCATE ( aux )
  !
  RETURN
  !
END SUBROUTINE H_matrix
