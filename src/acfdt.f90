!
! Copyright (C) 2001-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-------------------------------------------------------------------
PROGRAM main_acfdt
  !-----------------------------------------------------------------
  !
  ! ... This is the main driver of the acfdt code.
  ! ... It reads all the quantities computed by pwscf, then
  ! ... checks if some recover files present and determine
  !
  USE io_global,       ONLY : stdout
  USE disp,            ONLY : nqs
  USE control_ph,      ONLY : bands_computed
  USE check_stop,      ONLY : check_stop_init
  USE ph_restart,      ONLY : ph_writefile
  USE mp_global,       ONLY : mp_startup, npot => nimage
  USE pot_io_routines, ONLY : io_pot_start, io_pot_stop
  USE environment,     ONLY : environment_start
  USE control_flags,   ONLY : lscf
  USE control_acfdt,   ONLY : turn_oep_on, vc_rpa, efieldper
  USE save_ph,         ONLY : save_ph_input_variables
  USE acfdtest,        ONLY : skip_ph
  USE mp_world,        ONLY : mpime
  USE io_files,        ONLY : nd_nmbr, tmp_dir
  !
  IMPLICIT NONE
  !
  INTEGER   :: iq
  LOGICAL   :: do_band, do_iq, setup_pw
  CHARACTER (LEN=9)   :: code = 'ACFDT'
  CHARACTER (LEN=256) :: auxdyn
  CHARACTER(LEN=6), EXTERNAL :: int_to_char
  !
  INTEGER :: ierr
  !
  ! Initialize MPI, clocks, print initial messages
  !
#if defined __MPI
  CALL mp_startup ( start_images = .true. )
#endif
  CALL environment_start ( code )
  CALL start_clock( "PHONON" ) ! NsC 
  !
# if defined __MPI
  IF (npot>1) nd_nmbr = TRIM ( int_to_char( mpime+1 )) !NsC: See Modules/environment_start
  ! To open different wfc buffers when npot=nimage > 1. Prevent error in close_phq at the end of the code. 
# endif 
  !
  WRITE( stdout, '(/5x,"Ultrasoft (Vanderbilt) Pseudopotentials")' )
  !
  ! ... and begin with the initialization part
  !
#if defined __MPI
  IF (npot>1) CALL io_pot_start()
#endif
  CALL acfdt_readin()
#if defined __MPI
  IF (npot>1) CALL io_pot_stop()
#endif
  !
  CALL check_stop_init()
  !
  ! ... Checking the status of the calculation and initialize q-mesh if needed
  ! 
  skip_ph = .TRUE.
  CALL check_initial_status(auxdyn)
  skip_ph = .FALSE. 
  !
  ! ... Before this call was in check_initial_stauts 
  ! ... now we call it here (not sure it is really needed) !=NsC=!
  !
  CALL save_ph_input_variables()
  !
  ! ... Exact-exchange energy with orbitals computed from a pw.x run
  !
  CALL openfil_acfdt()
  !
  CALL allocate_acfdt()
  !
  IF ( turn_oep_on ) then
     !
     ! ... OEP for ACFDT(EXX/RPA)
     !
     call acfdt_oep() 
  ELSE
     !
     ! ... ACFDT (RPA Ec) @ LDA/GGA ..
     !
     ! Here we compute RPA Ec from LDA/GGA KS density within ACFD theory.
     !
     DO iq = 1, nqs 
        !
        CALL setup_q(auxdyn, do_band, do_iq, setup_pw, iq)
        !
        ! If this q is not done in this run, cycle
        !
        IF (.NOT.do_iq) CYCLE
        !
        ! If necessary the bands are recalculated
        ! lscf=.FALSE. by default (declared in Modules/control_flags.f90),
        ! so one-shot RPA should be ok. But if the loop over exxrpa potential
        ! is peformed, lscf must be reset to .FALSE. at each iteration
        lscf = .FALSE.
        ! NsC >>>
        !IF (setup_pw) CALL run_pwscf(do_band)
        IF (setup_pw) CALL run_nscf(do_band, iq)
        ! NsC <<< 
        !
        !  Initialize the quantities which do not depend on
        !  the linear response of the system
        !
        CALL initialize_acfdt()
        !
        ! NB: if q=/0, xk(:) also contains k+q, nks will be doubled.
        !     thus exxinit in exx.f90 does not work properly.
        !     need to think more about how to overcome this problem !
        !
        ! compute RPA Ec and eventually RPA Potential
        !
        efieldper = .false.
        CALL ec_acfdt ( 1, vc_rpa )
        !
        ! ... cleanup of the variables for the next q point
        !
        CALL clean_pw_ph(iq)
        !
     END DO 
     !
  END IF
  !
  CALL deallocate_acfdt()
  !
  CALL closefil_acfdt()
  !
  CALL ph_writefile('init',0,0,ierr)
  !
  IF (bands_computed) CALL print_clock_pw()
  !
  CALL stop_smoothly_ph( .TRUE. )
  !
  STOP
  !
END PROGRAM main_acfdt
