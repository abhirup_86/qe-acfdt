
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!=================================================
SUBROUTINE acfdt_estimate_veff_kspot ( oep_iter )
 !================================================
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE fft_base,      ONLY : dfftp
 USE control_acfdt, ONLY : control_vc_rpa, dvgenc, sum_dchi, &
                           vx_exx, vc_rpa
 USE scf,           ONLY : vrs
 USE acfdt_scf,     ONLY : dv_update 
 USE xml_io_base,   ONLY : write_rho
 USE io_files,      ONLY : tmp_dir, prefix
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN) :: oep_iter 
 !
 ! local variables
 !
 INTEGER  :: is 
 REAL(DP) :: vhion_veff(dfftp%nnr, nspin)
 !
 REAL(DP) :: pot_in(dfftp%nnr, nspin)
 REAL(DP) :: pot_out(dfftp%nnr, nspin)
 REAL(DP) :: ddpot_in(dfftp%nnr, nspin)
 !  
 COMPLEX(DP) :: vxc_app(dfftp%nnr)
 COMPLEX(DP) :: dden_in(dfftp%nnr)
 ! 
 REAL(DP) :: betamix, grad_norm  
 !
 CHARACTER(LEN=256) :: dirname
 !
 dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
 !
 control_vc_rpa = .FALSE.
 !
 betamix = 0.5_DP
 !
 ! ... estimate gradient \dE/dv, and approximate a potential
 !     that gives the gradient
 !
 CALL gradient_energy ( dden_in, vhion_veff, vxc_app, oep_iter, grad_norm) 
 write(stdout,*) "gradient_of_energy", oep_iter, grad_norm  
 !
 !
 ! ... update the input potential for the next oep step
 !
 !pot_in(:,:) = 0.0_DP; pot_in(:,:) = dv_update(:,:)
 pot_in(:,:) = 0.0_DP; pot_in(:,:) = vrs(:,:)
 ! 
 ddpot_in(:,:) = 0.0_DP; 
 DO is = 1, nspin
   ddpot_in(:,is) = vhion_veff(:,is) + DBLE (vxc_app(:))
 ENDDO
 !
 ! ... by pulay mixing method 
 ! 
 !CALL pot_pulay_mixing ( pot_in, pot_out, dden_in, ddpot_in, betamix, oep_iter)
 !
 ! ... by line search method
 !
 CALL line_search_method( pot_in, pot_out, dden_in, ddpot_in, betamix, oep_iter)
 !
 dv_update(:,:) = 0.0_DP; dv_update(:,:) = pot_out(:,:)       
 ! 
 ! ... save on disk this effective potential
 !
 CALL write_rho(dirname, dv_update, nspin, 'potential_KS_effective_pot')
 ! 
 ! ... 
 ! 
 RETURN
 !
ENDSUBROUTINE acfdt_estimate_veff_kspot
!============================================
!=============================================================
SUBROUTINE gradient_energy ( dden , vhion_veff, vxc_app, iter, grad_norm)
 !============================================================
 ! Gradient of energy wrt the effective potential
 ! |dden> = Xo|(vh+vion -veff)> + |dExc/dveff>
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE cell_base,     ONLY : omega
 USE fft_base,      ONLY : dfftp
 USE control_acfdt, ONLY : dvgenc, sum_dchi, exxvx, rpavc, &
                           vx_exx, vc_rpa 
 USE acfdt_scf,     ONLY : print_xc_pots, vxc_by_iteration,  vxc_by_invert_chi 
  !
  IMPLICIT NONE
  !
  ! I/O variable
  !
  COMPLEX(DP), INTENT(INOUT) :: dden      (dfftp%nnr) 
  COMPLEX(DP), INTENT(INOUT) :: vxc_app   (dfftp%nnr)
  REAL(DP),    INTENT(INOUT) :: vhion_veff(dfftp%nnr, nspin) 
  REAL(DP),    INTENT(INOUT) :: grad_norm
  INTEGER ,    INTENT(IN)    :: iter 
  !
  !local variables
  !
  INTEGER :: ndim, i
  REAL(DP):: sum_tmp, norm
  !    
  COMPLEX(DP), ALLOCATABLE :: diff_dha_dloc(:)
  !
  ndim = dfftp%nnr
  ALLOCATE(diff_dha_dloc(ndim))
  !
  dden(:)    = ZERO
  vxc_app(:) = ZERO 
  !----------------------------------------------------------------------------!
  !                     ! Gradien density by EXX !                             !
  !----------------------------------------------------------------------------!
  !
  !---density of exx; dn_nl = dExx/dv ---!
  !
  IF(vx_exx) THEN
    !
    !calculate dn(r) wrt V_x Fock
    !
    dvgenc(:,:,:) = ZERO
    CALL nonloc_dfpt ( )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    !
    dden(:) = dvgenc(:,1,2)
    !
  ENDIF
  !----------------------------------------------------------------------------!
  !                     ! Gradien density by RPA !                             !
  !----------------------------------------------------------------------------!
  !
  IF(vc_rpa) THEN
    !
    ! ... put dn(r) = \dEc(RPA)/dv(r) wrt Ec_RPA
    ! ... it is sum_dchi(:)
    !
    dden(:) = dden(:) + sum_dchi(:)
    !
  ENDIF
  !
  !
  !... estimate an approximation for V_xc(exx/rpa)
  !  
  IF (vxc_by_invert_chi) CALL approx_vxc_by_gorling (dden, vxc_app)
  !
  IF (vxc_by_iteration) THEN
     !
     CALL approx_vxc_by_anderson (iter, dden, vxc_app) 
     !
     !IF (print_xc_pots) THEN
        !
        !CALL approx_vx_by_anderson (iter, dden, vxc_app )
        !
     !ENDIF
     !
  ENDIF 
  !
  !--------------------------------------------------------!
  !       ! Gradien density by Xo|(Vh+Vion)-Veff> !        !
  !--------------------------------------------------------!
  CALL gra_vrs_vhi ( diff_dha_dloc, vhion_veff )
  !
  dden(:) = dden(:) + diff_dha_dloc(:)
  !
  !--------------------------------------------------------!
  !
  ! ... estimate normalization of Gradien: |dE/dV| 
  !
  grad_norm = 0.0_DP
  DO i = 1, ndim
    grad_norm = grad_norm + CONJG(dden(i))*dden(i)
  ENDDO
  grad_norm   = omega * grad_norm / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
  grad_norm   = DSQRT(grad_norm)
  !
  ! ...
  !
  DEALLOCATE(diff_dha_dloc)
  !
  RETURN
  !
END SUBROUTINE gradient_energy 
!==============================
