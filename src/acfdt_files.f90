!
! Copyright (C) 2001-2003 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE openfil_acfdt()
  !----------------------------------------------------------------------------
  USE control_acfdt,        ONLY : pot_collect, iundvr, iuchi0, &
                                   vc_rpa, iundedv, approx_chi0
  USE gvect,                ONLY : ngm, ngm_g
  USE io_global,            ONLY : ionode
  USE io_files,             ONLY : diropn
  USE acfdt_scf,            ONLY : iovrs
  !
  IMPLICIT NONE
  !
  LOGICAL :: exst
  !
  iundvr = 300
  !
  if ( pot_collect ) then
     if (ionode) call diropn(iundvr,'dvr', 2*ngm_g, exst)
  else
     call diropn(iundvr,'dvr', 2*ngm, exst)
  endif
  !
  if (approx_chi0)  then
     ! 
     iuchi0 = 299
     !
     if ( pot_collect ) then
        if (ionode) call diropn(iuchi0,'dvchi0', 2*ngm_g, exst)
     else
        call diropn(iuchi0,'dvchi0', 2*ngm, exst)
     endif
     !
  endif
  !
  if (vc_rpa) then
     !
     iundedv = 499
     !
     if ( pot_collect ) then
        if (ionode) call diropn(iundedv,'dedvg', 2*ngm_g, exst)
     else
        call diropn(iundedv,'dedvg', 2*ngm, exst)
     endif  
     !
  endif
  !
  return
  !
END SUBROUTINE openfil_acfdt
!
!
!----------------------------------------------------------------------------
SUBROUTINE closefil_acfdt()
  !----------------------------------------------------------------------------
  USE control_acfdt,        ONLY : pot_collect, iundvr, iuchi0, &
                                   vc_rpa, iundedv, approx_chi0 
  USE gvect,                ONLY : ngm, ngm_g
  USE io_global,            ONLY : ionode
  USE acfdt_scf,            ONLY : iovrs
  !
  IMPLICIT NONE
  !
  LOGICAL :: exst, opnd
  !
  if ( pot_collect ) then
     !
     if (ionode) then
        !
        inquire( unit = iundvr, opened = opnd )
        if ( opnd ) close( unit = iundvr, status = 'delete' )
        !
        if (approx_chi0) then 
           !
           inquire( unit = iuchi0, opened = opnd )
           if ( opnd ) close( unit = iuchi0, status = 'delete' )
           !
        endif
        !
        ! file for saving effective potential
        inquire( unit = iovrs, opened = opnd )
        if ( opnd ) close( unit = iovrs, status = 'delete' )
        !
        if (vc_rpa) then
           !
           inquire( unit = iundedv, opened = opnd )
           if ( opnd ) close( unit = iundedv, status = 'delete' )
           !
        endif
        ! 
     endif
     !
  else
     !
     inquire( unit = iundvr, opened = opnd )
     if ( opnd ) close( unit = iundvr, status = 'delete' )
     !
     if (approx_chi0) then
        !
        inquire( unit = iuchi0, opened = opnd )
        if ( opnd ) close( unit = iuchi0, status = 'delete' )
        !
     endif
     !
     if (vc_rpa) then
        ! 
        inquire( unit = iundedv, opened = opnd )
        if ( opnd ) close( unit = iundedv, status = 'delete' )
        !
     endif
     !
  endif
  ! 
  return
  !
END SUBROUTINE closefil_acfdt
