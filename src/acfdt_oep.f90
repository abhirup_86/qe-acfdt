!
! Copyright (C) 2001-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-------------------------------------------------------------------
SUBROUTINE acfdt_oep ()
  !-----------------------------------------------------------------
  !
  ! ... This is the driver of OEP within ACFDT theorem.
  ! ... It reads all the quantities calculated by pwscf, it
  ! ... checks if some recover file is present and determines
  !
  USE io_global,       ONLY : stdout
  USE acfdt_scf,       ONLY : hf_scfrpa, scf_exxrpa
  ! 
  IMPLICIT NONE
     !
     CALL allocate_oep  ()
     !
     !----- TESTING NUMERICAL TOTAL ENERGY AND EIGENVALUE DERIVATIVE --!
     !     IF (acfdt_is_active) CALL acfdt_num()		       !
     !-----------------------------------------------------------------!
     !
     IF (scf_exxrpa) THEN
       !
       WRITE(stdout,'(/,/,"SCF-EXXRPA CALCULATION ",/,/)')             
       CALL acfdt_scf_exxrpa()                                        
       !
     ENDIF  
     !
     !------------- HF+scfRPA METHOD WILL BE USED----------------------!
     !                                                                 !  
     IF (hf_scfrpa) THEN                                               ! 
       !                                                               !
       WRITE(stdout,'(/,/,"HF+scfRPA CALCULATION ",/,/)')              !
       WRITE(stdout,'(/,/," developing ...  ",/,/)')                   !
       !CALL acfdt_hf_scfrpa()                                         !
       !                                                               ! 
     ENDIF                                                             !
     !                                                                 !                                                                                
     !-----------------------------------------------------------------!
     !
     CALL deallocate_oep()
     !
  RETURN
  !
END SUBROUTINE acfdt_oep
