!
! Copyright (C) 2001-2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
SUBROUTINE acfdt_readin()
  !----------------------------------------------------------------------------
  !
  !    This routine reads the control variables for the program acfdt.x
  !    from standard input (unit 5).
  !    A second routine readfile reads the variables saved on a file
  !    by the self-consistent program.
  !
  !
  USE kinds,         ONLY : DP
  USE ions_base,     ONLY : nat
  USE mp,            ONLY : mp_bcast
  USE klist,         ONLY : xqq, xk, nks, nkstot
  USE control_flags, ONLY : gamma_only, restart, lkpoint_dir, lecrpa
  USE lsda_mod,      ONLY : nspin
  USE spin_orb,      ONLY : domag
  USE run_info,      ONLY : title
  USE control_lr,    ONLY : lgamma
  USE control_ph,    ONLY : maxter, lgamma_gamma, xmldyn, &
                            tr2_ph, niter_ph,       &
                            nmix_ph, ldisp, recover, start_irr, &
                            last_irr, start_q, last_q, current_iq, tmp_dir_ph, &
                            ext_recover, ext_restart, u_from_file
  USE save_ph,       ONLY : tmp_dir_save
  USE qpoint,        ONLY : nksq, xq
  USE output,        ONLY : fildyn, fildvscf, fildrho
  USE disp,          ONLY : nq1, nq2, nq3
  USE io_files,      ONLY : tmp_dir, prefix
  USE noncollin_module, ONLY : noncolin
  USE control_flags, ONLY : modenum, twfcollect
  USE io_global,     ONLY : ionode, stdout, meta_ionode, meta_ionode_id
  USE mp_global,     ONLY : nproc_pool, nproc_file, nproc_pool_file, &
                            my_image_id, nproc_image_file, nproc_image, &
                            intra_image_comm, world_comm ! NsC
  USE paw_variables, ONLY : okpaw
  USE fft_base,      ONLY : dfftp
  USE ph_restart,    ONLY : ph_readfile
  USE control_acfdt, ONLY : tau1, tau2, vc_rpa, vx_exx, turn_oep_on, u0_ec, wq, &
                            thr_oep, thr_ec, rpax, max_oep_space, neigv, neigvchi0, &
                            nlambda, oep_maxstep, pot_collect, savealldep, approx_chi0, &
                            c_mix, depdir1, depdir2, ecut_aux, idiag, iumax, nf_ec, potcorr
  USE acfdtest,      ONLY : acfdt_is_active, acfdt_num_der, ir_point, delta_vrs, &
                            f1,f2,f3, acfdt_term1, acfdt_term2, acfdt_term3, test_oep
  USE acfdt_scf,     ONLY : vrs_save, oep_recover, input_lambda, oep_method, &
                            stopped_first_iter, print_xc_pots, scf_rpa_plus, & 
                            line_search_index, hf_scfrpa, scf_exxrpa, do_acfdt, &
                            vxc_by_invert_chi, vxc_by_iteration 
  USE xml_io_base,   ONLY : read_rho
  USE acfdt_ener,    ONLY : acfdt_in_pw 
  USE exx,           ONLY : ecutfock
  USE gvecw,         ONLY : ecutwfc
  !
  IMPLICIT NONE
  !
  CHARACTER(LEN=256), EXTERNAL :: trimcheck
  !
  INTEGER :: ios, ipol, iter, na, it, ierr
    ! integer variable for I/O control
    ! counter on polarizations
    ! counter on iterations
    ! counter on atoms
    ! counter on types
  CHARACTER (LEN=256) :: outdir
  !
  CHARACTER(LEN=1), EXTERNAL :: capital
  CHARACTER(LEN=6) :: int_to_char
  INTEGER            :: i
  LOGICAL            :: nogg
  LOGICAL, EXTERNAL  :: imatches
  LOGICAL, EXTERNAL  :: has_xml
  CHARACTER(LEN=256) :: dirname
  !
  ! modenum      : single mode calculation
  ! fildyn       : output file for the dynamical matrix
  ! fildvscf     : output file containing deltavsc
  ! fildrho      : output file containing deltarho
  ! start_q      : in q list does the q points from start_q to last_q
  ! last_q       : 
  ! start_irr    : does the irred. representation from start_irr to last_irr
  ! last_irr     : 
  ! nogg         : if .true. lgamma_gamma tricks are not used
  !
  NAMELIST / INPUTEC / outdir, prefix, recover, nf_ec, u0_ec, neigv, neigvchi0, &
                       thr_ec, xqq, wq, idiag, iumax, pot_collect, savealldep,  &
                       depdir1, depdir2, tau1, tau2, vc_rpa, vx_exx, tr2_ph,    &
                       niter_ph, nmix_ph, ldisp, nq1, nq2, nq3, potcorr, c_mix, &
                       oep_maxstep, acfdt_num_der, acfdt_is_active, approx_chi0,&
                       input_lambda, oep_recover, ir_point, delta_vrs, f1,f2,f3,&
                       acfdt_term1, acfdt_term2, acfdt_term3, test_oep, thr_oep,&
                       max_oep_space, ecut_aux, oep_method, stopped_first_iter, &
                       do_acfdt, print_xc_pots, scf_rpa_plus, line_search_index,&
                       rpax, nlambda
 
  ! lecrpa       : allways put = .TRUE. in ACFDT run.
  ! prefix       : the prefix of files produced by pwscf
  ! outdir       : directory where input, output, temporary files reside
  ! recover      : recover=.true. to restart from an interrupted run
  ! nf_ec        : # of frequencies used in GL-intergration for Ec (default: 8)
  ! u0_ec        : parameter in transformation of frequency (default: 2.0) (the same u0_c6)
  ! neigv        : # of eigenvalues to compute Ec at first iteration (default: 20)
  ! neigvchi0    : # of eigenvalues of chi0 at iu = 0 (default: 20)
  ! thr_ec       : threshold for iterative diagonalization of KS Resp. Funct.
  ! thr_oep      : threshold for iterative oep procedure.
  ! xqq          : coordinates of q point
  ! wq           : weight of q-point
  ! idiag        : variable that determines algorithm for diagonalization of response functions
  ! iumax        : for smooth restart calculation
  ! pot_collect  : if .true., eigenpotentials are collected to ionode before writing to disk
  ! savealldep   : If .TRUE., all eigenpotentials at each frequency will be written to disk
  ! depdir1      : path to directory contains DEP of the monomer 1
  ! depdir2      : path to directory contains DEP of the monomer 2
  ! tau1         : translation vector of the monomer 1
  ! tau2         : translation vector of the monomer 2
  ! vc_rpa       : if .true., RPA correlation potential is calculated
  ! vx_exx       : if .true., exact exchange potential is calculated
  ! tr2_ph       : convergence threshold for linear systems
  ! niter_ph     : maximum number of iterations for solving DFPT equations (maybe unnecessary)
  ! nmix_ph      : number of previous iterations used in mixing (maybe unnecessary)
  ! oep_method   : OEP methods : (0) No scf calculation (one-shot).  
  !                              (1) Indirect   : Stephan_Perdew (SP) method is used. 
  !                              (2) Semi_direct: Gorling method is used to compute both Vx and Vc.
  !                              (3) Semi direct: An iterative method is used to compute Vx;
  !                                  and a hybrid between iterative method and Gorling's one 
  !                                  used to compute both Vx and Vc. 
  !                              (4) Direct     : OEP is used (Yang, Qu).
  ! potcorr        : if .true., Vxc(EXX/RPA) are corrected with Vxc(LDA/GGA) 
  ! acfdt_num_der  : if .true., a new OEP procedure will be activated
  ! acfdt_is_active: if .true., a new OEP procedure will be activated
  ! input_lambda   : the cofficient of updating potential in new OEP procedure
  ! approx_chi0    : if .true., a matrix chi0 is calculated at first step )
  ! oep_maxstep    : maximum number iteration step for each time OEP run
  ! max_oep_space  : maximum of dimention of the anderson'smixing method
  ! ecut_aux       : ecutoff of potential vectors
  ! oep_recover    : if .true., effective potential used in new OEP procedure is recovered
  ! do_acfdt       : if .true., deactive ec_acfdt calculation, used in case of exx-oep only
  ! print_xc_pots  : if .true., the calculation for vxc in solve_oep_andeson.f90 will be splitted into vxc and vx, then vc = vxc-vx. 
  ! scf_rpa_plus   : if .true., a short-range rpa correction for both potential and energy  is computed upon scf
  ! line_search_method : (1) using a simple linear search method with cofficient = 0.5
  !                      (2) using a quadratic line search  
  !                      (3) using a cubic line search  
  ! rpax           : if .true. exx kernel contribution in one shot caculation is computed
  ! 
  IF (meta_ionode) THEN
  !
  ! ... Input from file ?
  !
     CALL input_from_file ( )
  !
  ! ... Read the first line of the input file
  !
     READ( 5, '(A)', IOSTAT = ios ) title
  !
  ENDIF
  !
  ! NsC 
  CALL mp_bcast(ios, meta_ionode_id, world_comm )
  CALL errore( 'ACFDT_reading', 'reading title ', ABS( ios ) )
  ! NsC 
  call mp_bcast ( title, meta_ionode_id, world_comm )
  !
  ! Rewind the input if the title is actually the beginning of inputph namelist
  IF( imatches("&inputec", title)) THEN
    WRITE(*, '(6x,a)') "Title line not specified: using 'default'."
    title='default'
    REWIND(5, iostat=ios)
    CALL errore('acfdt_reading', 'Title line missing from input.', abs(ios))
  ENDIF
  !
  ! ... set default values for variables in namelist
  !
  CALL get_environment_variable( 'ESPRESSO_TMPDIR', outdir )
  IF ( TRIM( outdir ) == ' ' ) outdir = './'
  prefix       = 'pwscf'
  recover      = .FALSE.
  lecrpa       = .TRUE. 
  nf_ec        = 5
  iumax        = 5
  u0_ec        = 2.d0
  neigv        = 20
  neigvchi0    = 20
  thr_ec       = 1.d-5
  xqq(:)       = 0.d0
  wq           = 0.d0
  idiag        = 3
  vc_rpa       = .FALSE. 
  vx_exx       = .FALSE. 
  pot_collect  = .FALSE.
  savealldep   = .FALSE.
  depdir1      = ''
  depdir2      = ''
  tau1(:)      = 0.d0
  tau2(:)      = 0.d0
  niter_ph     = maxter
  nmix_ph      = 4
  tr2_ph       = 1.D-12
  ldisp        = .FALSE.
  fildvscf     = ' '
  nq1          = 0
  nq2          = 0
  nq3          = 0
  !
  oep_method   = 0
  oep_maxstep  = 50
  potcorr      = .FALSE.
  c_mix        = 1.0_DP
  thr_oep      = 1.d-5
  max_oep_space = 50
  ecut_aux     = 80 
  !
  acfdt_is_active =.FALSE.
  acfdt_num_der   =.FALSE.
  oep_recover     =.FALSE.
  approx_chi0     =.FALSE.
  input_lambda    = 1.0_DP
  stopped_first_iter=.FALSE. 
  !
  ir_point        = 0
  delta_vrs       = 0.0_DP
  f1=1.0_DP
  f2=1.0_DP
  f3=1.0_DP
  acfdt_term1 = .FALSE.
  acfdt_term2 = .FALSE.
  acfdt_term3 = .FALSE.
  test_oep    = .FALSE.
  do_acfdt    = .TRUE.
  print_xc_pots = .FALSE.
  scf_rpa_plus  = .FALSE.
  line_search_index = 1 
  rpax       = .FALSE. 
  nlambda    = 10
  !
  fildyn  = 'matdyn'
  ! 
  ! ...  reading the namelist inputec
  !
  IF (meta_ionode) READ( 5, INPUTEC, IOSTAT = ios )
  ! 
  ! NsC 
  CALL mp_bcast(ios, meta_ionode_id, world_comm)
  !
  CALL errore( 'acfdt_readin', 'reading inputec namelist', ABS( ios ) )
  !
  IF (meta_ionode) tmp_dir = trimcheck (outdir)
  !
  ! ...  assign value for q point (just if ldisp = .false.)
  !
  IF (meta_ionode .and. .not.ldisp) xq(:) = xqq(:)
  !
  ! ... broadcasting all input variables to other nodes
  !
  CALL bcast_acfdt_input ( ) 
  !
  ! ... Check all namelist variables
  !
  IF (tr2_ph <= 0.D0) CALL errore (' acfdt_readin', ' Wrong tr2_ph ', 1)
  !
  IF (niter_ph.LT.1.OR.niter_ph.GT.maxter) CALL errore ('acfdt_readin', &
       ' Wrong niter_ph ', 1)
  !
  IF (ldisp.AND.vc_rpa) &
     CALL errore ('acfdt_readin', 'scf RPA: single q-point only', 1 )
  !
  IF (ldisp.AND.vx_exx) &
     CALL errore ('acfdt_readin', 'exact exchange: single q-point only', 1 )
  !
  IF (.NOT.ldisp) THEN
     lgamma = xq (1) .EQ.0.D0.AND.xq (2) .EQ.0.D0.AND.xq (3) .EQ.0.D0
     IF ( wq .le. 0.d0 ) CALL errore ('acfdt_readin', 'wq must be POSITIVE!!!', 1 )
  ENDIF
  !
  IF (ldisp) wq=1.D0/(nq1*nq2*nq3)
  !
  IF (nf_ec<=0 .or. u0_ec<0.d0) &
     CALL errore ('acfdt_readin', 'nf_ec or u0_ec NEGATIVE!!!', 1 )
  ! 
  !   Here we finished the reading of the input file.
  !   Now allocate space for pwscf variables, read and check them.
  !
  tmp_dir_save=tmp_dir
  tmp_dir_ph= TRIM (tmp_dir) // '_ph' // int_to_char(my_image_id)
  ext_restart=.FALSE.
  ext_recover=.FALSE.

  IF (recover) THEN
     CALL ph_readfile('init',0,0,ierr)
     IF (ierr /= 0 ) THEN
        recover=.FALSE.
        goto 1001
     ENDIF
     tmp_dir=tmp_dir_ph
     CALL check_restart_recover(ext_recover, ext_restart)
     tmp_dir=tmp_dir_save
     IF (ldisp) lgamma = (current_iq==1)
     !
     !  If there is a restart or a recover file ph.x has saved its own data-file 
     !  and we read the initial information from that file
     !
     IF ((ext_recover.OR.ext_restart).AND..NOT.lgamma) tmp_dir=tmp_dir_ph
     u_from_file=.true.
  ENDIF
1001 CONTINUE
  !
  ! read data produced by pwscf
  !
  CALL read_file ( )
  if (ecutfock <=0.d0) ecutfock = 4._DP * ecutwfc

  tmp_dir=tmp_dir_save
  !
  IF (gamma_only) CALL errore('acfdt_readin',&
     'cannot start from pw.x data file using Gamma-point tricks',1)

  IF (okpaw.and.noncolin.and.domag) CALL errore('acfdt_readin',&
     'The acfdt code with paw and domag is not available yet',1)

  IF (nproc_image /= nproc_image_file .and. .not. twfcollect)  &
     CALL errore('acfdt_readin',&
     'pw.x run with a different number of processors. Use wf_collect=.true.',1)
 
  IF (nproc_pool /= nproc_pool_file .and. .not. twfcollect)  &
     CALL errore('acfdt_readin',&
     'pw.x run with a different number of pools. Use wf_collect=.true.',1)
  !
  !  the dynamical matrix is written in xml format if fildyn ends in
  !  .xml or in the noncollinear case.
  !
  xmldyn=has_xml(fildyn)
  IF (noncolin) xmldyn=.TRUE.
  !
  ! If a band structure calculation needs to be done do not open a file 
  ! for k point
  !
  lkpoint_dir=.FALSE.
  restart = recover
  !
  lgamma_gamma=.FALSE.
  IF (.NOT.ldisp) THEN
     IF (nkstot==1.OR.(nkstot==2.AND.nspin==2)) THEN
        lgamma_gamma=(lgamma.AND.(ABS(xk(1,1))<1.D-12) &
                            .AND.(ABS(xk(2,1))<1.D-12) &
                            .AND.(ABS(xk(3,1))<1.D-12) )
     ENDIF
     IF (nogg) lgamma_gamma=.FALSE.
     !
     IF (lgamma) THEN
        nksq = nks
     ELSE
        nksq = nks / 2
     ENDIF
  ENDIF
  !
  !  here we need to set proper value for variables used by ph.x only 
  !  ( we have not been able to remove from acfdt.x yet )
  !
  lgamma_gamma = .FALSE.
  modenum      = 0
  start_irr    = 0
  last_irr     = -1000 
  start_q      = 1 
  last_q       = -1000
  fildrho      = ''
  IF (start_irr < 0 ) CALL errore('acfdt_readin', 'wrong start_irr',1)
  IF (start_q <= 0 ) CALL errore('acdft_readin', 'wrong start_q',1)
  !
  CALL allocate_part ( nat )
  !
  IF (ldisp .AND. (nq1 .LE. 0 .OR. nq2 .LE. 0 .OR. nq3 .LE. 0)) &
       CALL errore('acfdt_readin','nq1, nq2, and nq3 must be greater than 0',1)
  !
  ! ... oep checking ...
  !
  IF(oep_method == 0) THEN  ! one_shot 
    !
    ! ... as defaul ... 
    !
    vc_rpa  = .FALSE.
    vx_exx  =  rpax         ! even in a one-shot calculation vx_exx is needed for rpax
    turn_oep_on = .FALSE.
    acfdt_in_pw = .FALSE.   ! one deactivates solving KS eqs in ./PW/src/non_scf.f90 
    !
    !
  ELSEIF (oep_method == 1) THEN ! scf exx_only
    !
    scf_exxrpa = .TRUE.
    !
    vc_rpa  = .FALSE.
    vx_exx  = .TRUE.
    turn_oep_on = .TRUE.
    acfdt_in_pw = .TRUE.    ! one deactivates solving KS eqs in ./PW/src/non_scf.f90  
    !
    vxc_by_invert_chi = approx_chi0
    vxc_by_iteration  = .not.approx_chi0
    !
  ELSEIF (oep_method == 2) THEN ! scf exxrpa
    !
    scf_exxrpa = .TRUE.
    !
    vc_rpa  = .TRUE.
    vx_exx  = .TRUE.
    turn_oep_on = .TRUE.
    acfdt_in_pw = .TRUE.    ! one activates solving KS eqs in ./PW/src/non_scf.f90  
    !
    vxc_by_invert_chi = approx_chi0
    vxc_by_iteration  = .not.approx_chi0
    !
  ELSEIF (oep_method == 3) THEN ! scf hfrpa
    !
    hf_scfrpa   = .TRUE. 
    !
    vc_rpa  = .TRUE.
    vx_exx  = .FALSE.       ! no need to compute vx_oep in a scf hfrpa calculation
    !
    turn_oep_on = .TRUE.
    acfdt_in_pw = .FALSE.   ! one deactivates solving KS eqs in ./PW/src/non_scf.f90  
    ! 
  ELSE
    !
    CALL errore ('acfdt_readin', 'oep_method wrong', 1 )
    !
  ENDIF
  !
  ! ... oep checking is done ...
  !
  !  If there is a restart for OEP procedure, code will read the xc(RPA) potential 
  !  or effective potential that has saved in previous oep runs. The input of previous orbitals
  !  have been read above. However, the enforce_dft_exxrpa() is specified to be LDA/GGA (in read_file() ),
  !  it will be modified in future for this stupid problem. In present, we are correcting this by simply subtract vxc
  !  before call set_vrs()
  !
  ! here we are updating a recover effective potential from previous run
  !
  IF(turn_oep_on) THEN 
    !
    ALLOCATE ( vrs_save(dfftp%nnr, nspin) )
    ! 
    IF (oep_recover) THEN
       !
       IF(.not.stopped_first_iter) THEN
         !
         dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
         CALL read_rho (dirname, vrs_save, nspin, 'potential_KS_effective_pot')
         write(stdout,*) vrs_save(1:3,nspin)
         !
       ENDIF
       ! 
    ENDIF
    !
  ENDIF
  !
  RETURN
  !
END SUBROUTINE acfdt_readin
