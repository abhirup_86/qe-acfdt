!
! Copyright (C) 2001-2010 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
! 
!-----------------------------------------------------------------------
subroutine acfdt_summary
  !-----------------------------------------------------------------------
  !
  !    This routine writes on output the quantities which have been read
  !    from the punch file, and the quantities computed in the phq_setup
  !    file.
  !
  !    if iverbosity = 0 only a partial summary is done.
  !
  !
  USE kinds,         ONLY : DP
  USE ions_base,     ONLY : nat, ntyp => nsp 
  USE io_global,     ONLY : stdout
  USE cell_base,     ONLY : ibrav, alat, omega
  USE gvecs,         ONLY : dual 
  USE gvecw,         ONLY : ecutwfc
  USE funct,         ONLY : write_dft_name
  USE run_info,      ONLY : title
  USE control_ph,    ONLY : nmix_ph, alpha_mix, tr2_ph 
  USE lr_symm_base,  ONLY : minus_q, nsymq 
  USE qpoint,        ONLY : xq
  USE control_acfdt, ONLY : wq
  
  implicit none

  integer :: i, mu, nu, ipol, apol, na, isymq, isym, nsymtot, &
       ik, irr, imode0, iu
  ! generic counter
  ! counter on modes
  ! counter on modes
  ! counter on polarizations
  ! counter on polarizations
  ! counter on atoms
  ! counter on symmetries
  ! counter on symmetries
  ! counter on symmetries
  ! counter on k points
  ! counter on beta functions
  ! counter on irreducible representation
  ! the first mode

  real(DP) :: ft1, ft2, ft3, xkg (3)
  ! fractionary translations
  ! k point in crystal coordinates

  !
  WRITE( stdout, 100) title, ibrav, alat, omega, nat, ntyp, &
       ecutwfc, ecutwfc * dual, tr2_ph, alpha_mix (1), &
       nmix_ph
100 format (/,5x,a75,/,/,5x, &
       &     'bravais-lattice index     = ',i12,/,5x, &
       &     'lattice parameter (a_0)   = ',f12.4,'  a.u.',/,5x, &
       &     'unit-cell volume          = ',f12.4,' (a.u.)^3',/,5x, &
       &     'number of atoms/cell      = ',i12,/,5x, &
       &     'number of atomic types    = ',i12,/,5x, &
       &     'kinetic-energy cut-off    = ',f12.4,'  Ry',/,5x, &
       &     'charge density cut-off    = ',f12.4,'  Ry',/,5x, &
       &     'convergence threshold     = ',1pe12.1,/,5x, &
       &     'beta                      = ',0pf12.4,/,5x, &
       &     'number of iterations used = ',i12)

  CALL write_dft_name ( )
  !
  !
     WRITE( stdout, '(/,5x,"Computing contribution to RPA Ec at")')  
     WRITE( stdout, '(20x,"q = (",3f12.7," )", 3x, "wq = (",f12.7," )")') &
                     (xq (ipol) , ipol = 1, 3), wq
  !
  !   description of symmetries
  !
  WRITE( stdout, * )
  if (nsymq.le.1.and..not.minus_q) then
     WRITE( stdout, '(5x,"No symmetry!")')
  else
     if (minus_q) then
        WRITE( stdout, '(5x,i2," Sym.Ops. (with q -> -q+G )",/)') &
             nsymq + 1
     else
        WRITE( stdout, '(5x,i2," Sym.Ops. (no q -> -q+G )",/)') nsymq
     endif
  endif
  !
  !
  !
  write(stdout,'(/)')
  !
  FLUSH(stdout)
  !
  return
end subroutine acfdt_summary
