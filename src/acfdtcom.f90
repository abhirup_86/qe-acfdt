!
! Copyright (C) 2001-2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
!
! ... Common variables for the  acfdt program
!
!
MODULE control_acfdt
  !
  USE kinds,        ONLY : DP
  !
  SAVE
  !
  INTEGER :: irr0             ! For restart
  !
  REAL (DP) :: wq             ! Weight of q point
  REAL (DP) :: mixpot_beta    ! MIX potential I/O
  !
  ! ... the variables for computing frequency dependent dielectric constant
  !     and Ec in EXX/RPA scheme.
  !
  LOGICAL :: savealldep,        &   ! If .TRUE., all eigenpotentials at each frequency will be written to disk
             veff_conv,         &   ! true if iter. procedure for Veff converges
             vc_rpa,            &   ! true if rpa correlation potential will be done
             vx_exx,            &   ! true if exact exchange potential is calculated
             lepsi,             &   ! if .true., diagonalizing epsilon_rpa-1, else Xo
             pot_collect,       &   ! if .true., collect potentials to head node before writing to disk
             chi0_is_solved=.false.,    &   ! .true. if eigenpotentials of Xo were already computed
             control_vc_rpa,    &   ! true if need to calculate variation wfcs from output potential.
             potcorr,           &   ! true if need to correct Vxc(EXX/RPA) with Vxc(LDA/GGA)    
             approx_chi0,       &   ! read from input 
             rpax,              &   
             inverted_chi           ! true if approx_chi0 is actived
  !
  INTEGER :: idiag         ! 1 for Davidson diagonalization algorithm
                           ! 2 for orthogonal iterative algorithm 

  INTEGER :: nf_ec, &      ! number of freq. used to compute Ec in ACFDT formalism
             iumax, &
             neigv, &      ! number of eigenvalues in calculation of Ec
             nbvef, &      ! number of bands for Veff
             neigvchi0     ! number of eigenvalues of chi0 matrix at (iu=0)
           
  REAL(DP):: u0_ec, &      ! parameter of transformation u=u0*(1-t)/(1+t) for Ec
                           !u0_c6, & parameter of transformation u=u0*(1-t)/(1+t) for C6
             thr_ec,  &    ! threshold for iterative diagonalization of KS Resp. Funct.
             thr_veff,&    ! threshold for effective potential
             thr_oep       ! threshold for oep convergency 

  INTEGER, PARAMETER :: nevlx = 500         ! max. eigval can be used in Ec calculation
  !
  INTEGER :: nlambda
  !
  REAL(DP), ALLOCATABLE :: t_nk(:), w_nk(:), &
                           tu_ec(:), u_ec(:), wu_ec(:), &
                           tl_ec(:), l_ec(:), wl_ec(:)
  ! Abscissas t (-1<t<1) in Gauss-Legendre integration
  ! Weight w in Gauss-Legendre integration
  ! frequency u corresponding to t for C6
  ! frequency u corresponding to t for Ec
  ! coupling constant \lambda corresponding to t
  !
  LOGICAL :: efieldper                         ! true if per. is an electric field
  REAL(DP):: lowest_eigchi0
  REAL   (DP), ALLOCATABLE :: dvgen(:,:)       ! real pertubation potential
  REAL   (DP), ALLOCATABLE :: eigchi0(:)       ! eigenvalues of Xo
  COMPLEX(DP), ALLOCATABLE :: dvgenc(:,:,:), & ! complex pertubation potential
                              depot(:,:),    & ! dielectric eigenpotentials
                              depotchi0(:,:),& ! chi0 eigenpotentials
                              dvgg(:)          ! workspace 
  !
  COMPLEX (DP), ALLOCATABLE :: dvdpsi(:,:), psi_2(:,:)
  ! the product of dVdpsi
  ! the change of the wavefunctions
  !
  COMPLEX (DP), ALLOCATABLE :: dchi_iu(:)  ! this global variable equal to dchi0 at iu
  COMPLEX (DP), ALLOCATABLE :: dchi_iu3(:) ! this global variable equal to dchi03 at iu
  COMPLEX (DP), ALLOCATABLE :: dchi_iu12(:)! this global variable equal to dchi012 at iu
  !                                           
  COMPLEX (DP), ALLOCATABLE :: summ_vc(:,:)  !  
  COMPLEX (DP), ALLOCATABLE :: summ_vc3(:,:) ! 
  COMPLEX (DP), ALLOCATABLE :: summ_vc12(:,:)!
  ! 
  COMPLEX (DP), ALLOCATABLE :: sum_dchi(:)   ! 
  COMPLEX (DP), ALLOCATABLE :: sum_dchi3(:)  ! 
  COMPLEX (DP), ALLOCATABLE :: sum_dchi12(:) ! 
  !
  COMPLEX (DP), ALLOCATABLE :: dpsi_plus(:,:), dpsi_minus(:,:), fockevc(:,:)
  COMPLEX (DP), ALLOCATABLE, TARGET :: dpsi_nl(:,:)
  COMPLEX (DP), POINTER ::  dpsiq_nl(:,:)
  REAL(DP),     ALLOCATABLE :: exxvx(:), rpavc(:)
  REAL(DP) :: ex_exx, ecrpa
  !
  ! these variables are for RPA+ stuffs
  REAL(DP) :: etx, etc, ec_sr 
  !
  REAL(DP) :: vtx_exx, vtc_rpa, delta_vtxc, delta_etxc
  REAL(DP) :: vtx_init, vtc_init, etc_init, etx_init
  REAL(DP), ALLOCATABLE :: vc_init(:,:), vx_init(:,:), delta_vxc(:), delta_vxc_old(:)
  !
  REAL(DP) :: old_etot=0.0_DP
  !
  INTEGER  :: infodia
  INTEGER  :: iundedv, iundvr, iundvw, iunrec_ec, iunngmg, iuneig, lreig, iudchi, lrdchi, iuchi0
  INTEGER  :: iunfx, lrfx
  INTEGER  :: unitchi0, chi0rec
  !
  REAL(DP) :: tau1(3), tau2(3)
  CHARACTER(LEN=256) :: depdir1, depdir2
  !
  ! these variables are for OEP proceduce
  LOGICAL  :: turn_oep_on                    ! on/off OEP procedure 
  INTEGER  :: oep_maxstep, max_oep_space
  REAL (DP):: ecut_aux
  REAL (DP), ALLOCATABLE :: save_exx(:), save_rpa(:), save_rpa12(:)
  REAL (DP), ALLOCATABLE :: save_rho(:)      ! this saved rho of previous oep step
  REAL (DP), ALLOCATABLE :: save_vloc (:,:)  ! this saved vrs of KS is used in mixing
  REAL (DP):: c_mix, norm_gradient
  !
END MODULE control_acfdt
!
! ... Common variables for the  acfdt non_pwscf program
!
MODULE acfdt_scf
  !
  USE kinds,        ONLY : DP
  !
  SAVE  
  !
  INTEGER  :: iovrs
  INTEGER  :: iter_save
  INTEGER  :: iter_save0     ! the restarting iteration
  INTEGER  :: oeprec = 1000  ! record of saved information file
  REAL(DP) :: lambda
  REAL(DP) :: input_lambda
  REAL(DP) :: etot_old
  REAL(DP) :: grad
  REAL(DP) :: gradsave
  LOGICAL  :: conjug_update
  LOGICAL  :: changed_lambda = .false.
  !
  ! ... for restarting
  !
  LOGICAL  :: oep_recover 
  LOGICAL  :: oep_restart_flag
  LOGICAL  :: restart_from_scratch = .false.
  LOGICAL  :: stopped_first_iter   = .false.
  LOGICAL  :: chi0_done = .false.
  LOGICAL  :: rpa_done = .false.
  LOGICAL  :: oep_done = .false.
  !
  LOGICAL  :: scf_rpa_plus         = .false.
  !
  ! .. for estimating potential
  !
  REAL(DP), ALLOCATABLE :: vrs_save (:,:)
  REAL(DP), ALLOCATABLE ::     dv_g (:,:)
  REAL(DP), ALLOCATABLE :: dv_update(:,:)
  !
  ! ... for saving KS density @ 1st step.
  !
  REAL(DP), ALLOCATABLE :: rho_1step(:,:)
  !
  REAL(DP), ALLOCATABLE ::    dn_1step(:)
  REAL(DP), ALLOCATABLE ::  vxx_guess(:)
  REAL(DP), ALLOCATABLE :: savedv_g (:,:)
  !
  ! ... for OEP 
  INTEGER  :: oep_method  
  LOGICAL  :: scf_exxrpa   = .false.
  LOGICAL  :: hf_scfrpa    = .false.
  !
  ! ... for ways to approx vxc
  !
  LOGICAL  :: vxc_by_invert_chi = .false.
  LOGICAL  :: vxc_by_iteration  = .false.
  !
  LOGICAL  :: line_search  = .false.
  LOGICAL  :: poly_search
  LOGICAL  :: print_result = .true.
  LOGICAL  :: print_save_result = .true.
  INTEGER  :: iter_oep = 0
  REAL(DP) :: save_norm
  REAL(DP) :: save_norm_1
  REAL(DP) :: save_norm_2
  REAL(DP) :: lambda_opt
  LOGICAL  :: do_acfdt = .true.
  LOGICAL  :: print_xc_pots = .false.
  INTEGER  :: line_search_index = 1 
  !
  ! ... 1. for OEP semidirect ... 
  !
  COMPLEX(DP), ALLOCATABLE ::  oeppot_g(:,:)
  COMPLEX(DP), ALLOCATABLE :: oepdpot_g(:,:)
  COMPLEX(DP), ALLOCATABLE ::   oepdf_g(:,:)
  COMPLEX(DP), ALLOCATABLE :: oepvcdf_g(:,:)
  ! 
  ! ... 2. for OEP direct ... 
  REAL(DP), ALLOCATABLE    :: b_in   (:)
  ! 
  ! ... for sum over q points Vcrpa potentials 
  !
  COMPLEX(DP), ALLOCATABLE :: sum_q_dden(:,:)
  COMPLEX(DP), ALLOCATABLE :: sum_q_pot (:,:)
  !
  !
END MODULE acfdt_scf


