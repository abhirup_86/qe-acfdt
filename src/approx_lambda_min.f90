!
! Copyright (C) 2001-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!---------------------------------------------------------------
SUBROUTINE approx_lambda_min (lambda, lambda_out ,iter, dv_lam0)
  !-------------------------------------------------------------
  !
  ! This routine is used to find a lambda0 that gives minimum 
  ! energy at a certain pertubation potential, dV.
  ! Method used here is Secant Method: 
  ! It reads derivatives (g1 and g2) at 2 point, 
  ! \lambda = 0 and \lambda = \lambda_1,
  ! then intrapolates: \lambda_0 = g1*\lambda_1/(g1-g2)
  !
  USE kinds,           ONLY : DP
  USE io_global,       ONLY : stdout
  USE mp,              ONLY : mp_sum
  USE mp_global,       ONLY : intra_pool_comm
  USE lsda_mod,        ONLY : nspin
  USE fft_base,        ONLY : dfftp
  USE acfdt_scf,       ONLY : grad, dv_g, dv_update, conjug_update
  !
  IMPLICIT NONE
  !
  INTEGER :: i
  REAL(DP), INTENT(IN) :: lambda
  REAL(DP), INTENT(INOUT) :: lambda_out
  REAL(DP), INTENT(INOUT) :: dv_lam0(dfftp%nnr, nspin) 
  INTEGER , INTENT(IN)    :: iter
  !
  REAL(DP) :: g1, g2, sum_tmp 
  REAL(DP) :: dv(dfftp%nnr, nspin) 
  !
  IF (.NOT.conjug_update) THEN
    dv_update (:,1) = 0.0_DP
    dv_update (:,1) = dv_g(:,1)     
  ENDIF
  !
  write(stdout,*) dv_lam0(1:3,1)
  write(stdout,*) dv_update(1:3,1)
  sum_tmp = 0.0_DP
  DO i = 1, dfftp%nnr
     sum_tmp = sum_tmp + dv_lam0(i,1) * dv_update(i,1)
  ENDDO
  !
#if defined __MPI
   CALL mp_sum( sum_tmp, intra_pool_comm )
#endif  
  g1 = sum_tmp
  ! 
  write(stdout,*) "update lambda1", g1, sum_tmp
  CALL gradient_potential(dv, .false.,.false.) 
  !
  sum_tmp = 0.0_DP
  DO i = 1, dfftp%nnr 
     sum_tmp = sum_tmp + dv(i,1) * dv_update(i,1)
  ENDDO
  !
#if defined __MPI
   CALL mp_sum( sum_tmp, intra_pool_comm )
#endif  
  ! 
  g2 = sum_tmp
  write(stdout,*) "update lambda22", g1, (g1-g2), g2
  !
  lambda_out = g1*lambda/(g1-g2) 
  write(stdout,*) lambda_out
  !   
  RETURN
  !
END SUBROUTINE approx_lambda_min
