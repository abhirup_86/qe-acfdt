
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!=========================================================
SUBROUTINE approx_vxc_by_anderson (scf_iter, den_nl, pot_best)
 !========================================================
 !
 ! 1) estimate the initial guess for  (Vexxrpa(lda/gga))
 ! 2) estimate the error density, then use it to update new pottential
 ! 3) combine a Adenson's Mixing method to find minimize 
 !    module of error density.
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE cell_base,     ONLY : omega
 USE gvect,         ONLY : ngm, nl
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE control_acfdt, ONLY : dvgenc, vx_init, vc_init, control_vc_rpa, &
                           thr_oep, max_oep_space, norm_gradient 
 USE acfdt_scf,     ONLY : line_search
 USE scf,           ONLY : rho, rho_core, rhog_core
 USE xml_io_base,   ONLY : read_rho, write_rho, create_directory
 USE io_files,      ONLY : tmp_dir, prefix
  !
  IMPLICIT NONE
  !
  ! I/O variables
  INTEGER, INTENT(IN) :: scf_iter
  COMPLEX(DP),INTENT(IN)    ::   den_nl(dfftp%nnr)
  COMPLEX(DP),INTENT(INOUT) :: pot_best(dfftp%nnr)
  !
  !local variables
  !
  LOGICAL :: convert
  !
  REAL(DP) :: c_mix, beta_mix 
  INTEGER  :: ndim
  !
  INTEGER  :: max_iter, max_subspace
  !    
  INTEGER  :: max_m, niter, iter_used, idum, jdum, i, j, k
  !
  REAL(DP),    ALLOCATABLE :: numer(:)
  REAL(DP),    ALLOCATABLE :: pot_loc(:)
  COMPLEX(DP), ALLOCATABLE :: den_0(:)
  !
  REAL(DP) :: save_tmp(dfftp%nnr, nspin)
  !
  COMPLEX(DP), ALLOCATABLE :: den_gnl(:), den_g0(:), pot_g0(:), df_g0(:)
  COMPLEX(DP), ALLOCATABLE :: den_g(:,:), pot_g(:,:), dden_g(:)
  COMPLEX(DP), ALLOCATABLE :: df_g(:,:),  dpot_g(:,:), vcdf_g(:,:)
  COMPLEX(DP), ALLOCATABLE :: pot_gbest(:), sum_pot_g(:), dpot_update_g(:) 
  !
  ! auxiliary arrays for FFT
  !
  COMPLEX(DP), ALLOCATABLE :: aux (:)
  COMPLEX(DP), ALLOCATABLE :: aux1(:), aux2(:), aux3(:)
  !
  COMPLEX(DP) :: sum_tmp
  REAL(DP)    :: normdot  
  REAL(DP)    :: etx_init, vtx_init, etc_init, vtc_init
  !
  INTEGER :: info
  REAL(DP):: alpha 
  REAL(DP),ALLOCATABLE :: alphamix(:,:), work(:)
  INTEGER, ALLOCATABLE :: iwork(:)
  CHARACTER(LEN=256) :: dirname
  !
  dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
  max_iter = max_oep_space
  max_subspace = max_oep_space
  !
  control_vc_rpa = .FALSE.
  !
  ndim = dfftp%nnr
  !
  ALLOCATE(aux(ndim), aux1(ndim), aux2(ngm), aux3(ngm))
  ALLOCATE(pot_gbest(ngm), pot_loc(ndim), den_0(ndim) )
  ALLOCATE(pot_g0(ngm), den_g0(ngm), den_gnl(ngm), df_g0(ngm) )
  !
  c_mix = 3
  beta_mix = 1
  !
  !---guess initial rpa+exx potential----!
  !
  IF (scf_iter == 1) THEN
     !
     vx_init(:,:) = 0.0_DP; vc_init(:,:) = 0.0_DP
     CALL vxc_init( rho, rho_core, rhog_core, &
                    etx_init, vtx_init, etc_init, vtc_init, vx_init, vc_init )
     pot_loc(:) = 0.0_DP; pot_loc(:) = vc_init(:, nspin )
     ! 
  ELSE
     !
     save_tmp(:,:) = 0.0_DP;
     ! ... read from disk 
     CALL read_rho(dirname, save_tmp, nspin, "vxxrpa_old")
     pot_loc(:)  = 0.0_DP; pot_loc(:) = save_tmp(:,nspin)
     !
  ENDIF
  ! 
  dvgenc(:,:,:)  = ZERO; dvgenc(:,1,1) = CMPLX(pot_loc(:),0.D0, kind=DP)
  CALL solve_linter_iu ( 0.0_DP )
  IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
  den_0(:) = ZERO; den_0(:)   = dvgenc(:,1,2)
  !
  !-------------------------------------------------------------------------------!
  !   THIS PART CONSTRIBUTES TO GRADIENT (dE/dV) EVALUATION DURING OEP STEPS      !
  !-------------------------------------------------------------------------------!
  aux(:) = ZERO; aux (:)  = den_nl(:) - den_0(:)
  !
  normdot = 0.0_DP
  DO i = 1, ndim
     normdot = normdot +  CONJG(aux(i)) * aux(i)
  ENDDO
  normdot = normdot * omega/((dfftp%nr1 * dfftp%nr2 * dfftp%nr3))
#if defined __MPI
  CALL mp_sum( normdot, intra_pool_comm )
#endif    
  norm_gradient = norm_gradient + normdot
  ! 
  !---------------------------------------------------------------------------------!
  !
  ! pass den_0 -> den_g0
  ! pass den_nl-> den_gnl
  ! pass pot_0 -> pot_g0
  !
  aux1(:)   = ZERO; aux1(:)    = den_nl(:)
  !go back to G-space
  CALL fwfft ('Dense', aux1, dfftp)
  den_gnl(:)= ZERO; den_gnl(:) = aux1(nl(:)) 
  !
  aux1(:)   = ZERO; aux1(:)    = den_0(:)
  !go back to G-space
  CALL fwfft ('Dense', aux1, dfftp)
  den_g0(:) = ZERO; den_g0(:)  = aux1(nl(:)) 
  !
  aux1(:)   = ZERO; aux1(:)    = CMPLX(pot_loc(:),0.D0, kind=DP)
  !go back to G-space
  CALL fwfft ('Dense', aux1, dfftp)
  pot_g0(:) = ZERO; pot_g0(:)  = aux1(nl(:))   
  !
  ! df_g0 = den_gnl - den_g0
  df_g0 (:) = ZERO; df_g0 (:)  = den_gnl(:) - den_g0(:) 
  !
  ALLOCATE(pot_g(max_iter,ngm), den_g(max_iter,ngm))
  ALLOCATE(dden_g(ngm), sum_pot_g(ngm), dpot_update_g(ngm))
  pot_gbest(:) = ZERO; pot_g(:,:) = ZERO; den_g(:,:) = ZERO
  !
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%          iterate !          %%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
100 continue
  ! 
  niter     = max_iter
  iter_used = 0
  DO idum = 1, niter 
    !
    iter_used = iter_used + 1
    !
    ! estimate the dimension of subspace
    ! 
    !IF(iter_used <= max_subspace) THEN
      max_m = iter_used
    !ELSE
    !  max_m = max_subspace
    !ENDIF
    !
    IF (iter_used==1) THEN
      pot_g (iter_used,:) =  1.02*pot_g0(:)
      den_g (iter_used,:) =  1.02*den_g0(:)
    ENDIF
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ALLOCATE (vcdf_g(max_m, ngm), dpot_g(max_m, ngm), df_g(max_m, ngm), numer(max_m)) 
    vcdf_g(:,:) = ZERO; dpot_g(:,:) = ZERO; df_g(:,:) = ZERO; numer(:) = 0.0_DP
    DO jdum = 1, max_m
       !
       ! |pot_i> - |pot_{0}> 
       !
       dpot_g(jdum,:) = pot_g(iter_used-(jdum-1),:) - pot_g0(:)
       !
       ! |rho_i> - |rho_{0}>
       !
       df_g (jdum, :) = den_g(iter_used-(jdum-1),:) - den_g0(:) 
       !
       ! vc|df_g>
       aux2(:) = ZERO; aux2(:) = df_g(jdum,:)
       CALL vc_gorling( ngm, aux2 )
       vcdf_g(jdum, :) = aux2(:)
       !
       sum_tmp = ZERO
       DO i = 1, ngm
         sum_tmp = sum_tmp + CONJG(df_g0(i))*vcdf_g(jdum,i)  
       ENDDO
#if defined __MPI
       CALL mp_sum( sum_tmp, intra_pool_comm )
#endif     
       numer(jdum) = DBLE(sum_tmp)
       !
    ENDDO  
    !  
    IF (iter_used ==1) THEN 
     !
     sum_pot_g(:) = ZERO
     GOTO 300
     !
    ENDIF
    !
    ALLOCATE(alphamix(max_m, max_m)) !iter_used))
    alphamix(:,:) = 0.0_DP
    DO i = 1, max_m 
      DO j = i, max_m
        sum_tmp = ZERO
        DO k= 1, ngm
           sum_tmp = sum_tmp + CONJG(df_g(i,k))*vcdf_g(j,k)
        ENDDO
#if defined __MPI
        CALL mp_sum( sum_tmp, intra_pool_comm )
#endif 
        alphamix(i,j) = DBLE(sum_tmp)
        alphamix(j,i) = alphamix(i,j)
      ENDDO
    ENDDO
    !
!!!!! Aij^-1 
    ! 
    ALLOCATE(work(max_m), iwork(max_m))
    CALL DSYTRF( 'U', max_m, alphamix , max_m, iwork, work, max_m, info )
    CALL errore( 'broyden', 'factorization', abs(info) )
    ! 
    CALL DSYTRI( 'U', max_m, alphamix , max_m, iwork, work, info )
    CALL errore( 'broyden', 'DSYTRI', abs(info) )    !
    DEALLOCATE(iwork)
    !
!!!!!
    !
    FORALL( i = 1:max_m, &
            j = 1:max_m, j > i ) alphamix(j,i) = alphamix(i,j)
    !
    DO i = 1, max_m
     !
     work(i) = numer(i)
     !
    ENDDO
    !
    sum_pot_g(:) = ZERO
    DO i = 1, max_m
      alpha = 0.0_DP
      DO j = 1, max_m
        alpha = alpha + (alphamix(i,j)* work(j) )
      ENDDO 
      sum_pot_g(:) = sum_pot_g(:) + alpha * dpot_g(i,:)
    ENDDO
    DEALLOCATE(alphamix, work)
    !
300 continue
    !
    DEALLOCATE (vcdf_g, dpot_g, df_g, numer)
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    pot_gbest(:) = pot_g0(:) + beta_mix*sum_pot_g(:)
    !
    !update new pot_i and dden_i
    !
    !put on the FFT grid
    aux1(:) = ZERO; aux1(nl(:)) = pot_gbest(:)
    !go back to R-space
    CALL invfft ('Dense', aux1, dfftp)
    !
    dvgenc(:,:,:) = ZERO; dvgenc(:,1,1) = aux1(:)
    CALL solve_linter_iu ( 0.0_DP )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    !
    aux1(:) = ZERO; aux1(:) = dvgenc(:,1,2)
    !go to G-space
    CALL fwfft ('Dense', aux1, dfftp)
    aux2(:) = ZERO; aux2(:) = aux1(nl(:))
    !
    !den_best is aux2(:)
    !
    aux2(:)   =  den_gnl(:) - aux2(:)
    !
    aux3(:)   = ZERO; aux3(:) = aux2(:) 
    !---updated potential needs -> 0 at ifinity----! 
    !
    !vc|aux2>
    CALL vc_gorling ( ngm, aux2 )
    !
    dpot_update_g(:) = ZERO; dpot_update_g(:) = aux2(:)
    !
    !---------------------------------------------!
    ! 
    !------estimate a exit for iteration----------!
    !
    !normalize density
    CALL norm_newdden(ngm, aux3, normdot)
    !
    write(stdout,*) "normalization of error density:", iter_used, max_m, normdot
    ! checking convergency..., using the thr_oep as a referency 
    convert = (normdot < thr_oep)
    IF (convert .and. iter_used.lt.(max_iter-1)) THEN
      write(stdout,*) "convergency is approach after", iter_used, "steps"
      write(stdout,*) "normalization of error density:", normdot
      EXIT
    ELSEIF (.not.convert .and. (iter_used.eq.(max_iter-1))) THEN
      write(stdout,*) "convergency is not approach"
      write(stdout,*) "normalization of error density:", iter_used, max_m, normdot
      EXIT
    ELSE
      
    ENDIF  
    !
    !----------------------------------------------!
    !
    !-----------new potential for next step--------!
    !
    pot_g(iter_used+1,:) = pot_gbest(:) + c_mix * dpot_update_g(:)
    !
    !put on the FFT grid
    aux1(:) = ZERO; aux1(nl(:)) =  pot_g(iter_used+1,:)
    !go back to R-space
    CALL invfft ('Dense', aux1, dfftp)
    !
    dvgenc(:,:,:) = ZERO; dvgenc(:,1,1) = aux1(:)
    CALL solve_linter_iu ( 0.0_DP )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    !
    aux1(:) = ZERO; aux1(:) = dvgenc(:,1,2)
    !go to G-space
    CALL fwfft ('Dense', aux1, dfftp)
    aux2(:) = ZERO; aux2(:) = aux1(nl(:))
    ! 
    !-----------new density for next step----------!
    den_g(iter_used+1,:) = ZERO; den_g(iter_used+1,:) = aux2(:)
    !
  ENDDO 
  ! STOP ITERATION ...
  !
  DEALLOCATE(pot_g, den_g, dden_g, sum_pot_g, dpot_update_g)
  !
9999 CONTINUE
  !
  !put on the FFT grid
  aux1(:) = ZERO; aux1(nl(:)) = pot_gbest(:)
  !go back to R-space
  CALL invfft ('Dense', aux1, dfftp)
  !
  pot_best(:) = ZERO; pot_best(:) = aux1(:)
  !
  ! ----------------------------------------------------------------!
  ! 
  ! ... saving and returning  the potential
  !
  ! ... here, the potential is saved again to used in the next OEP cycle
  !
  ! ... write on disk, when lamda is the optimized value (or linear_search = .false.)
  !
  IF(.not.line_search) THEN 
    save_tmp(:,:) = 0.0_DP;  save_tmp(:,nspin) = DBLE(pot_best(:)) 
    CALL create_directory( dirname )  ! NsC This was autohomatically done in write_rho in v6.1
    CALL write_rho(dirname, save_tmp, nspin, "vxxrpa_old")
  ENDIF
  !
  !
  DEALLOCATE ( aux, aux1, aux2, aux3)
  DEALLOCATE ( pot_g0, den_gnl, den_g0, df_g0)
  DEALLOCATE ( pot_gbest, pot_loc, den_0 )
  !
  RETURN
  !
END SUBROUTINE approx_vxc_by_anderson
