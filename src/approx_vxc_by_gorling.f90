! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!
! This routines includes the algorithm for sloving OEP problems
! The main ideas will be employed that proposed by A.Gorling
!
!
!===============================================
SUBROUTINE approx_vxc_by_gorling (den_nl, pot_loc)
 !==============================================
 !
 ! This algorithm is refered in A. Gorling PRL 83, 5459 (1999).
 ! That is applied for looking a local potential from input density.
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE qpoint,        ONLY : xq 
 USE gvect,         ONLY : ngm, nl
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE control_acfdt, ONLY : neigvchi0, depotchi0, iuchi0, & 
                           dvgenc, control_vc_rpa
  !
  IMPLICIT NONE
  !
  ! I/O variables
  COMPLEX(DP), INTENT(IN)    :: den_nl(dfftp%nnr)
  COMPLEX(DP), INTENT(INOUT) :: pot_loc(dfftp%nnr) 
  !
  !local variables
  !
  INTEGER     :: i, j, k, max_m, ndim
  REAL(DP)    :: normdot
  COMPLEX(DP) :: sum_tmp
  !
  COMPLEX(DP), ALLOCATABLE :: potvc(:,:), denvc(:,:), den_gnl(:), sum_pot_g(:)
  !
  INTEGER :: info
  REAL(DP):: alpha
  REAL(DP),ALLOCATABLE :: alphamix(:,:), work(:), numer(:), aux2(:)
  INTEGER ,ALLOCATABLE :: iwork(:)

  COMPLEX(DP), ALLOCATABLE :: aux (:), aux1(:)
  ! auxiliary arrays for FFT
  !
  control_vc_rpa = .FALSE.
  ndim = dfftp%nnr 
  !
  ALLOCATE(aux(ngm), aux1(ndim), aux2(ndim))
  ALLOCATE(potvc(ngm, neigvchi0), denvc(ngm, neigvchi0), den_gnl(ngm))
  !
  !----------------------------------------------------------------------------!
  !                             ! OEP for GENERAL POTENTIALS                   !
  !----------------------------------------------------------------------------!
  !
  ! pass the den_nl to G-space
  !
  !aux1(:)   = ZERO; aux1(:) = CMPLX(den_nl(:),0.D0, kind=DP)
  aux1(:)   = ZERO; aux1(:) = den_nl(:)
  !go back to G-space
  CALL fwfft ('Dense', aux1, dfftp)
  den_gnl(:) = ZERO; den_gnl(:) = aux1(nl(:))
  !
  !----------------------------------------------------------------------------!
  ! CONSTRUCTE THE MATRIX <vcfi|Xo|vcfj> WITH AUXILIARY EIGENVECTORS OF Xo
  !----------------------------------------------------------------------------!
  ! 
  ! ... read eigenfunctions of Xo
  !
  depotchi0 (:,:) = ZERO 
  CALL init_depot( neigvchi0, depotchi0, -1, iuchi0 )
  !
  ! ... loop over neigvchi0
  !
  DO i = 1, neigvchi0
     !
     ! pass depotchi0 to auxilary variable
     !
     aux(:) = ZERO; aux(:) = depotchi0(:,i)
     !
     ! multiple to vc is compulsory in Gorling's methods
     !
     CALL vc_gorling(ngm, aux)  
     !
     potvc(:,i) = aux(:)                ! |vcfi>(G) 
     !
     aux1(:) = ZERO; aux1(nl(:)) = aux(:)
     !
     ! go back to R-space
     !
     CALL invfft ('Dense', aux1, dfftp)
     ! 
     dvgenc(:,:,:)  = ZERO; dvgenc(:,1,1) = aux1(:)
     CALL solve_linter_iu ( 0.0_DP )
     IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
     aux1(:) = ZERO; aux1(:) = dvgenc(:,1,2)
     !
     ! go to G-space
     !
     CALL fwfft ('Dense', aux1, dfftp)
     ! 
     denvc(:,i) = aux1(nl(:))           ! Xo|vcfi>(G) 
     ! 
  ENDDO    
  !
max_m = neigvchi0 
  !
ALLOCATE(numer(max_m)) 
  DO i = 1, max_m
    sum_tmp = ZERO
    DO k = 1, ngm
      sum_tmp = sum_tmp + CONJG(potvc(k,i)) * den_gnl(k)
    ENDDO
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif     
    numer(i) = DBLE(sum_tmp)
  ENDDO
  !
ALLOCATE(alphamix(max_m, max_m)) !iter_used))
    alphamix(:,:) = 0.0_DP
    DO i = 1, max_m
      DO j = i, max_m
        sum_tmp = ZERO
        DO k = 1, ngm
          sum_tmp = sum_tmp + CONJG(potvc(k,i))* denvc(k,j)
        ENDDO
#if defined __MPI
        CALL mp_sum( sum_tmp, intra_pool_comm )
#endif     
        alphamix(i,j) = DBLE(sum_tmp)
        alphamix(j,i) = alphamix(i,j)
      ENDDO
      !
    ENDDO
    !
!!!!! Aij^-1 
    ! 
    ALLOCATE(work(max_m), iwork(max_m))
    CALL DSYTRF( 'U', max_m, alphamix , max_m, iwork, work, max_m, info )
    CALL errore( 'broyden', 'factorization', abs(info) )
    ! 
    CALL DSYTRI( 'U', max_m, alphamix , max_m, iwork, work, info )
    CALL errore( 'broyden', 'DSYTRI', abs(info) )    !
    DEALLOCATE(iwork)
    !
!!!!!
    !
    FORALL( i = 1:max_m, &
            j = 1:max_m, j > i ) alphamix(j,i) = alphamix(i,j)
    !
    DO i = 1, max_m
     !
     work(i) = numer(i)
     !
    ENDDO
    !
    ALLOCATE(sum_pot_g(ngm))
      sum_pot_g(:) = ZERO
      DO i = 1, max_m
        alpha = 0.0_DP
        DO j = 1, max_m
          alpha = alpha + (alphamix(i,j) * work(j) )
        ENDDO
        sum_pot_g(:) = sum_pot_g(:) + alpha * potvc(:,i) 
      ENDDO
    DEALLOCATE(alphamix, work, numer)
    !
    !put on the FFT grid
    aux1(:) = ZERO; aux1(nl(:)) = sum_pot_g(:)
    !go back to R-space
    CALL invfft ('Dense', aux1, dfftp)
    !
    DEALLOCATE(sum_pot_g)
    !----------------------------------------------------------------------------!
    !                              FINISHED
    !----------------------------------------------------------------------------!
    !
    ! return the local potential 
    !
    pot_loc(:)  = ZERO; pot_loc(:)   = aux1(:) 
    !
    !
  DEALLOCATE ( aux , aux1 , aux2)
  DEALLOCATE ( potvc, denvc, den_gnl)
  !
 RETURN
 !
END SUBROUTINE approx_vxc_by_gorling
!===============================
!
!------------------------------------------------------------------
SUBROUTINE solve_oep_gorling_simple( iflag , scf_iter)
 !-----------------------------------------------------------------
 !
 ! ----------------------------------------
 !  IF iflag == 1 OEP for EXX
 !  IF iflag == 2 OEP for RPA
 ! ----------------------------------------
 ! 
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE cell_base,     ONLY : omega
 USE qpoint,        ONLY : xq
 USE fft_base,      ONLY : dfftp
 USE control_acfdt, ONLY : dvgenc, control_vc_rpa, exxvx, rpavc, sum_dchi, &
                           norm_gradient, save_exx, save_rpa
  !
  IMPLICIT NONE
  !
  ! I/O variables
  INTEGER, INTENT(IN) :: iflag, scf_iter
  !
  !local variables
  !
  INTEGER  :: i, ndim
  REAL(DP) :: normdot  
  !
  REAL(DP), ALLOCATABLE :: den_nl(:) , den_0(:)
  REAL(DP), ALLOCATABLE :: pot_loc(:), pot_best(:)
  !
  REAL(DP),    ALLOCATABLE :: aux (:) 
  ! auxiliary arrays for FFT
  !
  control_vc_rpa = .FALSE.
  ndim = dfftp%nnr
  !
  ALLOCATE(aux(ndim))
  ALLOCATE(pot_best(ndim), den_nl(ndim), pot_loc(ndim), den_0(ndim) )
  !
  !----------------------------------------------------------------------------!
  !                                ! OEP for EXX !                             !
  !----------------------------------------------------------------------------!
  !
  !---density of exx; dn_nl = dExx/dv ---!
  !
  IF(iflag==1) THEN
    !
    norm_gradient = 0.0_DP
    !calculate dn(r) wrt V_x Fock
    !
    dvgenc(:,:,:) = ZERO
    CALL nonloc_dfpt ( )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    den_nl(:) = 0.0_DP; den_nl(:) = DBLE(dvgenc(:,1,2))
    !
  ENDIF
  !
  !---guess initial potential----!
  !
  IF(iflag == 1)  THEN
    !
    aux(:)     = 0.0_DP; aux(:)     = den_nl(:)
    !'values of potential output |dV> = Xo^-1|dn>' 
    CALL apply_inverted_chi0 (ndim, aux, pot_loc)
    !
  ENDIF
  !
  !-------------------------------------------------------------------------------!
  !                                ! OEP for RPA !                                !
  !-------------------------------------------------------------------------------!
  !
  IF(iflag==2) THEN
    !
    ! put dn(r) = \dEc(RPA)/dv(r) wrt Ec_RPA
    !
    den_nl(:) =  DBLE(sum_dchi(:))
    !
    aux(:)    = 0.0_DP; aux(:)    = den_nl(:)
    !'values of potential output |dV> = Xo^-1|dn>' 
    CALL apply_inverted_chi0 (ndim, aux, pot_loc )
    !
  ENDIF
  !
  dvgenc(:,:,:)  = ZERO; dvgenc(:,1,1) = CMPLX(pot_loc(:),0.D0,kind=DP)
  CALL solve_linter_iu ( 0.0_DP )
  IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
  den_0(:) = 0.0_DP; den_0(:) = DBLE(dvgenc(:,1,2))
  !
  !
  !-------------------------------------------------------------------------------!
  !   THIS PART CONSTRIBUTES TO GRADIENT (dE/dV) EVALUATION DURING OEP STEPS      !
  !-------------------------------------------------------------------------------!
  IF(iflag == 1)  THEN
    aux(:) = 0.0_DP; aux (:)  = den_nl(:) - den_0(:)
  ENDIF
  !
  IF(iflag == 2)  THEN
    !
    aux(:) = 0.0_DP; aux (:) = den_nl(:) - den_0(:)  
    !
  ENDIF
  !
  normdot = 0.0_DP
  DO i = 1, ndim
   normdot = normdot +  aux(i)* aux(i)
  ENDDO
  normdot = normdot * omega/((dfftp%nr1 * dfftp%nr2 * dfftp%nr3))
#if defined __MPI
  CALL mp_sum( normdot, intra_pool_comm )
#endif    
  norm_gradient = norm_gradient + normdot 
  !
  !-------------------------------------------------------------------------------!
  !
  pot_best(:) = 0.0_DP; pot_best(:) = pot_loc(:) 
  !
  ! return the vc_rpa and vx_exx potentials
  !
  IF(iflag==1) THEN
    !
    exxvx(:)    = 0.0_DP; exxvx(:)    = pot_best(:)
    !
!!!!
    DO i =1, 50
     write(stdout,*) i, pot_best(i)
    ENDDO
!!!!
    ! here, the potential is saved again to used in next scf step
    save_exx(:) = 0.0_DP; save_exx(:) = exxvx(:)
    !
  ENDIF
  !
  IF(iflag==2) THEN
    ! 
    rpavc(:)  = 0.0_DP; rpavc (:)   = pot_best(:) 
!!!!
    DO i =1, 50
     write(stdout,*) i, pot_best(i)
    ENDDO
!!!!
    ! here, the potential is saved again to used in next scf step
    save_rpa(:) = 0.0_DP; save_rpa(:) = pot_best(:)
    !
  ENDIF
  !
  DEALLOCATE ( aux )
  DEALLOCATE ( pot_best, den_nl, pot_loc, den_0 )
  !
  RETURN
  !
END SUBROUTINE solve_oep_gorling_simple
!
!------------------------------------------------------------------
SUBROUTINE solve_oep_gorling_splitted( iflag , scf_iter )
 !-----------------------------------------------------------------
 ! This algorithm is refered in A. Gorling PRL 83, 5459 (1999).
 ! That is applied for splitted EXX and RPA potentials.
 ! ----------------------------------------
 !  IF iflag == 1 OEP for EXX
 !  IF iflag == 2 OEP for RPA
 ! ----------------------------------------
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE gvect,         ONLY : ngm, g, nl, gstart, nlm
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE control_acfdt, ONLY : neigvchi0, depotchi0, & 
                           dvgenc, control_vc_rpa, exxvx, rpavc, sum_dchi, &
                           norm_gradient, save_exx, save_rpa
  !
  IMPLICIT NONE
  !
  ! I/O variables
  INTEGER, INTENT(IN) :: iflag, scf_iter
  !
  !local variables
  !
  INTEGER  :: i, j, k, max_m, ndim
  REAL(DP) :: normdot
  COMPLEX(DP) :: sum_tmp
  !
  REAL(DP),    ALLOCATABLE :: den_nl(:) 
  COMPLEX(DP), ALLOCATABLE :: potvc(:,:), denvc(:,:), den_gnl(:), sum_pot_g(:)
  !
  INTEGER :: info
  REAL(DP):: alpha
  REAL(DP),ALLOCATABLE :: alphamix(:,:), work(:), numer(:), aux2(:)
  INTEGER ,ALLOCATABLE :: iwork(:)

  COMPLEX(DP), ALLOCATABLE :: aux (:), aux1(:)
  ! auxiliary arrays for FFT
  !
  control_vc_rpa = .FALSE.
  ndim = dfftp%nnr 
  !
  ALLOCATE(aux(ngm), aux1(ndim), aux2(ndim))
  ALLOCATE(potvc(ngm, neigvchi0), denvc(ngm, neigvchi0), den_nl(ndim), den_gnl(ngm))
  !
  !----------------------------------------------------------------------------!
  !                                ! OEP for EXX !                             !
  !----------------------------------------------------------------------------!
  !
  !---density of exx; dn_nl = dExx/dv ---!
  !
  IF(iflag==1) THEN
    !
    norm_gradient = 0.0_DP
    !calculate dn(r) wrt V_x Fock
    !
    dvgenc(:,:,:) = ZERO
    CALL nonloc_dfpt ( )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    den_nl(:) = 0.0_DP; den_nl(:) = DBLE(dvgenc(:,1,2))
    !
  ENDIF
  !
  !----------------------------------------------------------------------------!
  !                                ! OEP for RPA !                             !
  !----------------------------------------------------------------------------!
  !
  IF(iflag==2) THEN
    !
    ! put dn(r) = \dEc(RPA)/dv(r) wrt Ec_RPA
    !
    den_nl(:) =  DBLE(sum_dchi(:))
    !
  ENDIF
  !
  ! pass the den_nl to G-space
  !
  aux1(:)   = ZERO; aux1(:) = CMPLX(den_nl(:),0.D0,kind=DP)
  !go back to G-space
  CALL fwfft ('Dense', aux1, dfftp)
  den_gnl(:) = ZERO; den_gnl(:) = aux1(nl(:))
  !
  !-------------------------------------------------------------------------------!
  !   THIS PART CONSTRIBUTES TO GRADIENT (dE/dV) EVALUATION DURING OEP STEPS      !
  !-------------------------------------------------------------------------------!
  !IF(scf_iter > 1) THEN
    !
  !  dvgenc(:,:,:)  = ZERO; dvgenc(:,1,1) = CMPLX(delta_vxc(:),0.D0,kind=DP)
  !  CALL solve_linter_iu ( 0.0_DP )
  !  IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    !
  !  aux2(:) = 0.0_DP; aux2 (:)  = den_nl(:) - DBLE(dvgenc(:,1,2))
    !
  !ENDIF
  !normdot = 0.0_DP
  !DO i = 1, ndim
  ! normdot = normdot +  aux(i)* aux(i)
  !ENDDO
  !normdot = normdot * omega/((dfftp%nr1 * dfftp%nr2 * dfftp%nr3))
!#if defined __MPI
 ! CALL mp_sum( normdot, intra_pool_comm )
!#endif    
  !norm_gradient = norm_gradient + normdot
  !
  !-------------------------------------------------------------------------------!
  !
  !-----------------------------------------------------------------------------!
  ! CONSTRUCTE THE MATRIX <vcfi|Xo|vcfj> WITH AUXILIARY VECTORS
  !-----------------------------------------------------------------------------!
  ! 
  ! read eigenfunctions of Xo
  ! depotchi0 (:,:) = ZERO 
  ! CALL init_depot( neigvchi0, depotchi0, -1, iuchi0 )
  !
  ! loop over neigvchi0
  !
  DO i = 1, neigvchi0
     ! pass depotchi0 to auxilary variable
     aux(:) = ZERO; aux(:) = depotchi0(:,i)
     !
     CALL vc_gorling(ngm, aux)  
     !
     potvc(:,i) = aux(:)                ! |vcfi>(G) 
     !
     aux1(:) = ZERO; aux1(nl(:)) = aux(:)
     !
     ! go back to R-space
     CALL invfft ('Dense', aux1, dfftp)
     ! 
     dvgenc(:,:,:)  = ZERO; dvgenc(:,1,1) = CMPLX(DBLE(aux1(:)),0.D0,kind=DP)
     CALL solve_linter_iu ( 0.0_DP )
     IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
     aux1(:) = ZERO; aux1(:) = dvgenc(:,1,2)
     !
     ! go to G-space
     CALL fwfft ('Dense', aux1, dfftp)
     ! 
     denvc(:,i) = aux1(nl(:))           ! Xo|vcfi>(G) 
     ! 
  ENDDO    
  !
max_m = neigvchi0 
  !
ALLOCATE(numer(max_m)) 
  DO i = 1, max_m
    sum_tmp = ZERO
    DO k = 1, ngm
      sum_tmp = sum_tmp + CONJG(potvc(k,i)) * den_gnl(k)
    ENDDO
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif     
    numer(i) = DBLE(sum_tmp)
  ENDDO
  !
ALLOCATE(alphamix(max_m, max_m)) !iter_used))
    alphamix(:,:) = 0.0_DP
    DO i = 1, max_m
      DO j = i, max_m
        sum_tmp = ZERO
        DO k = 1, ngm
          sum_tmp = sum_tmp + CONJG(potvc(k,i))* denvc(k,j)
        ENDDO
#if defined __MPI
        CALL mp_sum( sum_tmp, intra_pool_comm )
#endif     
        alphamix(i,j) = DBLE(sum_tmp)
        alphamix(j,i) = alphamix(i,j)
      ENDDO
      !
    ENDDO
    !
!!!!! Aij^-1 
    ! 
    ALLOCATE(work(max_m), iwork(max_m))
    CALL DSYTRF( 'U', max_m, alphamix , max_m, iwork, work, max_m, info )
    CALL errore( 'broyden', 'factorization', abs(info) )
    ! 
    CALL DSYTRI( 'U', max_m, alphamix , max_m, iwork, work, info )
    CALL errore( 'broyden', 'DSYTRI', abs(info) )    !
    DEALLOCATE(iwork)
    !
!!!!!
    !
    FORALL( i = 1:max_m, &
            j = 1:max_m, j > i ) alphamix(j,i) = alphamix(i,j)
    !
    DO i = 1, max_m
     !
     work(i) = numer(i)
     !
    ENDDO
    !
    ALLOCATE(sum_pot_g(ngm))
      sum_pot_g(:) = ZERO
      DO i = 1, max_m
        alpha = 0.0_DP
        DO j = 1, max_m
          alpha = alpha + (alphamix(i,j) * work(j) )
        ENDDO
        sum_pot_g(:) = sum_pot_g(:) + alpha * potvc(:,i) 
      ENDDO
    DEALLOCATE(alphamix, work, numer)
    !
    !put on the FFT grid
    aux1(:) = ZERO; aux1(nl(:)) = sum_pot_g(:)
    !go back to R-space
    CALL invfft ('Dense', aux1, dfftp)
    !
    DEALLOCATE(sum_pot_g)
    !----------------------------------------------------------------------------!
    !                              FINISHED
    !----------------------------------------------------------------------------!
    !
    ! return the vc_rpa and vx_exx potentials
    !
    IF(iflag==1) THEN
    !
    exxvx(:)    = 0.0_DP; exxvx(:)    = DBLE(aux1(:))
    !
!!!!
    DO i =1, 50
     write(stdout,*) i, DBLE(aux1(i))
    ENDDO
!!!!
    ! here, the potential is saved again to used in next scf step
    save_exx(:) = 0.0_DP; save_exx(:) = exxvx(:)
    !
  ENDIF
  !
  IF(iflag==2) THEN
    ! 
    rpavc(:)  = 0.0_DP; rpavc (:)   = DBLE(aux1(:)) 
!!!!
    DO i =1, 50
     write(stdout,*) i, DBLE(aux1(i))
    ENDDO
!!!!
    ! here, the potential is saved again to used in next scf step
    save_rpa(:) = 0.0_DP; save_rpa(:) = rpavc(:)
    !
  ENDIF
!!!!
  !
  DEALLOCATE ( aux , aux1 , aux2)
  DEALLOCATE ( potvc, denvc, den_nl, den_gnl)
  !
 RETURN
 !
END SUBROUTINE solve_oep_gorling_splitted
!===============================
!
!------------------------------------------------------------------
SUBROUTINE vc_gorling(ndim, dvg)
 !
 ! This routine computes results of applying Vc to a potential
 ! associated with vector q. Coulomb cutoff technique can be used
 !
 USE kinds,         ONLY : dp
 USE constants,     ONLY : fpi, e2
 USE cell_base,     ONLY : tpiba2, omega
 USE qpoint,        ONLY : xq
 USE gvect,         ONLY : ngm, g
 USE control_acfdt, ONLY : ecut_aux  
 !
 IMPLICIT NONE
 !  
 ! I/O variables
 !  
 INTEGER, INTENT(IN) :: ndim
 ! input: dimension of the arrays for potential and charge density
 COMPLEX(DP), INTENT(INOUT) :: dvg(ndim)
 ! inout: potentials in G-space (will be changed on exit)
 !
 ! local variables
 !
 REAL(DP) :: qg2, fac, fac_ch 
 INTEGER  :: ig
 !
 fac_ch   = ecut_aux/tpiba2    ! Ecut/(2pi/a)^2 
 !
 ! various checks
 !
 IF ( ndim .GT. ngm ) CALL errore('vc_dvg', 'ndim too large', 1)
 IF ( ndim .LE. 0   ) CALL errore('vc_dvg', 'ndim negative ', 1)
 !
 DO ig = 1, ngm
   !
   qg2 = (g(1,ig)+xq(1))**2 + (g(2,ig)+xq(2))**2 + (g(3,ig)+xq(3))**2
   !
   !IF( 1.D-10 < qg2 .and. qg2 < fac_ch ) THEN       ! the cutoff here can be handled so that the potential less oscilation
   IF( 1.D-10 < qg2 ) THEN       ! the cutoff here can be handled so that the potential less oscilation
     fac = (omega*e2*fpi)/(qg2*tpiba2)
   ELSE 
     fac = 0.0_DP  
   ENDIF
   dvg(ig) = dvg(ig) * cmplx(fac, 0.0_DP)
   !
 END DO
 !
 RETURN
 !
ENDSUBROUTINE vc_gorling

!------------------------------------------------------------------
SUBROUTINE apply_inverted_chi0 ( ndim, dn, dv)
 !
 ! This routine solves OEP equation by using diagonal decomposition
 ! of Xo in term of its eigenpotentials 
 !
 USE kinds,            ONLY : DP
 USE mp_global,        ONLY : intra_pool_comm
 USE mp,               ONLY : mp_sum        
 USE gvect,            ONLY : ngm, nl
 USE control_acfdt,    ONLY : iuchi0, depotchi0, neigvchi0, eigchi0, chi0_is_solved
 USE control_lr,       ONLY : lgamma 
 USE fft_base,         ONLY : dfftp
 USE fft_interfaces,   ONLY : fwfft, invfft
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER,  INTENT(IN) :: ndim
 REAL(DP), INTENT(IN) :: dn(ndim)
 ! input : dimension of the arrays for potential in R-space
 ! input : functional derivative of exchange or correlation energy 
 !        w.r.t. KS potential in R-space
 !
 REAL(DP), INTENT(INOUT) :: dv(ndim)
 ! output: the corresponding potential in R-space
 !
 ! Local variables
 !
 COMPLEX(DP), ALLOCATABLE :: aux(:),aux1(:),aux2(:),aux3(:)
 ! auxiliary arrays 
 COMPLEX(DP) :: zfac, sum_tmp
 INTEGER     :: i, j
 !
 ALLOCATE ( aux(ngm),aux1(dfftp%nnr),aux2(ngm),aux3(ngm) )
 !
 ! various checks
 !
 IF ( ndim > dfftp%nnr ) CALL errore('apply_inverted_chi0', 'ndim too large', abs(ndim)  )
 IF ( ndim <= 0   ) CALL errore('apply_inverted_chi0', 'ndim negative!', abs(ndim)+1)
 IF ( .not.chi0_is_solved ) CALL errore('apply_inverted_chi0', 'Xo is not solved yet!', 1)
 DO i = 1, neigvchi0
    IF ( abs(eigchi0(i)) .lt. 1.d-6 ) &
       CALL errore('apply_inverted_chi0', 'eigchi0 too small', i)
 ENDDO
 !
 aux1(:) = CMPLX(dn(:),0.d0,kind=DP)
 ! go to G-space
 CALL fwfft ('Dense', aux1, dfftp)
 aux2(:) = aux1(nl(:)) 
 !  
 ! read eigenfunctions of Xo
 depotchi0 (:,:) = ZERO 
 CALL init_depot( neigvchi0, depotchi0, -1, iuchi0 )
 !
 ! loop over neigvchi0
 !
 aux(:) = ZERO
 DO i = 1, neigvchi0
    ! pass depotchi0 to auxilary variable
    aux1(:) = ZERO; aux1(nl(:)) = depotchi0(:,i)
    !if q=(0,0,0), make sure that eigenpotentials of chi0 are real in R-space
    if (lgamma) then
       ! go back to R-space
       CALL invfft ('Dense', aux1, dfftp)
       ! keep only the real part
       aux1(:) = CMPLX(DBLE(aux1(:)), 0.d0, kind=DP)
       ! go to G-space
       CALL fwfft ('Dense', aux1, dfftp)
    end if
    aux3(:) = aux1(nl(:)) 
    !
    sum_tmp = ZERO
    DO j = 1, ngm
       sum_tmp = sum_tmp + CONJG(aux3(j))* aux2(j)
    ENDDO 
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif 
    zfac = sum_tmp 
    !
    ! accumulate over neigvchi0
    zfac = zfac*CMPLX(1.d0/eigchi0(i), 0.d0, kind=DP)
    !
    aux(:) = aux(:) + aux3(:)*zfac
    !
 ENDDO
 !
 ! go back to R-space 
 aux1(:)= ZERO; aux1(nl(:)) = aux(:)
 CALL invfft ('Dense', aux1, dfftp)
 dv  (:)= ZERO; dv(:) = DBLE(aux1(:))    
 !
 DEALLOCATE ( aux, aux1, aux2, aux3)
 !
 RETURN
 !
END SUBROUTINE apply_inverted_chi0

