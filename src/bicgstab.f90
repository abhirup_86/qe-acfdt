!
! Copyright (C) 2001-2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!----------------------------------------------------------------------
subroutine bicgstabsolve_all (h_psi, cg_psi, e, d0psi, dpsi, h_diag, &
     ndmx, ndim, ethr, ik, kter, conv_root, anorm, nbnd)
  !----------------------------------------------------------------------
  !
  !     iterative solution of the linear system by Bi-CGSTAB method:
  !     ( H. A. van der Vorst, "Bi-CGSTAB: A Fast and Smoothly Converging
  !       Variant of Bi-CG for the Solution of Nonsymmetric Linear Systems",
  !        SIAM J. Sci. Stat. Comput., 13, 631-664, (1992) )
  !                      
  !                 ( h - e + Q ) * dpsi = d0psi                      (1)
  !
  !                 where h is a complex non-hermitean matrix, e is a complex sca
  !                 dpsi and d0psi are complex vectors
  !
  !     on input:
  !                 h_psi    EXTERNAL  name of a subroutine:
  !                          h_psi(ndim,psi,psip)
  !                          Calculates  H*psi products.
  !                          Vectors psi and psip should be dimensined
  !                          (ndmx,nvec). nvec=1 is used!
  !
  !                 cg_psi   EXTERNAL  name of a subroutine:
  !                          g_psi(ndmx,ndim,notcnv,psi,e)
  !                          which calculates (h-e)^-1 * psi, with
  !                          some approximation, e.g. (diag(h)-e)
  !
  !                 e        complex     unperturbed eigenvalue plus
  !                          imaginary frequency.
  !
  !                 dpsi     contains an estimate of the solution
  !                          vector.
  !
  !                 d0psi    contains the right hand side vector
  !                          of the system.
  !
  !                 ndmx     integer row dimension of dpsi, ecc.
  !
  !                 ndim     integer actual row dimension of dpsi
  !
  !                 ethr     real     convergence threshold. solution
  !                          improvement is stopped when the error in
  !                          eq (1), defined as l.h.s. - r.h.s., becomes
  !                          less than ethr in norm.
  !
  !     on output:  dpsi     contains the refined estimate of the
  !                          solution vector.
  !
  !                 d0psi    is corrupted on exit
  !
  !
  USE kinds, only : DP
  USE mp,    only : mp_sum
  USE mp_global, only : intra_pool_comm
  implicit none
  !
  !   first the I/O variables
  !
  integer :: ndmx, & ! input: the maximum dimension of the vectors
             ndim, & ! input: the actual dimension of the vectors
             kter, & ! output: counter on iterations
             nbnd, & ! input: the number of bands
             ik      ! input: the k point

  real(kind=DP) :: &
             anorm,   & ! output: the norm of the error in the solution
             ethr       ! input: the required precision
  complex(kind=DP) ::  h_diag(ndmx,nbnd)  ! input: an estimate of ( H - \epsilon )

  complex(kind=DP) :: &
             e(nbnd), & ! input: the actual eigenvalue plus imaginary freq.
             dpsi (ndmx, nbnd), & ! output: the solution of the linear syst
             d0psi (ndmx, nbnd)   ! input: the known term

  logical :: conv_root ! output: if true the root is converged
  external h_psi, &    ! input: the routine computing h_psi
           cg_psi      ! input: the routine computing cg_psi
  !
  !  here the local variables
  !
  integer, parameter :: maxter = 100
  ! the maximum number of iterations
  integer :: iter, ibnd, lbnd
  ! counters on iteration, bands
  integer , allocatable :: conv (:)
  ! if 1 the root is converged

  complex(kind=DP), allocatable :: p(:,:), r (:,:), rr0(:,:), s(:,:), &
                                   t(:,:), v(:,:), y(:,:), z(:,:), aux(:,:)
  !  the gradient of psi
  !  the preconditioned gradient
  !  the delta gradient
  !  the conjugate gradient
  !  work space
  complex(kind=DP) ::  bk, ak, ZDOTC
  !  the ratio between rho
  !  step length
  !  the scalar product
  complex(kind=DP), allocatable :: c(:) 
  complex(kind=DP), allocatable :: rho (:), bet (:), alp(:), omg(:), &
                                   rhoold(:)
  real(kind=DP), allocatable :: res (:)
  complex(kind=DP), allocatable :: eu (:)
  ! the residue
  ! auxiliary for h_diag
  real(kind=DP) :: kter_eff
  ! account the number of iterations with b
  ! coefficient of quadratic form
  !
  call start_clock ('bicgstab')
  allocate ( p(ndmx,nbnd), r(ndmx,nbnd), rr0(ndmx,nbnd), aux(ndmx,nbnd) )
  allocate ( s(ndmx,nbnd), t(ndmx,nbnd) )
  allocate ( v(ndmx,nbnd), y(ndmx,nbnd), z(ndmx,nbnd))
  allocate ( eu(nbnd))    
  allocate ( conv ( nbnd))    
  allocate ( rho(nbnd), bet(nbnd), alp(nbnd), omg(nbnd), res(nbnd), &
            rhoold(nbnd) )    
  !      WRITE( stdout,*) g,t,h,hold

  kter_eff = 0.d0
  do ibnd = 1, nbnd
     conv (ibnd) = 0
  enddo
  !
  do iter = 1, maxter
     !
     if ( iter .eq. 1 ) then
        !
        ! initialize variables
        !  r = H*dpsi
        call h_psi (ndim, dpsi, r, e, ik, nbnd)
        !
        do ibnd = 1, nbnd
           ! change the side of r
           call ZSCAL (ndim, -ONE, r (1, ibnd), 1)
           ! r = d0psi - H*dpsi 
           call ZAXPY (ndim, ONE, d0psi(1,ibnd), 1, r(1,ibnd), 1)
           ! r at the first step is kept in rr0
           call ZCOPY (ndim, r (1, ibnd), 1, rr0 (1, ibnd), 1)     
           ! set initial values for v and p, rho and alpha and omega
           v(:, ibnd) = ZERO; p(:,ibnd) = ZERO
           rho(ibnd)  = ONE ; alp(ibnd) = ONE ;  omg(ibnd) = ONE ;  
        enddo
        !
     endif
     !
     !   compute the norm of residual  
     !
     lbnd = 0
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           lbnd = lbnd +1 
           res(lbnd) = ZDOTC (ndim, r(1,ibnd), 1, r(1,ibnd), 1)
        endif
     enddo
     !
     kter_eff = kter_eff + float (lbnd) / float (nbnd)
#if defined __MPI
     call mp_sum(res, intra_pool_comm)
#endif
     !
     !    convergence check
     !
     do ibnd = nbnd, 1, -1
        if (conv(ibnd) .eq. 0) then
           res(ibnd) = res(lbnd)
           lbnd = lbnd - 1
           anorm = sqrt (res (ibnd) )
!print*,anorm, ethr
           if (anorm.lt.ethr) conv (ibnd) = 1
!           if (anorm.lt.ethr) print*,ibnd, anorm, ethr
        endif
     enddo
!
     conv_root = .true.
     do ibnd = 1, nbnd
        conv_root = conv_root.and. (conv (ibnd) .eq.1)
     enddo
     if (conv_root) goto 100
     !
     !  compute the vectors in the next step.
     !
     !  First keep the values of rho in rhoold
     call ZCOPY (nbnd, rho (1), 1, rhoold (1), 1)     
     !
     !  compute the new values of rho and beta
     lbnd = 0
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           lbnd = lbnd +1 
           rho(lbnd) = ZDOTC (ndim, rr0(1,ibnd), 1, r(1,ibnd), 1)
        endif
     enddo
#if defined __MPI
     call mp_sum (rho, intra_pool_comm)
#endif
     do ibnd = nbnd, 1, -1
        if (conv (ibnd) .eq. 0) then
           rho(ibnd) = rho(lbnd)
           lbnd = lbnd - 1
        endif
     enddo
     do ibnd = 1, nbnd
        if ( abs(rho(ibnd)) .lt. 1.d-30 ) & 
              call errore( "bicgstab","rho too small-->bet too large",1)
        bet(ibnd) = (rho(ibnd)/rhoold(ibnd)) * (alp(ibnd)/omg(ibnd)) 
     enddo
     !
     ! compute the vector p at step i 
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           call ZAXPY (ndim, -omg(ibnd), v(1,ibnd), 1, p(1,ibnd), 1)
           call ZSCAL (ndim, bet(ibnd), p (1, ibnd), 1)
           call ZAXPY (ndim, ONE, r(1,ibnd), 1, p(1,ibnd), 1)
        endif
     enddo
     !
     ! solve y from Ky = p_i and compute v=h*y. 
     ! Here aux is used as auxiliary vector in order to compute preconditioned 
     ! vector as well as matvec multiplication efficiently 
     lbnd = 0
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           lbnd = lbnd + 1
           call ZCOPY (ndim, p (1, ibnd), 1, aux (1, lbnd), 1)
           eu(lbnd) = e(ibnd)
        endif
     enddo
     !
     ! precondition aux
     call cg_psi(ndmx, ndim, lbnd, aux(1,1), h_diag(1,1), 1 )
     !
     ! compute v = A*y ( exactly v' = A*aux)
     call h_psi (ndim, aux(1,1), v(1,1), eu(1), ik, lbnd)
     !
     ! rearrange aux and v' to form the "real" y and v 
     do ibnd = nbnd, 1, -1
        if (conv (ibnd) .eq. 0) then
           call ZCOPY (ndim, aux (1, lbnd), 1, y (1, ibnd), 1)
           call ZCOPY (ndim,   v (1, lbnd), 1, v (1, ibnd), 1)
           lbnd = lbnd - 1
        endif
     enddo
     !   
     ! compute alpha
     lbnd = 0
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           lbnd = lbnd +1 
           alp(lbnd) = ZDOTC (ndim, rr0(1,ibnd), 1, v(1,ibnd), 1)
        endif
     enddo
#if defined __MPI
     call mp_sum (alp, intra_pool_comm)
     !     
#endif
     do ibnd = nbnd, 1, -1
        if (conv (ibnd) .eq. 0) then
           alp(ibnd) = alp(lbnd)
           lbnd = lbnd - 1
        endif
     enddo
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           alp(ibnd) = rho(ibnd) / alp(ibnd)
        endif
     enddo
     !
     ! compute s = r_i-1 -alp*v_i
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           call ZCOPY (ndim, r (1, ibnd), 1, s (1, ibnd), 1)
           call ZAXPY (ndim, -alp(ibnd), v(1,ibnd), 1, s(1,ibnd), 1)
        endif
     enddo
     !
     ! solve z from Kz = s and then compute t = A*z
     ! aux is used here as for the purpose of above task
     lbnd = 0
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           lbnd = lbnd + 1
           call ZCOPY (ndim, s (1, ibnd), 1, aux (1, lbnd), 1)
           eu(lbnd) = e(ibnd)
        endif
     enddo
     !
     ! precondition aux
     call cg_psi(ndmx, ndim, lbnd, aux(1,1), h_diag(1,1), 1 )
     !
     ! compute t = A*z ( exactly t' = A*aux)
     call h_psi (ndim, aux(1,1), t(1,1), eu(1), ik, lbnd)
     !
     ! rearrange aux and t' to form the "real" z and t 
     do ibnd = nbnd, 1, -1
        if (conv (ibnd) .eq. 0) then
           call ZCOPY (ndim, aux (1, lbnd), 1, z (1, ibnd), 1)
           call ZCOPY (ndim,   t (1, lbnd), 1, t (1, ibnd), 1)
           lbnd = lbnd - 1
        endif
     enddo
     !
     ! compute omega = (h^-1*t,h^-1*s) / (h^-1*t, h^-1*t)
     ! ( here aux and r vectors are used as work space )
     lbnd = 0
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           lbnd = lbnd + 1
           call ZCOPY (ndim, t (1, ibnd), 1, aux (1, lbnd), 1)
           call ZCOPY (ndim, s (1, ibnd), 1, r (1, lbnd), 1)
        endif
     enddo
     call cg_psi(ndmx, ndim, lbnd, aux(1,1), h_diag(1,1), 1 )
     call cg_psi(ndmx, ndim, lbnd, r(1,1), h_diag(1,1), 1 )
     ! here eu is used as auxilliary variable to store the denominator
     ! in the expression of omega
     do ibnd = 1, lbnd
        eu (ibnd) = ZDOTC (ndim, aux(1,ibnd), 1, aux(1,ibnd), 1)
        omg(ibnd) = ZDOTC (ndim, aux(1,ibnd), 1, r(1,ibnd), 1) 
     enddo
#if defined __MPI
     call mp_sum (eu, intra_pool_comm)
     call mp_sum (omg, intra_pool_comm)
#endif
     omg(1:lbnd) = omg(1:lbnd) / eu(1:lbnd)
     do ibnd = nbnd, 1, -1
        if (conv (ibnd) .eq. 0) then
           omg(ibnd) = omg(lbnd)
           if ( abs(omg(ibnd)) .lt. 1.d-30) &
           call errore( "bicgstab","omega is too small",1)
           lbnd = lbnd - 1
        endif
     enddo
     !
     !  update dpsi
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           call ZAXPY (ndim, alp(ibnd), y(1,ibnd), 1, dpsi(1,ibnd), 1)
           call ZAXPY (ndim, omg(ibnd), z(1,ibnd), 1, dpsi(1,ibnd), 1)
        endif
     enddo
     !
     !  update residual vector
     do ibnd = 1, nbnd
        if (conv (ibnd) .eq. 0) then
           call ZCOPY (ndim, s (1, ibnd), 1, r (1, ibnd), 1)
           call ZAXPY (ndim, -omg(ibnd), t(1,ibnd), 1, r(1,ibnd), 1)
        endif
     enddo
     !
  enddo
  !
100 continue
  kter = kter_eff
  !
  ! check residual
!  call h_psi (ndim, dpsi(1,1), aux(1,1), e(1), ik, nbnd)
!  call ZAXPY (nbnd*ndim, -ONE, d0psi(1,1), 1, aux(1,1), 1)
!  write(*,*) ( REAL(ZDOTC (ndim, aux(1,ibnd), 1, aux(1,ibnd), 1)), ibnd=1,nbnd)
  !
  deallocate (rho, rhoold, bet, alp, omg, res)
  deallocate (conv)
  deallocate (eu)
  deallocate (p, r, rr0, aux, s, t, v, y, z)
  !
  call stop_clock ('bicgstab')
  !
  return
  !
end subroutine bicgstabsolve_all
