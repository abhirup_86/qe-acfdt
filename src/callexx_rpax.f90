!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!----------------------------------------------------------------------
subroutine callexx_rpax ( num_iter, exx_pot )
  !--------------------------------------------------------------------
  !
  !	
  !     This routine initialize the exx potential calculation 
  !     for RPAx stuff. The coulomb kernel entering in the 
  !     adiabatic connection formula has to be the same both in the RPA term 
  !     and exchange term. Here the G0 component is setted to zero as 
  !     it is done in vc12_dvg.f90. One can also use a modified Coulomb,
  !     e.g. spherical cut or Wigner-Seitz, for isolated system. 
  !
  !
  USE kinds,          ONLY : DP
  USE io_files,       ONLY : iunwfc
  USE units_ph,       ONLY : iuwfc
  USE io_global,      ONLY : stdout
  USE funct,          ONLY : init_dft_exxrpa, dft_is_hybrid, &
                             exx_is_active, stop_exx
  USE exx,            ONLY : exx_grid_init, exx_div_check, exxinit, &
                             exxdiv_treatment, x_gamma_extrapolation, &
                             exxenergy, exxenergy2, deallocate_exx, &
                             exx_grid_initialized, ecutvcut, exx_mp_init
  USE control_acfdt,  ONLY : ex_exx 
  USE acfdt_scf,      ONLY : iter_save0
  !
  implicit none
  !
  !
  logical, intent(in) :: exx_pot
  integer, intent(in) :: num_iter
  !logical, intent(in) :: first
  ! if .true., the routine is call for the first time.
  ! exx_grid_init must be called
  !
  IF(.NOT.exx_pot ) THEN
    RETURN 
  ENDIF
  !
  CALL start_clock( 'callexx' )
  !
  ! For solid case, where nks > 1, in PH/openqfile(), KS_wfc are open with a unit different 
  ! the unit used in PW/src/. Here this calculation is done with PH routines, so
  ! 
  iunwfc = iuwfc   
  !
  !
  WRITE (stdout,*) "callexx : dft_is_hybrid = ",dft_is_hybrid(),exx_is_active()
  !
  call deallocate_exx()
  ! 
  x_gamma_extrapolation = .false.
  exxdiv_treatment = 'none'
  !x_gamma_extrapolation = .false.
  !exxdiv_treatment = 'vcut_ws'
  ! ecutvcut=3
  call init_dft_exxrpa ()
  if ( dft_is_hybrid() ) then
    exx_grid_initialized = .FALSE.  
    call exx_grid_init()
    IF ( num_iter == iter_save0+1 ) CALL exx_mp_init()
    call exx_div_check()
  endif
  !
  WRITE (stdout,*) "callexx 2: dft_is_hybrid = ",dft_is_hybrid(),exx_is_active()
  WRITE (stdout,*) "callexx 2: Div treatment= ",exxdiv_treatment, x_gamma_extrapolation
  !
  ! initialization for exact exchange calculation
  !
  call exxinit ()
  !
  ex_exx = 0.5d0*exxenergy()
  write(stdout,9064) ex_exx, 0.5d0*exxenergy2()                   
  !
9064 format( '     + Fock energy             =',F17.8,' Ry = ', F17.8,' Ry ',/)   
  !
!  if( exx_pot ) then
!    call deallocate_exx()
!    !
!    x_gamma_extrapolation = .false.
!    exxdiv_treatment = 'none'
!    if ( dft_is_hybrid() ) then
!      call exx_grid_init()
!      call exx_div_check()
!    endif
!    !
!    call exxinit ()
!  endif
  !
  WRITE (stdout,*) "callexx 3: dft_is_hybrid = ",dft_is_hybrid(),exx_is_active()
  !
  call stop_exx()  !!! we do not really want h_psi with Fock excghange
  !
  WRITE (stdout,*) "callexx 4: dft_is_hybrid = ",dft_is_hybrid(),exx_is_active()
  !
  CALL stop_clock( 'callexx' )
  !
  return
  !
end subroutine callexx_rpax
