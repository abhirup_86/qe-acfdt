!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!----------------------------------------------------------------------
subroutine chi0_eigfct ( )
  !--------------------------------------------------------------------
  !
  !	This subroutine is a driver for calculation of RPA Ec in the 
  !     Adiabatic Connection Fluctuation-Dissipation Theory (ACFDT). 
  !     The following steps are performed:
  !
  !	 1) Low-lying eigenvalues of Vc*Xo are computed by Davidson 
  !         iterative diagonalization ( calling diagnlres_david.f90 )
  !
  !      2) In RPA Ec is computed from eigenvalues obtained in step 1
  !
  !
  USE kinds,          ONLY : DP
  USE io_global,      ONLY : stdout, ionode, ionode_id
  USE mp_global,      ONLY : intra_pool_comm, intra_image_comm
  USE mp,             ONLY : mp_sum, mp_bcast
  USE io_files,       ONLY : diropn, seqopn 
  USE gvect,          ONLY : ngm
  ! 
  USE acfdt_scf,      ONLY : oep_restart_flag
  USE control_acfdt,  ONLY : chi0_is_solved, chi0rec, control_vc_rpa, iuchi0, lepsi, &
                             lreig, neigvchi0, unitchi0, eigchi0, eigchi0, depotchi0, &
                             depotchi0, eigchi0, lowest_eigchi0


  !
  implicit none
  !
  ! local variables
  !
  real(dp) :: avgiter
  !
  real(dp) :: norm
  integer  :: nvecx, i 
  logical  :: ortho_base = .true.
  !
  logical :: exst_rec, exst_ei
  !
  real(DP), external :: DDOT
  complex(DP), allocatable :: gs(:)
  !
  !
  nvecx = 4*neigvchi0
  ALLOCATE ( gs(nvecx) )
  !
  CALL start_clock( 'chi0_eigfct' )
  !
  ! when OEP calculation is restarted, check ...
  !
  unitchi0 = 66
  chi0rec  = 77; lreig = neigvchi0
  !
  if ( ionode ) then
    !
    call seqopn (unitchi0, 'recchi0', 'unformatted', exst_rec)
    WRITE(stdout,*) exst_rec
    if ( exst_rec ) then
       rewind(unitchi0); read(unitchi0) chi0_is_solved
    else
       chi0_is_solved = .false.       
    endif
    !
    WRITE(stdout,*) chi0_is_solved
    call diropn( chi0rec,'eigchi0', lreig, exst_ei )
    ! 
  endif
  !
#if defined __MPI
  call mp_bcast( chi0_is_solved, ionode_id, intra_image_comm ) ! NsC added the commumicator CHECK
#endif 
  !
  if (oep_restart_flag) then
    !
    ! check if Xo has been computed yet or not
    !  
    if ( ionode ) then
       !
       if ( chi0_is_solved ) then
          !
          ! 
          write(stdout,'(/,7x,''.... X0 HAS BEEN COMPUTED COMPLETELY ....'')')
          ! 
          call davcio( eigchi0(1), lreig, chi0rec, 1, -1 )  
          !
          call print_eig( neigvchi0, eigchi0 )
          !
       endif
       !
    endif
    !
#if defined __MPI
  call mp_bcast( eigchi0, ionode_id, intra_image_comm ) ! NsC added the communicator.. CHECK!!!!!!!
#endif
    !
    if (chi0_is_solved) return
    !
  endif
  !
  ! write down the status of chi0 calculations here 
  !
  if ( ionode ) then
        rewind(unitchi0); write(unitchi0) chi0_is_solved
  endif
  !
  write(stdout, '(/,/,5x,''Compute eigenfunctions of Xo for solving OEP problem'')' )
  ! 
  ! make sure that the vc_scf's variables have not considered yet in this routine.  
  control_vc_rpa = .false.
  ! make sure that Xo is gonna be diagonalized
  lepsi = .false.
  !
  ! initial guess for eigenpotential 
  ! if ( chi0_is_solved ) then
  !    call init_depot( neigv, depot, -1, iuchi0 )
  ! else
       call init_depot( neigvchi0, depotchi0, 0 ) 
  ! endif
  !
  ! diagonalize Xo here
  call diagnlres_david( 0.d0, neigvchi0, depotchi0, eigchi0, avgiter )
  write(stdout, '(/,7x,''Converged eigenvalues of Xo in ascending order'',/)' )
  lowest_eigchi0 =  maxval(eigchi0(:)) 
  call print_eig( neigvchi0, eigchi0 )
  !
  !
  ! orthogonalize if required
  !
  IF (ortho_base) THEN 
     CALL start_clock ('ortho')
     DO i = 1, neigvchi0
        CALL ZGEMM( 'C', 'N', i-1, 1, ngm, ONE, depotchi0, ngm, &
                    depotchi0(1,i), ngm, ZERO, gs(1), nvecx )
#if defined __MPI
        CALL mp_sum( gs(1:i-1), intra_pool_comm )
#endif
        CALL ZGEMM( 'N', 'N', ngm, 1, i-1, -ONE, depotchi0(1,1), ngm, &
                    gs(1), nvecx, ONE, depotchi0(1,i), ngm )
        ! and normalize
!         norm = DDOT(2*ngm, depotchi0(1,i), 1, depotchi0(1,i), 1)
!#if defined __MPI
!        CALL mp_sum( norm, intra_pool_comm ); norm = 1.d0/SQRT(norm)
!#endif
!        CALL DSCAL (2*ngm, norm, depotchi0(1,i), 1 )
     ENDDO
     CALL stop_clock ('ortho')
  ENDIF
  ! 
  ! save eigenpotentials on disk
  call init_depot( neigvchi0, depotchi0, +1, iuchi0 )
  !
  ! save eigenvalues of chi0 on disk
  if ( ionode ) call davcio( eigchi0(1), lreig, chi0rec, 1, +1 )
  !
  ! save status of chi0 calculation on disk
  !
  chi0_is_solved = .true.
  !
  if ( ionode ) then
        rewind(unitchi0); write(unitchi0) chi0_is_solved 
  endif
  !
  return
  !
end subroutine chi0_eigfct
