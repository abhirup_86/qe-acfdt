
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE dchi_by_dv_rpax(u, ndim, m, dvg)
 !
 ! This routine computes Xo*fx*Xo at immaginary frequency iu 
 ! applyied to m perturbation potentials.
 ! Input potentials dvg are in G-space  and become induced
 ! densites on exit.
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE lsda_mod,      ONLY : nspin
 USE wvfct,         ONLY : npwx, nbnd
 USE gvect,         ONLY : ngm, g, nl, gstart, nlm
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE control_acfdt, ONLY : dpsi_plus, dpsi_minus, summ_vc, summ_vc3, summ_vc12, &
                           dvgenc, control_vc_rpa 
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
! INTEGER,  INTENT(IN) :: iu_index
 REAL(DP), INTENT(IN) :: u
 ! input: immagine frequency
 INTEGER, INTENT(IN) :: ndim, m
 ! input: dimension of the arrays for potential and charge density
 ! input: number of pot. 
 COMPLEX(DP), INTENT(INOUT) :: dvg(ndim,m)
 ! input: purturbed potentials in G-space
 ! output: induced densities in G-space
 !
 ! local variables
 !
 LOGICAL exx_pot
 !
 COMPLEX(DP), ALLOCATABLE :: aux(:)
 ! auxiliary arrays for FFT
 INTEGER :: i 
 ! counter
 !
 ALLOCATE (dpsi_plus (npwx,nbnd), dpsi_minus(npwx,nbnd))
 ! First-order variation of wfc at imaginary freq +/- iu
 ALLOCATE (summ_vc(dfftp%nnr, nspin))
 ! Result of Xo*fx*Xo|Dv>
 ALLOCATE (summ_vc3(dfftp%nnr, nspin))
 ALLOCATE (summ_vc12(dfftp%nnr, nspin))
 ! Partial Results of Xo*fx*Xo|Dv>
 ALLOCATE (aux(dfftp%nnr))
 !
 ! various checks
 !
 IF ( ndim > ngm ) CALL errore('dchi_by_dv', 'ndim too large', abs(ndim))
 IF ( ndim <= 0  ) CALL errore('dchi_by_dv', 'ndim negative!', abs(ndim))
 !
 !
 ! dXo|dvg>  is computed in R-space
 !
 !
 ! Loop over potentials
 !
 !
 DO i = 1, m
   ! 
   ! first bring dvg(:,i) to R-space
   !
   aux(:) = ZERO; aux(nl(1:ndim)) = dvg(1:ndim,i)
   CALL invfft ('Dense', aux, dfftp)
   !
   ! define the perturbing potential
   !
   dvgenc(:,1,1) = aux(:); dvgenc(:,:,2) = ZERO
   IF (nspin==2) dvgenc(:,nspin,1) = dvgenc(:,1,1)
   !  
   ! solve the linear system and find the variation of wcfs
   !
   dpsi_plus (:,:) = ZERO;  dpsi_minus(:,:) = ZERO
   !
   control_vc_rpa = .TRUE.                                           !!! if TRUE save dpsi_plus dpsi_minus
   CALL solve_linter_iu ( u )
   control_vc_rpa = .FALSE.
   !
   ! here, we call a subroutine to do the detail calculations.  
   !
   summ_vc(:,:) = ZERO
   summ_vc3(:,:) = ZERO
   summ_vc12(:,:) = ZERO
   !
   CALL solve_dec_dv_rpax ( u )
   !
   IF (nspin==2) summ_vc(:,1) = summ_vc(:,1) + summ_vc(:,nspin) 
   IF (nspin==2) summ_vc3(:,1) = summ_vc3(:,1) + summ_vc3(:,nspin) 
   IF (nspin==2) summ_vc12(:,1) = summ_vc12(:,1) + summ_vc12(:,nspin) 
   !
   ! go back to G-space
   aux(:) = ZERO ; aux(:) = summ_vc(:,1)
   CALL fwfft ('Dense', aux, dfftp)
   dvg(1:ndim,i) = aux(nl(1:ndim))
   !
 END DO
 !
 !
 DEALLOCATE (dpsi_plus, dpsi_minus)
 DEALLOCATE ( aux       )
 DEALLOCATE ( summ_vc   )
 DEALLOCATE ( summ_vc3  )
 DEALLOCATE ( summ_vc12 )
 !
 RETURN
 !
END SUBROUTINE dchi_by_dv_rpax
