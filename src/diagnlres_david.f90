
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!#define DEBUG
!
!----------------------------------------------------------------------
SUBROUTINE diagnlres_david ( uu, nevl, dv, e, avg_iter )
!SUBROUTINE diagnlres_david ( uu, nevl, e, avg_iter )
  !--------------------------------------------------------------------
  !
  !	This subroutine calculates low-lying eigenvalues of response functions
  !     (or dielectric matrix if lepsi=.true.) by Davidson iterative diagonalization. 
  !
  USE kinds,                ONLY : DP
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE mp_global,            ONLY : intra_pool_comm, inter_pot_comm => inter_image_comm, npot => nimage
  USE gvect,                ONLY : ngm,  nl, gstart, ngm_g
  USE mp,                   ONLY : mp_sum, mp_bcast
  USE qpoint,               ONLY : xq
  USE control_lr,           ONLY : lgamma
  USE fft_base,             ONLY : dfftp
  USE fft_interfaces,       ONLY : fwfft, invfft
  USE control_acfdt,        ONLY : thr_ec, lepsi
  !
  IMPLICIT NONE
  !
  INTEGER, PARAMETER :: kterx=50
  !
  ! I/O variables
  !
  REAL(DP), INTENT(IN) :: uu
  ! input: imaginary frequency 
  INTEGER, INTENT(IN) :: nevl
  ! input: number of eigenmodes to be computed
  REAL(DP), INTENT(OUT) :: e(nevl) 
  ! output: eigenvalues
  COMPLEX(DP), INTENT(INOUT) :: dv(ngm,nevl)
  ! input: initial guess of eigenpotentials
  ! output: converged eigenpotentials
  REAL(DP), INTENT(OUT) :: avg_iter
  ! output: average number of iteration per mode in Davidson diagonalization
  !
  ! local variables
  !
  COMPLEX(DP), ALLOCATABLE :: dvg(:,:), dng(:,:)
  ! potentials and density response in G-space
  COMPLEX(DP), ALLOCATABLE :: chi0(:,:), vc(:,:), xr(:,:)
  ! Vc^{1/2}*Xo*Vc^{1/2} in reduced basis set
  ! overlap matrix 
  ! estimated eigenpotentials 
  REAL(DP), ALLOCATABLE :: ew(:), ethr(:)
  ! estimated eigenvalues 
  ! threshold for eigenvalues
  COMPLEX(DP), ALLOCATABLE :: gs(:), aux(:)
  ! working space
  INTEGER :: notcnv
  ! integer  number of iterations performed
  LOGICAL, ALLOCATABLE :: conv(:), comp_dv(:)
  ! true if the root is converged
  INTEGER :: nkter, kter, kter0, kter1, nbase, n, m, nb1, np, nbn, ios, info
  ! counter on iterations
  ! dimension of the reduced basis
  ! counter on the reduced basis vectors
  ! do-loop counters
  !LOGICAL :: exst, opnd, ortho_base = .false.
  LOGICAL :: exst, opnd, ortho_base = .true.
  REAL(DP) :: norm, epsi00, epstsr(3,3)
  !
  INTEGER :: ig, ir, i, j, ip, nvecx
  ! counters
  REAL(DP), EXTERNAL :: DDOT
  COMPLEX(DP), EXTERNAL :: ZDOTC
  !
  nvecx = 4*nevl
!  nvecx = 2*nevl
  !WRITE(stdout,'(7x,''Davidson diagonalization'')')
  !
  ! various checkings
  !
  IF (ngm_g .LT. nvecx) &
     CALL errore('diagnlres_david', 'nvecx TOO LARGE', 1)
  !IF ( lgamma ) call errore('diagnlres_david', &
  !              'Implementation at q=0 not completed yet', 1)
  !
  ! local arrays  
  !
  !write(stdout,*)"ngm = ", ngm, "nvecx = ", nvecx
  ALLOCATE ( dvg(ngm,nvecx), dng(ngm,nvecx) ) 
  ALLOCATE ( chi0(nvecx,nvecx),vc(nvecx,nvecx), xr(nvecx,nvecx) )
  ALLOCATE ( ew(nvecx), ethr(nevl), conv(nevl), comp_dv(nevl) )
  ALLOCATE ( gs(nvecx) )
  IF (lgamma)  ALLOCATE ( aux(dfftp%nnr) )
  !
  CALL start_clock ('diagnlres_david')
  !
  ethr(:) = thr_ec; avg_iter = 0.d0
  !
  dvg(:,:) = ZERO; dng(:,:) = ZERO
  chi0(:,:) = ZERO; vc(:,:)  = ZERO; xr(:,:) = ZERO
  ew(:) = 0.d0; e(:) = 0.d0; kter0 = 0; ios = 0; nkter = 0
  comp_dv(:) = .TRUE. ! must be set to .TRUE. for the code work in case NPOT=1
  !
  IF ( ortho_base ) THEN
     DO n = 1, nvecx; vc(n,n) = ONE; ENDDO
  ENDIF
  !
  dvg(:,1:nevl) = dv(:,1:nevl)
  !
  ! if q=(0,0,0) make sure that eigenpotentials are real in R-space
  !
  IF ( lgamma ) THEN
     DO n = 1, nevl
        ! put dvg on the FFT grid
        aux(:) = ZERO; aux(nl(1:ngm)) = dvg(1:ngm,n)
        ! go to R-space
        CALL invfft ('Dense', aux, dfftp)
        ! keep only the real part
        aux(:) = CMPLX( DBLE(aux(:)), 0.d0, kind=DP )
        ! go back to G-space
        CALL fwfft ('Dense', aux, dfftp)
        ! put back to dvg
        dvg(1:ngm,n) = aux(nl(1:ngm))
     ENDDO
     !
     ! for the case of diagonalizing the dielectric matrix, 
     ! the head element must be computed separately first
     !
     IF ( lepsi ) THEN
        !call solve_head( uu, epstsr )
        !epsi00 = epstsr(1,1)
        !WRITE(stdout,'(9x,"Head element of Vc*Xo: ",3F12.6)') (epstsr(n,n), n=1,3)
        !WRITE(stdout,'(/,9x,"epsilon_RPA(0,0)   : ",3F12.6)') (1.d0-epstsr(n,n), n=1,3)
        epsi00 = 0.d0
     ENDIF
     !
  ENDIF
  !
  ! orthogonalize if required
  !
  IF (ortho_base) THEN
     CALL start_clock ('ortho')
     DO i = 1, nevl
        CALL ZGEMM( 'C', 'N', i-1, 1, ngm, ONE, dvg, ngm, &
                    dvg(1,i), ngm, ZERO, gs(1), nvecx )
        CALL mp_sum( gs(1:i-1), intra_pool_comm )
        CALL ZGEMM( 'N', 'N', ngm, 1, i-1, -ONE, dvg(1,1), ngm, &
                    gs(1), nvecx, ONE, dvg(1,i), ngm )
        ! and normalize
        norm = DDOT(2*ngm, dvg(1,i), 1, dvg(1,i), 1)
        CALL mp_sum( norm, intra_pool_comm ); norm = 1.d0/SQRT(norm)
        CALL DSCAL (2*ngm, norm, dvg(1,i), 1 )
     ENDDO
     CALL stop_clock ('ortho')
  ENDIF
  !
  ! compute matrix elements in the trial basis set 
  !
  ! determine image that actually compute dng first
  IF ( npot.GT.1 ) CALL distribute_dv ( nevl, comp_dv )
  !
  DO i = 1, nevl
     !
     IF ( comp_dv(i) ) THEN 
        !
        dng(:,i) = dvg(:,i)
        IF ( lgamma .AND. lepsi .AND. (gstart==2) ) dng(1,i) = (0.d0,0.d0)  
        IF ( lepsi ) CALL vc12_dvg( ngm, 1, xq, dng(1,i) )
        CALL drhoiu_by_dv( uu, ngm, 1, dng(1,i) )
        IF ( lepsi ) CALL vc12_dvg( ngm, 1, xq, dng(1,i) )
        ! special treatment for q+G=0 component
        IF ( lgamma .AND. lepsi .AND. (gstart==2) ) &
           dng(1,i) = CMPLX(epsi00, 0.d0, kind=DP) * dvg(1,i)
        !
        ! if q=(0,0,0) make sure that screened potentials are real in R-space
        ! ( remove numerical noise which makes imaginary part non-zero )
        IF ( lgamma ) THEN
           ! put dvg on the FFT grid
           aux(:) = ZERO; aux(nl(1:ngm)) = dng(1:ngm,i)
           ! go to R-space
           CALL invfft ('Dense', aux, dfftp)
           ! keep only the real part
           aux(:) = CMPLX( DBLE(aux(:)), 0.d0, kind=DP )
           ! go back to G-space
           CALL fwfft ('Dense', aux, dfftp)
           ! put back to dvg
           dng(1:ngm,i) = aux(nl(1:ngm))
        ENDIF
        !
     ELSE
        !
        dng(:,i) = (0.d0, 0.d0)
        !
     ENDIF
     !
  ENDDO
  !
#if defined __MPI
  ! mp_sum is used as a trick to circulate dng
  IF ( npot.GT.1 ) CALL mp_sum ( dng(:,1:nevl), inter_pot_comm )
#endif
  !
  ! chi0 = <dvg|dng>
  CALL ZGEMM( 'C', 'N', nevl, nevl, ngm, ONE, dvg(1,1), ngm,&
               dng(1,1), ngm, ZERO, chi0(1,1), nvecx )
#if defined __MPI
  CALL mp_sum( chi0(:,1:nevl), intra_pool_comm )
#endif
  !
  ! compute overlap matrix vc if non-orthonormal basis set is used
  !
  IF ( .NOT. ortho_base ) THEN
     CALL ZGEMM( 'C', 'N', nevl, nevl, ngm, ONE, dvg(1,1), ngm,&
                 dvg(1,1), ngm, ZERO, vc(1,1), nvecx )
#if defined __MPI
     CALL mp_sum( vc(:,1:nevl), intra_pool_comm )
#endif
  ENDIF
  !
  ! arrange them in the right order
  !
  DO n = 1, nevl
     !
     vc  (n,n) = CMPLX( REAL(vc  (n,n)), 0.D0, kind=DP )
     chi0(n,n) = CMPLX( REAL(chi0(n,n)), 0.D0, kind=DP )
     !
     DO m = n + 1, nevl
        !
        IF ( lgamma ) THEN
           vc  (n,m) = CMPLX( REAL(vc  (n,m)), 0.D0, kind=DP )
           chi0(n,m) = CMPLX( REAL(chi0(n,m)), 0.D0, kind=DP )
           vc  (m,n) = CONJG( vc  (n,m) )
           chi0(m,n) = CONJG( chi0(n,m) )
        ELSE
           vc  (m,n) = CONJG( vc  (n,m) )
           chi0(m,n) = CONJG( chi0(n,m) )
        ENDIF
        !
     END DO
     !
  ENDDO
  !
  ! diagonalize reduced matrix
  !
  IF (npot .GT. 1) THEN
    CALL mp_bcast(chi0, ionode_id, inter_pot_comm)
    CALL mp_bcast(vc, ionode_id, inter_pot_comm)
    CALL cdiaghg( nevl, nevl, chi0, vc, nvecx, ew, xr )
    CALL mp_bcast(ew, ionode_id, inter_pot_comm)
    CALL mp_bcast(xr, ionode_id, inter_pot_comm)
  ELSE
    CALL cdiaghg( nevl, nevl, chi0, vc, nvecx, ew, xr )
  ENDIF
  !
  ! estimated roots are kept in e(1:nevl)
  e(1:nevl) = ew(1:nevl)
  !
#if defined DEBUG
  CALL print_eig( nevl, e )
#endif
  !
  ! set proper value for some variables
  notcnv = nevl; nbase = nevl; conv(:)=.false.
  !
  !====================================================================
  !
  !                     ... BEGIN ITERATE ...
  !
  !====================================================================
  !
  iterate: DO kter = 1, kterx
    !
    nkter = kter0 + kter
    !
#if defined DEBUG
    WRITE(stdout,'(/,7x,"Iteration # ",i3,3x,"notcnv = ",i4,3x, "nbase = ",i4)') &
         nkter, notcnv, nbase+notcnv
#endif
    !
    ! ... reorder eigenvectors so that coefficients for unconverged
    ! ... roots come first. This allows to use quick matrix-matrix
    ! ... multiplications to set a new basis vector (see below)
    np = 0
    DO n = 1, nevl
       IF ( .NOT. conv(n) ) THEN
          ! this root not yet converged
          np = np + 1
          IF ( np /= n ) xr(:,np) = xr(:,n)
          ew(nbase+np) = e(n)
       END IF
    END DO
    !
    nb1 = nbase + 1
    !
    ! build correction vectors for unconverged roots
    !
    CALL start_clock('update')
    CALL ZGEMM( 'N', 'N', ngm, notcnv, nbase, ONE, dvg, &
                 ngm, xr, nvecx, ZERO, dvg(1,nb1), ngm )
    DO np = 1, notcnv
       dvg(:,nbase+np) = - ew(nbase+np)*dvg(:,nbase+np)
    END DO
    CALL ZGEMM( 'N', 'N', ngm, notcnv, nbase, ONE, dng, &
                 ngm, xr, nvecx, ONE, dvg(1,nb1), ngm )
    CALL stop_clock('update')
    !
    IF (ortho_base) THEN
       !
       ! orthonormalize correction vectors if required
       ! 
       CALL start_clock ('ortho')
       DO n = 1, notcnv
          nbn = nbase + n
          CALL ZGEMM( 'C', 'N', nbn-1, 1, ngm, ONE, dvg, ngm, &
                      dvg(1,nbn), ngm, ZERO, gs(1), nvecx )
          CALL mp_sum( gs(1:nbn-1), intra_pool_comm )
          CALL ZGEMM( 'N', 'N', ngm, 1, nbn-1, -ONE, dvg(1,1), ngm,&
                      gs(1), nvecx, ONE, dvg(1,nbn), ngm )
          ! and normalize
          norm = DDOT(2*ngm, dvg(1,nbn), 1, dvg(1,nbn), 1)
          CALL mp_sum( norm, intra_pool_comm ); norm = 1.d0/sqrt(norm)
          CALL DSCAL (2*ngm, norm, dvg(1,nbn), 1 )
       ENDDO
       CALL stop_clock ('ortho')
       !
    ELSE
       !
       ! otherwise normalize correction vectors to improve stability
       !
       DO n = 1, notcnv
          ew(n) = DDOT(2*ngm, dvg(1,nbase+n), 1, dvg(1,nbase+n), 1)
       ENDDO
       !CALL reduce(notcnv, ew)
       CALL mp_sum( ew(1:notcnv), intra_pool_comm )
       DO n = 1, notcnv
          CALL DSCAL ( 2*ngm, 1.d0/sqrt(ew(n)), dvg(1,nbase+n), 1 )
       ENDDO
       !
    ENDIF
    !
    ! apply the response function to new vectors
    !
    ! determine image that actually compute dng first
    IF ( npot.GT.1 ) CALL distribute_dv ( notcnv, comp_dv(1) )
    !
    DO n = 1, notcnv
       !
       nbn = nbase + n
       !
       IF ( comp_dv(n) ) THEN 
          !
          dng(:,nbn) = dvg(:,nbn)
          IF ( lgamma .AND. lepsi .AND. (gstart==2) ) dng(1,nbn) = (0.d0, 0.d0) 
          IF ( lepsi ) CALL vc12_dvg( ngm, 1, xq, dng(1,nbn) )
          CALL drhoiu_by_dv( uu, ngm, 1, dng(1,nbn) )
          IF ( lepsi ) CALL vc12_dvg( ngm, 1, xq, dng(1,nbn) )
          ! special treatment for q+G=0 component
          IF ( lgamma .AND. lepsi .AND. (gstart==2) ) &
             dng(1,nbn) = CMPLX(epsi00, 0.d0, kind=DP) *  dvg(1,nbn)
          !
          ! if q=(0,0,0) make sure that screened potentials are real in R-space
          ! ( remove numerical noise which makes imaginary part non-zero )
          !
          IF ( lgamma ) THEN
             ! put dvg on the FFT grid
             aux(:) = ZERO; aux(nl(1:ngm)) = dng(1:ngm,nbn)
             ! go to R-space
             CALL invfft ('Dense', aux, dfftp)
             ! keep only the real part
             aux(:) = CMPLX( DBLE(aux(:)), 0.d0, kind=DP )
             ! go back to G-space
             CALL fwfft ('Dense', aux, dfftp)
             ! put back to dvg
             dng(1:ngm,nbn) = aux(nl(1:ngm))
          ENDIF
          !
       ELSE
          !
          dng(:,nbn) = (0.d0, 0.d0)
          !
       ENDIF
       !
    ENDDO
    !
#if defined __MPI
    IF ( npot.GT.1 ) CALL mp_sum ( dng(:,nb1:nbase+notcnv), inter_pot_comm )
#endif
    !
    ! update matrix elements of chi0, vc 
    !
    CALL ZGEMM( 'C', 'N', nbase+notcnv, notcnv, ngm, ONE, dvg, &
                 ngm, dng(1,nb1), ngm, ZERO, chi0(1,nb1), nvecx )
#if defined __MPI
    CALL mp_sum( chi0(:, nb1:nbase+notcnv), intra_pool_comm )
#endif
    !
    IF ( .NOT. ortho_base ) THEN
       CALL ZGEMM( 'C', 'N', nbase+notcnv, notcnv, ngm, ONE, dvg, &
                   ngm, dvg(1,nb1), ngm, ZERO, vc(1,nb1), nvecx )
#if defined __MPI
    CALL mp_sum( vc(:, nb1:nbase+notcnv), intra_pool_comm )
#endif
    ENDIF
    !
    nbase = nbase + notcnv
    DO n = 1, nbase
       !
       vc  (n,n) = CMPLX( REAL(vc  (n,n)), 0.D0, kind=DP )
       chi0(n,n) = CMPLX( REAL(chi0(n,n)), 0.D0, kind=DP )
       !
       DO m = n + 1, nbase
          !
          IF ( lgamma ) THEN
             vc  (n,m) = CMPLX( REAL(vc  (n,m)), 0.D0, kind=DP )
             chi0(n,m) = CMPLX( REAL(chi0(n,m)), 0.D0, kind=DP )
             vc  (m,n) = CONJG( vc  (n,m) )
             chi0(m,n) = CONJG( chi0(n,m) )
          ELSE
             vc  (m,n) = CONJG( vc  (n,m) )
             chi0(m,n) = CONJG( chi0(n,m) )
          ENDIF
          !
       END DO
       !
    ENDDO
    !
    ! diagonalize reduced matrix
    !
    IF (npot .GT. 1) THEN
      CALL mp_bcast(chi0, ionode_id, inter_pot_comm)
      CALL mp_bcast(vc, ionode_id, inter_pot_comm)
      CALL cdiaghg( nbase, nevl, chi0, vc, nvecx, ew, xr )
      CALL mp_bcast(ew, ionode_id, inter_pot_comm)
      CALL mp_bcast(xr, ionode_id, inter_pot_comm)
    ELSE
      CALL cdiaghg( nbase, nevl, chi0, vc, nvecx, ew, xr )
    ENDIF
    !
    ! check for convergence and print eigenvalues if required
    !
    conv(1:nevl) = ( ( ABS( ew(1:nevl) - e(1:nevl) ) < ethr(1:nevl) ) )
    notcnv = COUNT( .NOT. conv(:) )
    avg_iter = avg_iter + DBLE(notcnv)
    !
    ! estimated roots are kept in e(1:nevl)
    e(1:nevl) = ew(1:nevl)
#if defined DEBUG
    CALL print_eig( nevl, e )
#endif
    !
    ! exit if convergence has been archieved
    !
    IF ( notcnv .EQ. 0 ) THEN
       !
#if defined DEBUG
       WRITE(stdout,'(/, 5x,"CONVERGENCE ACHIEVED",/)')
#endif
       !
       EXIT iterate
       !
    ENDIF
    !
    ! stop if too many iterations 
    !
    IF (kter .EQ. kterx) &
       call errore( 'diagnlres_david','eigenvalues not converged', notcnv ) 
    !
    ! if the basis set becomes too large, restart with the initial 
    ! subspace formed by current estimated eigenvectors 
    !
    IF ( (nbase+notcnv) .GT. nvecx ) THEN
       !
#if defined DEBUG
       WRITE(stdout,'(/,7x,"Refresh the basis set")')
#endif
       ! refresh dvg
       CALL ZGEMM( 'N', 'N', ngm, nevl, nbase, ONE, dvg, &
                   ngm, xr, nvecx, ZERO, dv, ngm )
       dvg(:,:)=ZERO; dvg(:,1:nevl) = dv(:,1:nevl)
       ! refresh dng
       CALL ZGEMM( 'N', 'N', ngm, nevl, nbase, ONE, dng, &
                    ngm, xr, nvecx, ZERO, dv, ngm )
       dng(:,:) = ZERO; dng(:,1:nevl) = dv(:,1:nevl)
       !
       ! refresh chi0, xr
       !
       nbase = nevl; chi0(:,:)=ZERO; xr(:,:)=ZERO
       IF (.NOT. ortho_base) vc(:,:)=ZERO
       DO n = 1, nbase
          chi0(n,n) = CMPLX( e(n), 0.D0, kind=DP )
          xr  (n,n) = ONE
          vc  (n,n) = ONE
       END DO
       !
    ENDIF
    !
  ENDDO iterate
  !
155 CONTINUE
  !
  ! compute nevl converged eigenvectors (output in dv) 
  !
  dv(:,:) = ZERO
  CALL ZGEMM( 'N', 'N', ngm, nevl, nbase, ONE, dvg, &
               ngm, xr, nvecx, ZERO, dv, ngm )
  !
  avg_iter = avg_iter / dble(nevl)
  !
  CALL stop_clock ('diagnlres_david')
  !
  DEALLOCATE ( dvg, dng )
  DEALLOCATE ( chi0, vc, xr )
  DEALLOCATE ( ew, ethr, conv, comp_dv )
  DEALLOCATE ( gs )
  IF (lgamma)  DEALLOCATE ( aux )
  !
  RETURN
  !
END SUBROUTINE diagnlres_david
!
!
SUBROUTINE distribute_dv ( ndv, comp_dv )
  !
  ! This routine takes the number of perturbations as input
  ! and determines which group of processors will handle which
  ! perturbation ( comp_dv will be set to .true.)
  ! The code make use of a recently introduced level of parallelization
  ! between image and pool called npot 
  !
  USE kinds,        ONLy : dp
  USE io_global,    ONLy : stdout
  USE mp_global,    ONLy : my_pot_id => my_image_id, npot => nimage
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT(IN) :: ndv
  ! ndv    : total number of potentials 
  LOGICAL, INTENT(INOUT) :: comp_dv(ndv)
  ! 
  INTEGER, ALLOCATABLE :: ndvpp(:)
  INTEGER :: i, is, il, nr
  !
  comp_dv(:) = .FALSE.
  ALLOCATE ( ndvpp(0:npot-1) )
  !
  ndvpp(0:npot-1) = ndv/npot
  nr = ndv - npot*ndvpp(0)
  ndvpp(0:nr-1) = ndvpp(0:nr-1) + 1
  ! 
  IF ( SUM(ndvpp(0:npot-1)) .ne. ndv ) &
     CALL errore('distribute_dv', 'some potentials is missing', 1)
  !
  IF (my_pot_id.EQ.0) comp_dv(1:ndvpp(0)) = .TRUE.
  DO i = 1, npot-1
     IF ( my_pot_id == i ) THEN
        is = SUM ( ndvpp(0:my_pot_id-1) ) + 1
        il = is - 1 + ndvpp(my_pot_id)
        comp_dv(is:il) = .TRUE.
     ENDIF
  ENDDO
  !
  DEALLOCATE ( ndvpp )
  !
  RETURN
  !
END SUBROUTINE distribute_dv
