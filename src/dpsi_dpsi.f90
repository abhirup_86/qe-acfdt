!
! Copyright (C) 2001-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
subroutine dpsi_dpsi (summ, weight, ik, dpsi_plus, dpsi_minus, kq_control)
  !-----------------------------------------------------------------------
  !
  !     This routine computes the change of the charge density due to the
  !     perturbation. It is called at the end of the computation of the
  !     change of the wavefunction for a given k point.
  !
  !
  USE kinds,                 ONLY : DP
  USE cell_base,             ONLY : omega
  USE fft_base,              ONLY : dffts
  USE fft_interfaces,        ONLY : invfft
  USE gvecs,                 ONLY : nls
  USE wvfct,                 ONLY : npwx, nbnd
  USE qpoint,                ONLY : ikks, ikqs
  USE control_lr,            ONLY : nbnd_occ
  USE klist,                 ONLY : ngk, igk_k

  implicit none
  ! I/O variables
  COMPLEX(DP), INTENT (INOUT) :: summ (dffts%nnr)
  ! the value of 1rt part 
  real(DP), INTENT (IN) :: weight
  ! input: the weight of the k point
  integer, INTENT (IN) :: ik, kq_control
  ! input: the k point
  ! input: control calc. at the k point (!= 1) or q point (= 1)
  complex(DP), INTENT (IN) :: dpsi_plus (npwx,nbnd)
  complex(DP), INTENT (IN) :: dpsi_minus(npwx,nbnd)
  ! input: the perturbed wfc for the given k point for +/- iu
  !
  ! Here the local variable
  !
  real(DP) :: wgt
  ! the effective weight of the k point
  !
  complex(DP), allocatable  :: dpsic_plus  (:)
  complex(DP), allocatable  :: dpsic_minus (:)
  ! the change of wavefunctions in real space for +/- iu
  !
  integer :: ibnd, ikk, ir, ig
  ! NsC >
  integer :: ikq, npw, npwq
  ! counters
  !
  call start_clock ('dpsi_dpsi')
  !ok
  allocate (dpsic_plus (  dffts%nnr))
  allocate (dpsic_minus(  dffts%nnr))
  !
  wgt = 2.d0 * weight / omega
  ikk = ikks(ik)
  !NsC > 
  ikq = ikqs(ik)
  npw = ngk(ikk)
  npwq= ngk(ikq)
  !    <
  !
  summ (:)        = ( 0.D0, 0.D0 )
  !
  !Insulators: note that nbnd_occ(ikk)=nbnd_occ(ikq) in an insulator  
  !
  do ibnd = 1, nbnd_occ (ikk)
    !
    dpsic_plus (:)  = ( 0.D0, 0.D0 )
    IF (kq_control == 1) THEN              ! NsC: check with the call in solve_dec_dv. If kq_control=1 we are at k+q not k. Tobochecked!!
!       dpsic_plus (nls(igk (1:npw ))) = dpsi_plus (1:npw ,ibnd)
       dpsic_plus (nls ( igk_k(1:npw,ikk) ) ) = dpsi_plus (1:npw ,ibnd)
    ELSE
!       dpsic_plus (nls(igkq (1:npwq ))) = dpsi_plus (1:npwq ,ibnd)
       dpsic_plus (nls(igk_k (1:npwq,ikq ))) = dpsi_plus (1:npwq ,ibnd)
    ENDIF 
    !
    CALL invfft ('Wave', dpsic_plus, dffts)
    ! 
    dpsic_minus (:) = ( 0.D0, 0.D0 )
!    dpsic_minus (nls(igkq (1:npwq ))) = dpsi_minus (1:npwq ,ibnd)
    dpsic_minus (nls(igk_k (1:npwq,ikq ))) = dpsi_minus (1:npwq ,ibnd)
    !
    CALL invfft ('Wave', dpsic_minus, dffts)
    !
    !
    do ir = 1, dffts%nnr
       summ (ir) = summ (ir) + wgt* CONJG( dpsic_plus (ir) ) * dpsic_minus(ir)
    enddo
    !
  enddo 
  !
  deallocate (dpsic_plus)
  deallocate (dpsic_minus)
  !
  call stop_clock ('dpsi_dpsi')
  return
end subroutine dpsi_dpsi
