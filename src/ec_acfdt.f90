!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!----------------------------------------------------------------------
subroutine ec_acfdt ( num_iter, rpa_pot )
  !--------------------------------------------------------------------
  !
  !	This routine is a driver for calculation of correlation energy,
  !     and its functional derivative w.r.t. KS potential if required,
  !     in Random Phase Approximation within the Adiabatic Connection 
  !     Fluctuation-Dissipation Theory (ACFDT). 
  !     The exact-exchange kernel can also be added (RPAx). Different
  !     resummations are implemented.
  ! 
  !     The following steps are performed:
  !
  !	 1) Low-lying eigenvalues of Vc^{1/2}*Xo*Vc^{1/2} are computed 
  !         by Davidson iterative diagonalization
  !
  !      2) In RPA Ec is computed from eigenvalues obtained in step 1 
  !
  !
  USE kinds,                ONLY : DP
  USE io_files,             ONLY : nd_nmbr, &
                                   diropn, seqopn, tmp_dir
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE mp,                   ONLY : mp_bcast
  USE mp_global,            ONLY : intra_image_comm
  USE xml_io_base,          ONLY : create_directory
  USE save_ph,              ONLY : tmp_dir_save
  USE fft_base,             ONLY : dfftp
!  USE io_pot_xml,           ONLY : write_pot, read_pot
  USE acfdt_scf,            ONLY : iter_save, line_search
  USE pwcom,                ONLY : npwx, nbnd, nspin
  USE gvect,                ONLY : ngm, ngm_g
  USE constants,            ONLY : pi
  USE control_lr,           ONLY : lgamma
  USE phcom,                ONLY : lgamma_gamma, tr2_ph, iunrec
  USE qpoint,               ONLY : xq
  USE control_acfdt,        ONLY : dpsi_nl, dpsiq_nl, sum_dchi,  sum_dchi3, sum_dchi12, &
                                   dchi_iu, dchi_iu3, dchi_iu12, wu_ec, &
                                   dvgenc, exxvx, depot, control_vc_rpa, ecrpa, idiag, &
                                   infodia, iudchi, iumax, iundvr, iundvw, iuneig, iunfx, &
                                   lepsi, lrdchi, lreig, lrfx, neigv, nevlx, nf_ec, nlambda, &
                                   pot_collect, rpax, savealldep, thr_ec, u0_ec, wq, u_ec
  !
  implicit none
  !
  ! I/O variables
  !
  logical, intent(in) :: rpa_pot
  integer, intent(in) :: num_iter
  !
  ! local variables
  !
  integer :: iudv, lrdv, iudn, lrdn, ndv, ndn 
  ! unit of direct access file to store dv
  ! length of record to store dv 
  ! unit of direct access file to store dn
  ! length of record to store dn 
  ! counter on record number of dv 
  ! and dn
  logical :: exst, exst2, opnd

  INTEGER :: kter, nkter, nbase, np, n, m, nb1, ib 
    ! counter on iterations
    ! dimension of the reduced basis
    ! counter on the reduced basis vectors
    ! do-loop counters
  integer :: iu, iu0, irr, i, j, k, l     ! counters
  integer :: int_u_ec, dec_u_ec
  !
  real(DP) :: eciu_rpa, ecex_rpa, eciu_rpax, ecex_rpax, avgiter
  real(DP) :: eciu_rpax_ev, eciu_rpax_fu, ecex_rpax_ev, ecex_rpax_fu
  real(DP) :: eciu_ttrpax, ecex_ttrpax
  REAL(DP), ALLOCATABLE :: dnvcdn_aux(:)
  REAL(DP), ALLOCATABLE :: lambda(:), tl_nk(:), wl(:)
  REAL(DP) ll, a, b, ec_lambda, ec_lambda_ev, ec_lambda_fu
!  INTEGER nlambda
  real(DP), allocatable :: e_rpa(:), e_rpax(:),  ec_rpa(:,:), ec_rpax(:,:), eigiu(:,:)
  real(DP), allocatable :: ec_rpax_ev(:,:), ec_rpax_fu(:,:), ec_ttrpax(:,:)
  real(DP) :: gq2(ngm)
  real(DP), external :: get_clock
  character(len=256), allocatable :: iu_dir(:)
  character(len=256) :: temp_dir_save
  character(len=256) :: string
  character(len=6), external :: int_to_char
  integer, external  :: find_free_unit
  !
  COMPLEX(DP), ALLOCATABLE :: H(:,:,:), S(:,:,:), Haux(:,:), Saux(:,:), II(:,:)
  COMPLEX(DP), ALLOCATABLE ::  xr(:,:)
  REAL(DP), EXTERNAL :: DDOT
  REAL(DP) :: norm, aux
  !
  !
  COMPLEX(DP), ALLOCATABLE :: den_nl (:)
  COMPLEX (DP), ALLOCATABLE :: pot_best(:)
  COMPLEX(DP), ALLOCATABLE ::   depot_aux(:,:) 
  LOGICAL aux_flag
  COMPLEX(DP) :: carica
  !
  !
  !
  CALL start_clock( 'EcRPA' )
  !
  if (idiag == 0) then
     write(stdout,'(5x,''Calculation of Dielectric Eigenmodes: Tight-binding-like method'')')
  else
     write(stdout,'(5x,''Calculation of Dielectric Eigenmodes: Davidson diagonalization'')')
  endif
  !
  ! allocate local arrays
  allocate ( e_rpa(neigv), ec_rpa(nf_ec,neigv), eigiu(nf_ec,neigv) )
  !
  IF ( RPAX ) THEN
     !
     ! Allocate a vector solution of [H-e]|dpsi_nl> = -P_c[Vx_NL-vx]|psi> at k and k+q
     !
     ALLOCATE ( dpsi_nl(npwx,nbnd))
     IF ( .NOT. lgamma ) THEN
        ALLOCATE ( dpsiq_nl(npwx,nbnd))
     ELSE
        dpsiq_nl => dpsi_nl
     ENDIF
     ALLOCATE ( dnvcdn_aux(neigv) )
     ALLOCATE ( den_nl(dfftp%nnr), pot_best(dfftp%nnr), depot_aux(ngm,neigv) )
     ALLOCATE ( e_rpax(neigv), ec_rpax(nf_ec,neigv) ,ec_rpax_ev(nf_ec,neigv), ec_rpax_fu(nf_ec,neigv),ec_ttrpax(nf_ec,neigv))
     ALLOCATE ( H(nf_ec,neigv,neigv), S(nf_ec,neigv,neigv), Haux(neigv,neigv), Saux(neigv,neigv), II(neigv,neigv) )
     ALLOCATE ( xr(neigv,neigv) )
     H(:,:,:) = (0.D0, 0.D0)
     S(:,:,:) = (0.D0, 0.D0)
     Haux(:,:) = (0.D0, 0.D0)
     Saux(:,:) = (0.D0, 0.D0)
     II (:,:) = (0.D0, 0.D0)
     DO i=1,neigv ; II(i,i)=(1.D0,1.D0) ; ENDDO
     !
     ! Generate lambda grid for alternative RPAx resummations
     !
     a=0.D0 ; b=1.D0 
     ALLOCATE ( lambda(nlambda) , tl_nk(nlambda) ,wl(nlambda) )
     CALL gl_weight2( nlambda, a, b, lambda, tl_nk, wl )
     !
  ENDIF
  ! 
  ! Write some info
  !
  write(stdout,'(/,7x,"lgamma = ",L1,",  lgamma_gamma = ",L1)')lgamma, lgamma_gamma
  write(stdout,'(/,7x,"ngm = ",i6,"    nrxx = ", i6)')ngm, dfftp%nnr
  write(stdout,'(/,7x,"nf_ec = ",i2,3x,"u0_ec = ",f6.2,3x,"neigv = ",i4)')nf_ec, u0_ec, neigv
  write(stdout,'(7x,''thr_ec = '', 1PE8.1,''     tr2_ph = '', 1PE8.1 )') thr_ec, tr2_ph
  !
  ! Generate imaginary frequency grid
  !
  call gen_freq_grid ( )
  !
  !
  ! If savealldep=.true., create subfolders in the ourdir to store eigenpotentials
  ! Note that iundvw is used to write eigenpotentials to these subfolders, while
  ! iundvr is used to read/write eigenpotentials to the outdir folder
  !
  if ( savealldep ) then
     allocate ( iu_dir(0:nf_ec) )
     do iu = 1, nf_ec
        call real_to_char ( u_ec(iu), string )
!        write(stdout,*)trim( string )
        iu_dir(iu) = TRIM( tmp_dir_save ) //"iu"// TRIM(string) //"/"
        call create_directory ( iu_dir(iu) )
     enddo
     iundvw = 298
  endif
  !
  ! Check the status of calculation 
  !
  if (rpa_pot) then 
     sum_dchi(:)   = ZERO
     sum_dchi3(:)   = ZERO
     sum_dchi12(:)   = ZERO
  end if
  !
  iunrec = find_free_unit() 
  if ( ionode ) then
     call seqopn (iunrec, 'recEc', 'unformatted', exst)
     !
     if ( exst ) then
        write(stdout,'(/,7x,''Calculation is restarting'')')
        rewind(iunrec); read(iunrec) iu0, infodia
     else
        iu0 = 0; infodia = 0
     endif
  endif
#if defined __MPI
  call mp_bcast(iu0, ionode_id, intra_image_comm); call mp_bcast(infodia, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
#endif
  !
  ! Open files for writing and read calculated data if any
  !
  iudchi = 999; lrdchi = dfftp%nnr
  iuneig = find_free_unit(); lreig = neigv 
  if ( ionode ) then
     !
     call diropn( iuneig,'eig', lreig, exst )
     do iu = 1, iu0
        call davcio( e_rpa(1), lreig, iuneig, iu, -1 )
        eigiu(iu,:) = e_rpa(:)
     enddo
     !
     if ( iu0.gt.0 ) write(stdout,'(/,/,/,7x,''################### &
        &          RESULTS OBTAINED SO FAR ######################'')')
     do iu = 1, iu0
        write(stdout, '(/,7x,''Converged eigenvalues at frequency iu ='',f15.8,/)' )u_ec(iu)
        e_rpa(:)=eigiu(iu,:)
        call print_eig( neigv, e_rpa(1) )
     enddo
     if ( iu0.gt.0 ) write(stdout,'(/,/,/,7x,''################### &
        &         RESULTS FROM NEW CALCULATION ##################'')')
     !
  endif
#if defined __MPI
  call mp_bcast( eigiu, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
#endif
  !
  IF ( RPAX ) THEN
     iunfx = find_free_unit(); lrfx=neigv*neigv*2       !! H is a neigv x neig COMPLX Matrix => 2*(neigv)^2 REAL vector
     IF ( ionode ) THEN
        !  
        CALL diropn( iunfx, 'fx', lrfx, exst)
        DO iu = 1, iu0
           CALL davcio ( Haux(1,1), lrfx, iunfx, iu, -1 )
           H(iu,:,:) = -Haux(:,:)
           DO i = 1, neigv;  S(iu,i,i) = eigiu(iu,i); ENDDO
        ENDDO
        !
     ENDIF
  ENDIF
  !
#if defined __MPI
  CALL mp_bcast ( H, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
#endif
  !
  !
  ! read from disk sum_dchi's saved before corruption 
  !
  if (rpa_pot) then
     !
     !if (iu0.gt.0) then  
        !sumdchi_tmp(:,:) = 0.0_DP; sumdchi_tmp3(:,:) = 0.0_DP; sumdchi_tmp12(:,:) = 0.0_DP  
        !
        !CALL read_pot(sumdchi_tmp, nspin, "sumdchi_old")
        !CALL read_pot(sumdchi_tmp3, nspin, "sumdchi_old3")
        !CALL read_pot(sumdchi_tmp12, nspin, "sumdchi_old12")
        !
     if (iu0.gt.0) call read_write_dec (sum_dchi, sum_dchi12, sum_dchi3, -1) 
        !sum_dchi(:)  = CMPLX(sumdchi_tmp(:,nspin), DP, kind=DP) 
        !sum_dchi3(:)  = CMPLX(sumdchi_tmp3(:,nspin), DP, kind=DP)
        !sum_dchi12(:)  = CMPLX(sumdchi_tmp12(:,nspin), DP, kind=DP)
     !endif
     ! 
  endif
  !
  if (neigv.gt.nevlx) call errore('ec_acfdt','neigv TOO LARGE!!!',neigv)
  !
  !
  ! iterate ...
  !
  !OPEN( UNIT = stdout, FILE = './out_'//nd_nmbr, STATUS = 'UNKNOWN' )
  !
  if (iumax.gt.nf_ec) then
     call infomsg('ec_acfdt', 'iumax too large. Reduced to nf_ec')
     iumax = nf_ec
  endif
  !
  !
  WRITE( stdout, '(/,/,/,5X,"RPA Ec calculation in ACFD theory")' )
  !
  !  If rpax=TRUE compute exact-exchange local potential 
  !  needed for RPAX-Ec calculation
  !
  IF ( rpax ) THEN
    !
    WRITE( stdout, '(/,/,/,5X,"Initialize the exx calculation",/)' )
    aux_flag=.TRUE.
    CALL callexx_rpax (num_iter, aux_flag)
    !
    WRITE(stdout,'(/,5X,"Exx local potential calculation START",/)' )
    !
    ! COMPUTE dpsi_nl AND EVENTUALLY dpsiq_nl 
    !   1) ... Compute the exx potential ...
    !
    dvgenc(:,:,:) = ZERO
    CALL nonloc_dfpt ( )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    den_nl(:) = 0.0_DP; den_nl(:) = dvgenc(:,1,2)
    !
    ! If iter_save=0 start from v_ks for computing v_exx
    !    else start from v_xc at previous step
    !
    i=iter_save+1
    !
    ! here we activate the flag line_search to prevent 
    !    writing the output potential (v_exx) on disk  
    !
    aux_flag = line_search
    line_search = .TRUE.
!!    WRITE(stdout,*) line_search, aux_flag
    CALL approx_vxc_by_anderson (i, den_nl, pot_best)
    line_search = aux_flag
    !
    ! exx potential
    ! 
    exxvx(:) = 0.D0;  exxvx(:) = DBLE(pot_best)
    !
    WRITE(stdout,'(/,5X,"Exx local potential calculation END",/)' )
    !
    !   2) ... solve linear problem (H-e)|dpsi_nl> = -P_c(Vx_NL-vx)|psi> ...
    !
    dvgenc(:,:,:) = ZERO
    dpsi_nl(:,:) = ZERO
    IF ( .NOT. lgamma) dpsiq_nl(:,:) = ZERO
    CALL nonloc_dfpt_rpax ()
!    WRITE( stdout,'(/,3X,"Check for exx potential from oep procedure",/)' )
!    WRITE( stdout,'(/,1X,"dpsi_nl",/)' )
!    WRITE(stdout,*) dpsi_nl(1:3,1)
!    WRITE(stdout,'(/,1X,"delta rho NL -delta rho L (expected ~zero)",/)' )
!    WRITE(stdout,*)  dvgenc(1:3,1,2)
    !
  ENDIF
  !
  do iu = iu0+1, iumax
     !
     control_vc_rpa = .FALSE.
     ! 
     WRITE(stdout,'(/,/,7x,''Calculating eigenvalues at frequency &
                               &iu ='',f15.8)') u_ec(iu)
     !
     ! diagonalize the KS Res. Funct.
     !
     if (idiag == 0) then
        !
        WRITE(stdout, '(/,5x, "TIGHT BINDING METHOD NOT SUPPORTED ANYMORE. Going to stop",/)')
        lepsi = .true.; avgiter = 1.d0; iundvw = 298
        call eciu_tb ( iu, u_ec(iu), neigv, e_rpa )
        !
     else 
        !
        if ( (iu0==0) .and. (iu==(iu0+1)) ) then
           ! generate random pots if necessary 
           call init_depot( neigv, depot, 0 )
        else
           ! read converged eigenpots at previous iu
           if (savealldep) then
              temp_dir_save = tmp_dir; tmp_dir = iu_dir(iu-1)
              if ( pot_collect ) then
                 if (ionode) call diropn(iundvw,'dvr', 2*ngm_g, exst)
              else
                 call diropn(iundvw,'dvr', 2*ngm, exst)
              endif
              call init_depot( neigv, depot, -1, iundvw )
              if ( pot_collect ) then
                 if (ionode) close( unit = iundvw, status = 'keep' )
              else
                 close( unit = iundvw, status = 'keep' )
              endif
              tmp_dir = temp_dir_save 
           else
              call init_depot( neigv, depot, -1, iundvr )
           endif
        endif
        ! diagonalize dielectric matrix
        lepsi = .true.
        call diagnlres_david( u_ec(iu), neigv, depot, e_rpa, avgiter )
        nkter = int(avgiter)
        ! save eigenpotentials on disk
        if (savealldep) then
           temp_dir_save = tmp_dir; tmp_dir = iu_dir(iu)
           if ( pot_collect ) then
              if (ionode) call diropn(iundvw,'dvr', 2*ngm_g, exst)
           else
              call diropn(iundvw,'dvr', 2*ngm, exst)
           endif
           call init_depot( neigv, depot, +1, iundvw )
           if ( pot_collect ) then
              if (ionode) close( unit = iundvw, status = 'keep' )
           else
              close( unit = iundvw, status = 'keep' )
           endif
           tmp_dir = temp_dir_save 
        else
           call init_depot( neigv, depot, +1, iundvr )
        endif
        !
     endif
     !
     eigiu(iu,:) = e_rpa(:)
     if ( ionode ) call davcio( e_rpa(1), lreig, iuneig, iu, +1 )
     !
     write(stdout, '(/,7x,''Converged eigenvalues in ascending order'',/)' )
     call print_eig( neigv, e_rpa )
     write(stdout, '(/,7x,"Freq: ",f15.8, 4x,"iters: ",i3,4x,"cpu time up to now: ", &
                 &   f9.2, '' secs'')') u_ec(iu), nkter, get_clock( 'EcRPA' )
     !
     !  If rpax=TRUE compute XofxXo matrix element
     !
     IF (RPAX ) THEN 
       !
       WRITE(stdout,'(/,7x,''Calculating Exact-Exchange contribution at frequency iu ='',f15.8)') u_ec(iu)
       !
       lepsi = .TRUE.
       DO i=1,neigv
         !
         ! Auxiliary bases set
         depot_aux (:,i) = depot(:,i)
         CALL vc12_dvg( ngm, 1, xq, depot_aux(1,i) )
         !
         ! Overlap Matrix on the auxiliary bases set
         S(iu,i,i) = e_rpa(i)
         !
       ENDDO
       !
       ! Build XofxXo Matrix on the auxiliary base
       ! 
       !   -XofxXo
       CALL H_matrix (u_ec(iu), neigv, depot_aux, Haux, avgiter)
       !
!       DO i=1,neigv
!          WRITE(stdout,*) Haux(i,1:neigv)
!       ENDDO
       !
       !   Store the result
       H(iu,:,:) = -Haux(:,:)
       !
       !   Save the partial result on disk
       IF ( ionode ) CALL davcio( Haux(1,1), lrfx, iunfx, iu, +1 )
       !
       WRITE(stdout, '(/,7x,"Res. matrix at Freq: ",f15.8,''   DONE'',4x,"cpu time up to now: ", &
                   &   f9.2, '' secs'')') u_ec(iu), get_clock( 'EcRPA' )
       !   
     ENDIF
     !
     ! calculate functional derivative of KS Res. Funct.
     !
1000 continue
     lepsi = .true.
     if (rpa_pot) then
        !
        WRITE(stdout,'(/,/,/,7x,''Calculating functional derivative of Res. Func. at frequency &
                               &iu ='',f15.8)') u_ec(iu)  
        !
        call vc_scf (iu, u_ec(iu), e_rpa, neigv, depot)
        !
        ! calculate for functional derivative of Res. Func. over imaginary frequencies
        !
        sum_dchi(:) = sum_dchi(:) + DBLE(dchi_iu(:))* wu_ec(iu)/(2.0_DP*pi)
        sum_dchi3(:) = sum_dchi3(:) + DBLE(dchi_iu3(:))* wu_ec(iu)/(2.0_DP*pi)
        sum_dchi12(:) = sum_dchi12(:) + DBLE(dchi_iu12(:))* wu_ec(iu)/(2.0_DP*pi)
        !
        write(stdout, '(/,7x, "Done functional derivative of Res. Func. at frequency: ",f15.8, 2x,  &
                 &  "cpu time up to now: ",f9.2, '' secs'')') u_ec(iu), get_clock ( 'EcRPA' )
        !
        ! save again the values to disk 
        !
        call read_write_dec (sum_dchi, sum_dchi12, sum_dchi3, 1) 
        !sumdchi_tmp(:,:) = 0.0_DP; sumdchi_tmp(:,nspin) = sum_dchi(:)
        !sumdchi_tmp3(:,:) = 0.0_DP; sumdchi_tmp3(:,nspin) = sum_dchi3(:)
        !sumdchi_tmp12(:,:) = 0.0_DP; sumdchi_tmp12(:,nspin) = sum_dchi12(:)
        !
        !CALL write_pot(sumdchi_tmp, nspin, "sumdchi_old")
        !CALL write_pot(sumdchi_tmp3, nspin, "sumdchi_old3")
        !CALL write_pot(sumdchi_tmp12, nspin, "sumdchi_old12")
        !
     end if
     !
     ! save status of the calculation
     !
     if ( ionode ) then
        rewind(iunrec); write(iunrec) iu, infodia
     endif
     !
  enddo
  !
  WRITE( stdout, '(/,/,5X,"End of RPA Ec calculation")' )
  !
  !  compute RPA Ec by itegrating over frequency
  !
  IF ( iumax.eq.nf_ec ) THEN
     !
     DO iu = 1, nf_ec 
        !
        e_rpa(:) = eigiu(iu,:)
        !
        DO i = neigv, 1, -10
           !
           ! RPA
           !
           ec_rpa(iu,i) = sum( e_rpa(1:i) + log(1.d0-e_rpa(1:i)) )
!           ec_rpa(iu,i) = sum( e_rpa(1:i) + 2.D0*log( (2.d0-e_rpa(1:i))/2.D0 ) )
           !
           !
           !RPAX
           !
           IF ( RPAX ) THEN 
             !
             ec_rpax_ev(iu,i) = 0.D0
             ec_rpax_fu(iu,i) = 0.D0
             ec_rpax(iu,i)    = 0.D0
             ec_ttrpax(iu,i)  = 0.D0
             !
             !  NEW re-summ
             !
             Haux(:,:) = H(iu,:,:)
             dnvcdn_aux(:) = 0.D0
             DO k = 1, i;  dnvcdn_aux(k)=Haux(k,k); ENDDO
             ec_ttrpax(iu,i) = sum(  (1.D0/(e_rpa(1:i)*e_rpa(1:i)))* &
                                     (dnvcdn_aux(1:i)+e_rpa(1:i)*e_rpa(1:i))*(e_rpa(1:i)+ log(1.d0-e_rpa(1:i))) )
 
             !  BARE RPAx
             ! 
             Saux(:,:) = - S(iu,:,:)
             Haux(:,:) = - H(iu,:,:)
             CALL ZGEMM( 'N', 'N', neigv, neigv, neigv, -ONE, Saux, &
                        neigv, Saux, neigv, ONE, Haux, neigv )
             !
             ! 
             e_rpax(:) = 0.D0
             xr(:,:) = (0.D0, 0.D0)
             !
             CALL cdiaghg( i, i, Haux, Saux, neigv, e_rpax, xr )
             WRITE( stdout, '(/,/,5X,"RPAx Eigenvalues",/)' )
             IF (i == neigv) CALL print_eig( neigv, e_rpax )
             !
             ! compute <dv|chi0 vc chi0|dv> needed for Ec
             !
             DO k = 1, i
                dnvcdn_aux(k) = 0.D0
                DO j =1, i
                   dnvcdn_aux(k) = dnvcdn_aux(k) + ( xr(j,k)*Saux(j,j))*CONJG(xr(j,k)*Saux(j,j))
                ENDDO
             ENDDO
             ec_rpax(iu,i) = - sum( dnvcdn_aux(1:i)/e_rpax(1:i) * ( e_rpax(1:i) + log(1.d0-e_rpax(1:i)) ) )
             !
             !           ##### ENGEL&VONSKO and FURCHE resummation need numerical lamba integration ########
             !
             ! Integration over lambda 
             ec_lambda_ev=0.D0
             ec_lambda_fu=0.D0
             Saux(:,:) = S(iu,:,:)
             DO l = 1, nlambda
               !
               ll=lambda(l)
!!               WRITE(stdout, '(/,5x,''Lambda'',f17.8,f17.8,/)') ll, wl(l)
               !
               !  ENGEL & VONSKO
               !
               e_rpax(:) = 0.D0
               xr(:,:) = (0.D0, 0.D0)
               Haux(:,:) = (0.D0, 0.D0)
               Haux(:,:) = ll*H(iu,:,:)
               DO k = 1, i
                 Haux(k,k) = Haux(k,k) + Saux(k,k)
               ENDDO
               CALL cdiaghg( i, i, Haux, II, neigv, e_rpax, xr )
               !
               ! Check if Chi is well defined
               !
               IF ( i == neigv ) THEN
!                  WRITE(stdout,*) 'Lambda =',  ll
!                  CALL print_eig(i,e_rpax)
                  DO k = 1, i 
                     aux = e_rpax(k) / ( 1.D0 - ll * e_rpax(k) )
                     IF ( aux .gt. 0.D0)  WRITE(stdout,*) 'WARNING Chi EV wrong sign', ll, k, e_rpax(k) 
                  ENDDO
               ENDIF
               !
               ! compute <dv|chi0|dv> needed for Ec
               !
               DO k = 1, i
                  dnvcdn_aux(k) = 0.D0
                  DO j =1, i
                     dnvcdn_aux(k) = dnvcdn_aux(k) + ( xr(j,k)*CONJG(xr(j,k)) * Saux(j,j) )
                  ENDDO
               ENDDO
               ec_lambda_ev = - sum( e_rpax(1:i)/(1.D0-ll*e_rpax(1:i)) - dnvcdn_aux(1:i) )
               !
               !  FURCHE
               !
               e_rpax(:) = 0.D0
               xr(:,:) = (0.D0, 0.D0)
               Haux(:,:) = (0.D0, 0.D0)
               DO m = 1, i
                  DO k = 1, i
                     Haux(m,k) = ll*(Saux(m,k)**2)/(1.D0-ll*Saux(m,k)) + ll*H(iu,m,k)/( (1.D0-ll*Saux(m,m))*(1.D0-ll*Saux(k,k)) )
!                     Haux(m,k) = Saux(m,k)/(1.D0-ll*Saux(m,k)) + ll*H(iu,m,k)/( (1.D0-ll*Saux(m,m))*(1.D0-ll*Saux(k,k)) )
                     Haux(m,k) = - Haux(m,k)
                  ENDDO
               ENDDO
               CALL cdiaghg( i, i, Haux, II, neigv, e_rpax, xr ) 
               !
               ! Check if Chi is well defined
               !
               IF ( i == neigv ) THEN
                  DO k = 1, i
                     dnvcdn_aux(k) = 0.D0
                     DO j =1, i
                        dnvcdn_aux(k) = dnvcdn_aux(k) + ( xr(j,k)*CONJG(xr(j,k)) * Saux(j,j) )
                     ENDDO
                     aux = e_rpax(k) + dnvcdn_aux(k)
                     IF ( aux .gt. 0.D0)  WRITE(stdout,*) 'WARNING Chi Furche wrong sign', ll, k, e_rpax(k), dnvcdn_aux(k)
                  ENDDO
               ENDIF
!               IF ( i == neigv ) THEN
!                  DO k =1, i
!                     WRITE(stdout,*) k, e_rpax(k)! , dnvcdn_aux(k), e_rpax(k) - dnvcdn_aux(k)
!                     WRITE(stdout,*) k, e_rpax(k) - dnvcdn_aux(k)
!                  ENDDO
!               ENDIF
!               ec_lambda_fu =  - sum( e_rpax(1:i) - dnvcdn_aux(1:i) )
               ec_lambda_fu = sum( e_rpax(1:i) )
               !
!               IF (i == neigv ) WRITE(stdout, '(/,5x,''Lambda'',f17.8,f17.8,f17.8,f17.8,/)') ll, wl(l), ec_lambda_ev, ec_lambda_fu
               !
               ec_rpax_ev(iu,i) = ec_rpax_ev(iu,i) + ec_lambda_ev*wl(l)
               ec_rpax_fu(iu,i) = ec_rpax_fu(iu,i) + ec_lambda_fu*wl(l)
             ENDDO
             !
           ENDIF
           !
        ENDDO
        eciu_rpa = ec_rpa(iu,neigv) 
        IF ( RPAX ) THEN
          eciu_rpax    = ec_rpax(iu,neigv)
          eciu_rpax_ev = ec_rpax_ev(iu,neigv)
          eciu_rpax_fu = ec_rpax_fu(iu,neigv)
          eciu_ttrpax  = ec_ttrpax(iu,neigv)
          write(stdout, '(/,7x,"Freq: ",f15.8, 2x, "Ec-RPA(iu): ",f17.8, 2x, "Ec-RPAx(iu): ",f17.8  &
                         , 2x, "Ec-tRPAx(iu): ",f17.8, 2x, "Ec-tpRPAx(iu): ",f17.8, 2x, "Ec-ttRPAx(iu): ",f17.8)') &
                         u_ec(iu), eciu_rpa, eciu_rpax, eciu_rpax_ev, eciu_rpax_fu,eciu_ttrpax
        ELSE
          write(stdout, '(/,5x,"Freq: ", f15.8,  2x, "Ec-RPA(iu): ",f17.8)')  &
                               u_ec(iu), eciu_rpa
        ENDIF
     ENDDO
     !
     write(stdout,'(/,5x,''RPA Ec with different number of eigenvalues'',/)')
     !
     ecex_rpa = 0.d0
     IF ( RPAX ) ecex_rpax = 0.d0
     !
     do i = neigv, 1, -10 
        !
        ecex_rpa = 0.d0
        IF ( RPAX ) THEN
           ecex_rpax    = 0.d0 
           ecex_rpax_ev = 0.D0
           ecex_rpax_fu = 0.D0
           ecex_ttrpax  = 0.D0
        ENDIF
        !
        do iu = 1, nf_ec
           ecex_rpa = ecex_rpa + ec_rpa(iu,i) * wu_ec(iu) 
           IF ( RPAX) THEN
              ecex_rpax = ecex_rpax + ec_rpax(iu,i) * wu_ec(iu)
              ecex_rpax_ev = ecex_rpax_ev + ec_rpax_ev(iu,i) * wu_ec(iu)
              ecex_rpax_fu = ecex_rpax_fu + ec_rpax_fu(iu,i) * wu_ec(iu)
              ecex_ttrpax = ecex_ttrpax + ec_ttrpax(iu,i) * wu_ec(iu)
           ENDIF
        enddo
        !
        ecex_rpa = ecex_rpa / (2.d0 * pi)
        IF ( RPAX ) THEN  
           ecex_rpax = ecex_rpax / (2.d0 * pi)
           ecex_rpax_ev = ecex_rpax_ev / (2.d0 * pi)
           ecex_rpax_fu = ecex_rpax_fu / (2.d0 * pi)
           ecex_ttrpax = ecex_ttrpax / (2.d0 * pi)
        ENDIF
        !
        if ( i.eq.neigv ) eciu_rpa = ecex_rpa
        if ( i.eq.neigv .AND. RPAX ) eciu_rpax = ecex_rpax
        if ( i.eq.neigv .AND. RPAX ) eciu_rpax_ev = ecex_rpax_ev
        if ( i.eq.neigv .AND. RPAX ) eciu_rpax_fu = ecex_rpax_fu
        if ( i.eq.neigv .AND. RPAX ) eciu_ttrpax = ecex_ttrpax
        !
        IF ( RPAX ) THEN
           write(stdout,'(/,7x, "nevl =",i4, 5x,''RPA Ec:'',f16.6, 5x,''RPAx Ec:'',f16.6, ''(ryd.)'', &
                           5x, ''tRPAx Ec:'',f16.6, ''(ryd.)'', 5x, ''tpRPAx Ec:'',f16.6, ''(ryd.)'', &
                           5x, ''ttRPAx Ec:'',f16.6, ''(ryd.)'')')  & 
                          i, ecex_rpa, ecex_rpax, ecex_rpax_ev, ecex_rpax_fu, ecex_ttrpax
        ELSE 
           write(stdout,'(/,14x, "nevl =",i4,10x,''RPA Ec:'',f16.6)') i,ecex_rpa
        ENDIF
     ENDDO
     !
     ! this value will be used to correct total energy in PWSCF
     ecrpa =  eciu_rpa*wq
     IF ( RPAX )  ecrpa = eciu_rpax*wq
     !
     !ecex = ecex / (2.d0 * pi); err_ecex = err_ecex / (2.d0 * pi)
     write(stdout,'(/,/,5x,"Final RPA-Ec:",f17.8,'' (no weight)'',f17.8, &
                 &  '' (with weight)'', '' Ryd'')') eciu_rpa, eciu_rpa*wq
     !
     IF ( RPAX ) write(stdout,'(/,/,5x,"Final RPAx-Ec:",f17.8,'' (no weight)'',f17.8, &
                             &  '' (with weight)'', '' Ryd'')') eciu_rpax, eciu_rpax*wq
     IF ( RPAX ) write(stdout,'(/,/,5x,"Final tRPAx-Ec:",f17.8,'' (no weight)'',f17.8, &
                             &  '' (with weight)'', '' Ryd'')') eciu_rpax_ev, eciu_rpax_ev*wq
     IF ( RPAX ) write(stdout,'(/,/,5x,"Final tpRPAx-Ec:",f17.8,'' (no weight)'',f17.8, &
                             &  '' (with weight)'', '' Ryd'')') eciu_rpax_fu, eciu_rpax_fu*wq
     IF ( RPAX ) write(stdout,'(/,/,5x,"Final ttRPAx-Ec:",f17.8,'' (no weight)'',f17.8, &
                             &  '' (with weight)'', '' Ryd'')') eciu_ttrpax, eciu_ttrpax*wq
     !
  else
     !
     write(stdout,'(/,/,7x,''Calculation not completed. RPA Ec cannot be computed'')')
     !
  endif
  !
  if ( ionode ) then
     inquire( unit = iunrec, opened = opnd )
     close( unit = iunrec, status = 'delete' )
     inquire( unit = iuneig, opened = opnd )
     close( unit = iuneig, status = 'delete' )
     IF ( RPAX ) THEN 
        INQUIRE( unit = iunfx, opened = opnd )
        CLOSE( unit = iunfx, status = 'delete' )
     ENDIF
  endif
  !
  deallocate (e_rpa, ec_rpa, eigiu )
  !
  IF ( RPAX ) THEN
     DEALLOCATE (dpsi_nl)
     IF (.NOT. lgamma ) DEALLOCATE (dpsiq_nl)
     DEALLOCATE ( H, S, Haux, Saux ,II )
     DEALLOCATE ( xr )
     DEALLOCATE ( dnvcdn_aux )
     DEALLOCATE ( ec_rpax, e_rpax, ec_rpax_ev, ec_rpax_fu, ec_ttrpax )
     DEALLOCATE ( den_nl, pot_best, depot_aux)
     DEALLOCATE ( lambda, tl_nk, wl )
  ENDIF
  !
  if ( savealldep ) deallocate ( iu_dir )
  !
  CALL stop_clock( 'EcRPA' )
  !
  return
  !
end subroutine ec_acfdt


subroutine print_eig( neig, e )
  !
  use kinds,         only : dp
  use io_global,     only : stdout
  !
  implicit none
  ! i/o
  integer, intent(in) :: neig
  real(dp), intent(in) :: e(neig)
  ! dummy
  integer :: l, m, n
  real(dp) :: ew(neig)
  !
  m = INT( neig / 10 ); ew(1:neig) = e(1:neig)
  do l = 1, m
     write(stdout,'("%",7x, 10(f14.7,1x))') (ew(n), n=10*(l-1)+1,10*l)
     !WRITE(stdout,'(7x, 10(f19.12,1x))') (ew(n), n=10*(nb1-1)+1,10*nb1)
  enddo
  if ( (neig-10*m) .gt. 0 ) &
     write(stdout,'("%",7x, 10(f14.7,1x))') (ew(n), n=10*m+1,neig)
  FLUSH(stdout)
  !
  return
  !
end subroutine print_eig

