!
! Copyright (C) 2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------
subroutine gen_freq_grid ()
   !
   ! This routine generates imaginary frequency grid for Gauss-Legendre 
   ! quadrature in RPA Ec calculation
   !
   use kinds,            only : dp
   use io_global,        only : stdout
   use control_acfdt,    only : tu_ec, u_ec, wu_ec, nf_ec, u0_ec
   use wvfct,            only : et
   use klist,            only : nelec, nkstot, lgauss
   USE wvfct,            only : nbnd
   !
   implicit none
   !
   real(dp), allocatable :: t_nk(:), w_nk(:)
   real(dp) :: u0
   integer :: iu, occ_bnd
   real(dp) :: a, b, ehl, c, ehomo, elumo
 
   allocate( t_nk(nf_ec), w_nk(nf_ec) )
   !
   ! If homo-lumo gap available select adapted-to-system mesh
   !
   occ_bnd = NINT(nelec/2.D0) 
   IF ( nbnd .gt. occ_bnd .AND. .NOT. lgauss )  THEN
      a =0.D0
      b = 2.D0*ATAN(1.D0)
      ehomo = MAXVAL( et(occ_bnd,  1:nkstot) )
      elumo = MINVAL( et(occ_bnd+1,1:nkstot) )
      ehl = elumo-ehomo
      !   Transormation u = (ehl -c*z ) * tan(z) with z in [0:pi/2]
      call gl_weight2 ( nf_ec, a, b, tu_ec, t_nk, w_nk )
      c = (u0_ec/TAN(tu_ec(nf_ec)) - ehl )/tu_ec(nf_ec)
      DO iu = 1, nf_ec 
         u_ec (iu) = ( ehl + c*tu_ec(1-iu+nf_ec) ) * TAN( tu_ec(1-iu+nf_ec) )
         wu_ec (iu) = w_nk(1-iu+nf_ec) * ( c*TAN( tu_ec(1-iu+nf_ec) ) + & 
         ( ehl + c*(tu_ec(1-iu+nf_ec)) ) * ( 1.D0 + ( TAN( tu_ec(1-iu+nf_ec) ) )**2 ) )
      ENDDO
      !u_ec (:) = ( ehl + c*tu_ec(:) ) * TAN( tu_ec(:) )
      !wu_ec (:) = w_nk(:) * ( c*TAN( tu_ec(:) ) + ( ehl + c*(tu_ec(:)) ) * ( 1.D0 + ( TAN( tu_ec(:) ) )**2 ) )
!      c = (u0_ec/TAN(tu_ec(nf_ec)) - ehl )/(tu_ec(nf_ec))**2
!      u_ec (:) = ( ehl + c*(tu_ec(:))**2 ) * TAN( tu_ec(:) )
!      wu_ec (:) = w_nk(:) * ( 1.D0 + ( TAN( tu_ec(:) ) )**2 ) * ( ehl + 2*c*TAN(tu_ec(:)) )
      !
      write(stdout,'(/,5x,"HOMO-LUMO gap", 5x , "c-PARAMETER")')
      write(stdout,'(7x,f15.8,f15.8)') ehl, c
      !
      write(stdout,'(/,5x,"List of frequency and weight")')
      do iu = 1, nf_ec
         write(stdout,'(7x,f15.8,f15.8)') u_ec(iu), wu_ec(iu)
      enddo
      RETURN
   ENDIF
   !
   ! If homo-lumo gap not available select standard mesh
   !
   WRITE (stdout,'(/,5x,"NO HOMO-LUMO GAP DATA")')
   WRITE (stdout,'(/,5x,"Standard Mesh")')
   call gl_weight( nf_ec, t_nk, w_nk )
   u0 = u0_ec * (1.d0+t_nk(1))/(1.d0-t_nk(1))
   !
   tu_ec(:) = t_nk(:)
   wu_ec(:) = w_nk(:) * 2.d0*u0/((1.d0+tu_ec(:))**2)
   u_ec (:) = u0 * (1.d0-tu_ec(:)) / (1.d0+tu_ec(:))
   write(stdout,'(/,5x,"List of frequency and weight")')
   do iu = 1, nf_ec
      write(stdout,'(7x,f15.8,f15.8)') u_ec(iu), wu_ec(iu)
   enddo

   deallocate(t_nk, w_nk)
   !
   return
   !
end subroutine gen_freq_grid

subroutine gl_weight( nfs, t_nk, w_nk)
   !--------------------------------------------------------------------------
   !
   ! this routine gives abscissas and weight for Gauss-Legendre quadrature  
   !
   use kinds,            only : dp
   !
   implicit none
   !
   integer, intent(in) :: nfs
   real(dp), intent(out) :: t_nk(nfs), w_nk(nfs)
   !
   call gauleg(-1.d0, 1.d0, t_nk, w_nk, nfs)
   !
   return
   !
end subroutine gl_weight
!----------------------------------------------------------------------
subroutine gauleg(x1,x2,x,w,n)
   !
   ! This routine compute abscissas and weights used in
   ! Gauss-Legendre integration 
   ! Adapted from gauleg.f routine in Numerical Recipe p.145
   !
   use kinds,     only: DP
   !
   implicit none
   !
   ! I/O variables
   !
   integer :: n           ! # of points used to approx. integral
   real(DP) :: x1, x2     ! lower and upper limits of the integral
                          ! ( must be -1 and 1 for both integration
                          !   over freq. and coupling const. )
   real(DP) :: x(n), w(n) ! arrays contain abscissas and weights
   !
   ! Local variables
   !
   real(DP), parameter :: eps=3.d-14
   integer :: i,j,m
   real(DP) :: p1,p2,p3,pp,xl,xm,z,z1
   !
   !
   m=(n+1)/2
   xm=0.5d0*(x2+x1)
   xl=0.5d0*(x2-x1)
   do 12 i=1,m
      z=cos(3.141592654d0*(i-.25d0)/(n+.5d0))
1     continue
         p1=1.d0
         p2=0.d0
         do 11 j=1,n
            p3=p2
            p2=p1
            p1=((2.d0*j-1.d0)*z*p2-(j-1.d0)*p3)/j
11       continue
         pp=n*(z*p1-p2)/(z*z-1.d0)
         z1=z
         z=z1-p1/pp
      if(abs(z-z1).gt.eps) goto 1
      x(i)=xm-xl*z
      x(n+1-i)=xm+xl*z
      w(i)=2.d0*xl/((1.d0-z*z)*pp*pp)
      w(n+1-i)=w(i)
12 continue
   !
   return
   !
   end subroutine gauleg

subroutine gl_weight2( nfs, a, b, u_nk, t_nk, w_nk )
   !--------------------------------------------------------------------------
   !
   ! this routine gives abscissas and weights of frequencies integration in
   ! Gauss-Legendre method with transformation u=(b+a)/2 + (b-a)/2*t
   ! Note: the weights w_nk(:) also contain term (b-a)/2 due to transformation
   !
   use kinds,   only : DP
   !
   implicit none
   !
   integer :: nfs
   real(DP) :: u_nk(nfs), t_nk(nfs), w_nk(nfs)
   real(DP) :: a, b
   !
   integer :: i
   !
   if ( b<=a ) call errore('gl_weight2', 'b < a', 1)
   !
   call gauleg(-1.d0, 1.d0, t_nk, w_nk, nfs)
   !
   do i = 1, nfs
      u_nk(i) = 0.5d0*(b+a) + 0.5d0*(b-a)*t_nk(i)
      w_nk(i) = 0.5d0*(b-a)*w_nk(i)
   enddo
   !
   return
   !
end subroutine gl_weight2

subroutine real_to_char( r, string_ )
  !
  ! This stupid piece of code converts a real number
  ! to character with "." replaced by "_". The real
  ! number must be less than 10000, and only five 
  ! digits after "." are converted
  !
  use kinds,         only : dp
  !
  real(dp), intent(in) :: r
  character(len=256), intent(out) :: string_
  !
  real(dp) :: x
  integer :: i, intx, decx
  character(len=11) :: string
  character(len=6), external :: int_to_char
  !
  x = r
  if ( x <= 0.d0) call errore('real_to_char','real number must be positive',1)
  if ( x >= 1.d4) call errore('real_to_char','real number too large',1)
  !
  j = int(x); i = j/1000
  string(1:1) = trim( int_to_char(i) )
  !
  i = (j - 1000*(j/10000))/100
  string(2:2) = trim( int_to_char(i) )
  !
  i = (j - 100*(j/100))/10
  string(3:3) = trim( int_to_char(i) )
  !
  i = (j - 10*(j/10))
  string(4:4) = trim( int_to_char(i) )
  !
  !
  string(5:5) = '_'
  !
  !
  x = 1000000.d0 * ( r - int(r) ) 
  !
  i = int(x)/100000
  string(6:6) = trim( int_to_char(i) )
  !
  i = (int(x) - 100000*(int(x)/100000))/10000
  string(7:7) = trim( int_to_char(i) )
  !
  i = (int(x) - 10000*(int(x)/10000))/1000
  string(8:8) = trim( int_to_char(i) )
  !
  i = (int(x) - 1000*(int(x)/1000))/100
  string(9:9) = trim( int_to_char(i) )
  !
  i = (int(x) - 100*(int(x)/100))/10
  string(10:10) = trim( int_to_char(i) )
  !
  i = (int(x) - 10*(int(x)/10))
  string(11:11) = trim( int_to_char(i) )
  !
  string_ = string
  !
  return
  !
end subroutine real_to_char
