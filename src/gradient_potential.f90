
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE gradient_potential ( dn, infor, guess, conv)
 !-----------------------------------------------------------------
 !
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE cell_base,     ONLY : omega
 USE fft_base,      ONLY : dfftp
 USE control_acfdt, ONLY : dvgenc, sum_dchi, vx_exx, vc_rpa, vx_init, vc_init
 USE scf,           ONLY : rho, rho_core, rhog_core
 USE acfdt_scf,     ONLY : gradsave, grad, vxx_guess
  !
  IMPLICIT NONE
  ! I/O variable
  LOGICAL , INTENT(IN)    :: infor
  LOGICAL , INTENT(IN)    :: guess
  REAL(DP), INTENT(INOUT) :: dn(dfftp%nnr, nspin) 
  LOGICAL , INTENT(OUT), OPTIONAL :: conv
  !
  !local variables
  !
  INTEGER :: ndim, i
  !    
  COMPLEX(DP), ALLOCATABLE :: deff_hatree (:)
  !
  REAL(DP):: sum_tmp, norm
  !
  REAL(DP) :: aux1(dfftp%nnr)
  REAL(DP) :: aux2(dfftp%nnr)
  REAL(DP):: etx_init, vtx_init, etc_init, vtc_init
  !
  ndim = dfftp%nnr
  !
  ALLOCATE(deff_hatree(ndim))
  !
  dn(:,:) = 0.0_DP
  !----------------------------------------------------------------------------!
  !                     ! Gradien density by EXX !                             !
  !----------------------------------------------------------------------------!
  !
  !---density of exx; dn_nl = dExx/dv ---!
  !
  IF(vx_exx) THEN
    !
    !calculate dn(r) wrt V_x Fock
    !
    dvgenc(:,:,:) = ZERO
    CALL nonloc_dfpt ( )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    !
    dn(:,1) = DBLE(dvgenc(:,1,2))
    write(stdout,*) "hello"
    write(stdout,*) guess
    !
    IF(guess) THEN
      !
      aux1(:) = dn(:,1)
      !CALL solve_oep_iterative(aux1, aux2)
      ! 
      CALL vxc_init( rho, rho_core, rhog_core, &
                 etx_init, vtx_init, etc_init, vtc_init, vx_init, vc_init )
      !
      vxx_guess(:) = 0.0_DP; vxx_guess(:) = aux2(:)- vx_init(:, nspin)!-vc_init(:, nspin)
      do i = 1, 50
       write(stdout,*) i, aux1(i), aux2(i), vxx_guess(i)
      enddo
      !
    ENDIF
    !
  ENDIF
  !----------------------------------------------------------------------------!
  !                     ! Gradien density by RPA !                             !
  !----------------------------------------------------------------------------!
  !
  IF(vc_rpa) THEN
    !
    !put dn(r) = \dEc(RPA)/dv(r) wrt Ec_RPA
    !it is DBLE(sum_dchi(:))
    !
    dn(:,1) = dn(:,1) +  DBLE(sum_dchi(:))
    !
  ENDIF
  !
  !--------------------------------------------------------!
  !       ! Gradien density by Xo|(Vh+Vion)-Veff> !        !
  !--------------------------------------------------------!
  CALL gra_vrs_vhi ( deff_hatree )
  !
  dn(:,1) = dn(:,1) + DBLE(deff_hatree(:))
  !
  !--------------------------------------------------------!
  !
  !!!!
  dn(:,1) = -1.0_DP*dn(:,1)
  !!!!
  !  
  IF (infor) THEN
     !
     sum_tmp = 0.0_DP
     DO i = 1, ndim
        sum_tmp = sum_tmp + dn(i,nspin) * dn(i,nspin)
     ENDDO
     !
     !estimate the convergency of OEP 
     norm = sum_tmp*omega/(dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
#if defined __MPI
   CALL mp_sum( sum_tmp, intra_pool_comm )
   CALL mp_sum(    norm, intra_pool_comm )
#endif  
     !
     norm = DSQRT(norm)
     !this variables, used for conjugate gradient, if any.
     gradsave = grad
     grad     = sum_tmp
     !
 !!!!
     WRITE(stdout,*) "convert", norm, grad, gradsave 
 !!!!
     IF (norm <= 1.0D-05) conv = .true.
  ENDIF
  !  
  DEALLOCATE(deff_hatree)
  !
  RETURN
  !
END SUBROUTINE gradient_potential
!
!------------------------------------------------------------------
SUBROUTINE gra_vrs_vhi( deff_ha, vhion_veff )
 !-----------------------------------------------------------------
 !
 ! Veff <=> vrs; As vrs = Vhxc + vltot(unchanged), 
 !   |deff_ha> = Xo(new)|(Vh(new) +Vltot)- Veff(new)>
 ! 
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE lsda_mod,      ONLY : nspin
 USE qpoint,        ONLY : xq
 USE fft_base,      ONLY : dfftp
 USE scf,           ONLY : rho, vrs, vltot
 !
 USE control_acfdt, ONLY : control_vc_rpa, dvgenc
 USE acfdt_scf,     ONLY : scf_rpa_plus 
 !
 IMPLICIT NONE 
 ! 
 ! I/O variable
 ! 
 COMPLEX(DP), INTENT(INOUT) :: deff_ha(dfftp%nnr)
 REAL(DP),    INTENT(INOUT) :: vhion_veff(dfftp%nnr, nspin)
 !
 ! local variables
 !
 INTEGER :: is
 REAL   (DP), ALLOCATABLE :: vh_new(:,:)
 REAL   (DP), ALLOCATABLE :: rpavc_plus(:,:)
 COMPLEX(DP), ALLOCATABLE :: deff_dv(:)
 REAL(DP) :: uffa, uffa2
 !
 control_vc_rpa = .FALSE.
 vhion_veff(:,:)=  0.0_DP
 deff_ha (:)    =  ZERO 
 !
 ALLOCATE ( vh_new(dfftp%nnr,nspin), rpavc_plus(dfftp%nnr,nspin), deff_dv(dfftp%nnr) )
 !
 ! ... X0(lambda) | Veff_lambda >
 !
 ! ... solve the linear system to obtain the density response
 !dvgenc(:,:,:) = ZERO; dvgenc(:,1,1) = CMPLX(vrs(:,1), 0.D0, kind=DP)
 dvgenc(:,:,:) = ZERO; dvgenc(:,:,1) = CMPLX(vrs(:,:), 0.D0, kind=DP)
 CALL solve_linter_iu ( 0.0_DP )
 IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
 !
 deff_dv(:) = ZERO; deff_dv(:) = dvgenc(:,1,2)
 !
 !
 ! ... recalculate Hartree potential
 !
 vh_new(:,:) = 0.0_DP
 CALL v_h( rho%of_g, uffa, uffa2, vh_new)
 !
 ! ... compute local rpa_plus potential, if any 
 !
 IF (scf_rpa_plus) THEN
    !
    CALL pw_rpa_plus_pot (rpavc_plus) 
    !
    vh_new(:,:)  = vh_new(:,:) + rpavc_plus(:,:)
    !
 ENDIF 
 !  
 ! ... |vh_new+vion>, for each spin ..
 !
 DO is = 1, nspin
    vh_new(:,is) = vh_new(:,is) + vltot(:) 
 ENDDO 
 !
 ! ... X0(lambda) | vh_new+vion >
 !
 ! ... solve the linear system to obtain the density response
 !
 dvgenc(:,:,:) = ZERO; dvgenc(:,:,1) = CMPLX(vh_new(:,:), 0.D0, kind=DP) 
 CALL solve_linter_iu ( 0.0_DP )
 IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
 !
 deff_ha (:) = dvgenc(:,1,2) - deff_dv(:)
 !
 ! ... |vh_new+vion> - |Veff_lambda> 
 !
 vhion_veff(:,:) = vh_new(:,:) - vrs(:,:) 
 !
 DEALLOCATE ( vh_new, rpavc_plus, deff_dv )
 !
 RETURN
 !
END SUBROUTINE gra_vrs_vhi
!
!
!------------------------------------------------------------------
SUBROUTINE update_potential ( iter, dv_gold, dv_gnew)
 !-----------------------------------------------------------------
 !
 ! Note: the variables dv_gnew is gradient dE/dv at iteration (i+1)_th
 !       the variables dv_gold is gradient dE/dv at iteration   (i)_th
 ! Updated gradient potential |dV>:
 !        |dV_new> = |dv_gnew> + gamma * |dV_old> 
 ! where: gamma = <dv_gnew| (dv_gnew - dv_gold)>/<dv_gold|dv_gold> Polak-Ribiere
 ! where: gamma = <dv_gnew| dv_gnew>/<dv_gold|dv_gold>           Fletcher_Reeves
 !
 USE kinds,         ONLY : DP
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE fft_base,      ONLY : dfftp
 USE acfdt_scf,     ONLY : grad, gradsave, dv_update, & 
                           conjug_update, oep_restart_flag
 !
 IMPLICIT NONE
 ! 
 ! I/O variable
 INTEGER , INTENT(IN)  :: iter
 REAL(DP), INTENT(IN)  :: dv_gnew(dfftp%nnr,nspin)
 REAL(DP), INTENT(IN)  :: dv_gold(dfftp%nnr,nspin)
 ! local variable
 LOGICAL, PARAMETER    :: conj_PL = .FALSE.
 REAL(DP) :: beta
 REAL(DP) :: pr_conj
 INTEGER  :: i
 !
 IF(iter == 1 .or. oep_restart_flag)THEN
   !
   dv_update(:,nspin) = dv_gnew(:,nspin)
   !
   RETURN
   !
 ENDIF
 !
 IF(conjug_update) THEN 
   !
   pr_conj = 0.0_DP
   DO i = 1, dfftp%nnr
      pr_conj = pr_conj + dv_gnew(i,nspin)*dv_gold (i,nspin) 
   ENDDO
   !
#if defined __MPI
   CALL mp_sum( pr_conj, intra_pool_comm )
#endif  
   !
   IF (conj_PL) THEN
     beta = (grad-pr_conj)/gradsave 
     IF (beta <= 0.0_DP) beta = 0.0_DP
   ELSE 
     beta = grad/gradsave 
   ENDIF
   !
   dv_update(:,nspin) = dv_gnew(:,nspin) + beta*dv_update(:,nspin)
   !
 ELSE 
   !
   dv_update(:,:)     = 0.0_DP
   dv_update(:,nspin) = dv_gnew(:,nspin) 
   !
 ENDIF 
 !
 RETURN
 !
END SUBROUTINE update_potential
!
!
!------------------------------------------------------------------
SUBROUTINE inverted_chi_vxc ( dn, iter)
 !-----------------------------------------------------------------
 !
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE fft_base,      ONLY : dfftp
 USE control_acfdt, ONLY : dvgenc, control_vc_rpa, vx_exx, vc_rpa, &
                           vx_init, vc_init
 USE acfdt_scf,     ONLY : dn_1step
 USE scf,           ONLY : rho, rho_core, rhog_core
  !
  IMPLICIT NONE
  ! I/O variable
  INTEGER , INTENT(IN)     :: iter
  REAL(DP), INTENT(INOUT)  :: dn(dfftp%nnr, nspin)
  !
  !local variables
  !
  LOGICAL, PARAMETER :: inverted_iteration = .true.
  INTEGER :: ndim, i  
  !    
  REAL(DP):: etx_init, vtx_init, etc_init, vtc_init
  !
  REAL(DP) :: aux1(dfftp%nnr)
  REAL(DP) :: aux2(dfftp%nnr)
  REAL(DP) :: pot3(dfftp%nnr)
  !
  control_vc_rpa = .FALSE.
  ndim = dfftp%nnr 
  !----------------------------------------------------------!
  ! At iter == 1, we know that:                              !
  !               dn(input) = dExc/dV - Xo|Vxc(lda)>         !
  ! => we can simplify the update of veff by :               !
  !  Veff(2) = Veff(1) + lambda*(Xo^-1| dExc/dV>  - Vxc(lda))!
  !							     !
  ! At output this routine: dn = Xo^-1| dExc/dV>  - Vxc(lda) !
  !   Therefore, we need subtract Xo|Vxc(lda)> from dn(input)!
  !   then plus -Vxc(lda) to dn(input)                       !
  !----------------------------------------------------------!
  !
  ! recalculate XC(initial) potential 
  !
  !vx_init(:,:) = 0.0_DP; vc_init(:,:) = 0.0_DP
  !CALL vxc_init( rho, rho_core, rhog_core, &
  !     etx_init, vtx_init, etc_init, vtc_init, vx_init, vc_init )
  !
  !dvgenc(:,:,:) = ZERO; dvgenc(:,1,1) = CMPLX((vx_init(:, nspin) + vc_init(:, nspin)), 0.D0, kind=DP)
  !CALL solve_linter_iu ( 0.0_DP )
  !IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
  ! 
  !--- |potential> = Xo^-1 |density>---!
  !
  !aux1(:) = 0.0_DP; aux1(:) = dn_1step(:) - DBLE(dvgenc(:,1,2))
  !aux1(:) = 0.0_DP; aux1(:) = dn_1step(:) 
  !!!!!!!
  !IF (inverted_iteration) THEN
     !
  !   CALL solve_oep_iterative (dn, pot3)
     !
  !   aux1(:) = 0.0_DP; aux1(:) = DBLE(dec_dv_12(:))
  !   CALL solve_oep(dfftp%nnr, aux1, aux2 )
     !
  !   dn(:,nspin) = 0.0_DP; dn(:,nspin) = pot3(:) + aux2(:)
  !ELSE
     !
  !   aux1(:) = 0.0_DP; aux1(:) = dn(:,1)
  !   CALL solve_oep(dfftp%nnr, aux1, aux2 )
  !   dn(:,1) = 0.0_DP; dn(:,1) = aux2(:)
     !
  !ENDIF
  ! 
   !!!! test
  !WRITE(stdout,*) "deltaV nenenene"
  !DO i = 1, 50
  !   WRITE(stdout,*) i,  pot3(i) 
  !ENDDO
 !!!
  !
  aux1(:) = 0.0_DP; aux1(:) = dn(:,1) 
  CALL update_pot_rpa( ndim, aux1, aux2 )
  !CALL solve_oep(dfftp%nnr, aux1, aux2 )
  ! 
  dn(:,1) = aux2(:)
  !
  RETURN
  ! 
END SUBROUTINE inverted_chi_vxc

