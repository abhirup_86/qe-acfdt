!
! Copyright (C) 2009 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------
subroutine check_initial_oep_status (num_iter) 
  !--------------------------------------------------------------------
  !
  !	This routine is a driver for checking status running of OEP.
  !     The acfdt run can be restarted or started from scratch
  !     
  USE kinds,                ONLY : DP
  USE io_global,            ONLY : stdout
  USE lsda_mod,             ONLY : nspin
  USE scf,                  ONLY : vrs 
  USE acfdt_ener,           ONLY : acfdt_eband
  USE acfdtest,             ONLY : off_vrs_setup
  USE acfdt_scf,            ONLY : hf_scfrpa, scf_exxrpa, vrs_save, &
                                   iter_save, iter_oep, etot_old, &
                                   oep_restart_flag, oep_recover,    &
                                   restart_from_scratch, line_search,&
                                   poly_search,print_result, lambda_opt,&
                                   save_norm_1, save_norm_2, &
                                   stopped_first_iter, & 
                                   rpa_done, oep_done, chi0_done, iter_save0
  !
  implicit none
  !
  ! I/O variables
  integer, intent(inout) :: num_iter
  !
  logical :: exst
  !
  INTEGER, EXTERNAL :: find_free_unit
  !
  if (oep_recover) then
     !
     if (scf_exxrpa) write(stdout,'(/,7x,''.... SCF-RXXRPA CALCULATION IS RESTARTING ....'')')
     if (hf_scfrpa) write(stdout,'(/,7x,''....  HF-SCFRPA CALCULATION IS RESTARTING ....'')')
     !
     CALL read_oep_status ()
     iter_save0 = iter_save
     !
     oep_restart_flag = .true. 
     ! 
  else
     !
     if (scf_exxrpa) write(stdout,'(/,7x,''.... SCF-EXXRPA CALCULATION IS STARTING ....'')')
     if (hf_scfrpa) write(stdout,'(/,7x,''....  HF-SCFRPA CALCULATION IS STARTING ....'')')
     oep_restart_flag = .false.
     iter_save = 0
     iter_save0 = 0
     iter_oep  = 0
     acfdt_eband = 0.0_DP
     !
  endif
  !
  num_iter = iter_save
  !
     write(stdout,*) "chi0_done", chi0_done
     write(stdout,*) "rpa_done",  rpa_done
     write(stdout,*) "oep_done",  oep_done
     write(stdout,*) "stopped_first_iter", stopped_first_iter
     write(stdout,*) "iter_save", iter_save
     write(stdout,*) "iter_oep",  iter_oep
     write(stdout,*) "One_body_term", acfdt_eband
     write(stdout,*) "oep_restart_flag", oep_restart_flag
     write(stdout,*) "restart_from_scratch", restart_from_scratch
     write(stdout,*) "line_search",  line_search
     write(stdout,*) "poly_search",  poly_search
     write(stdout,*) "print_result", print_result
     write(stdout,*) "lambda_opt,",  lambda_opt
     write(stdout,*) "save_norm_1, and save_norm_2", save_norm_1, save_norm_2
     write(stdout,*) "off_vrs_setup", off_vrs_setup
  !
  !  In case calculation is restarted, it saved vrs is read in acfdt_readin.
  !  Here we pass it to vrs(:,:), to use in solving linear DFPT equations.
  !
  if (oep_restart_flag) then
   !
   if(.not.stopped_first_iter) then 
     !
     vrs(:,:) = vrs_save(:,:)
     ! 
   endif
     !
  endif
  !  
  DEALLOCATE (vrs_save)
  !
  return
  !
end subroutine check_initial_oep_status
!
!---------------------------------------------------------------
SUBROUTINE allocate_oep ( )
  !-------------------------------------------------------------
  USE fft_base,             ONLY : dfftp
  USE lsda_mod,             ONLY : nspin
  USE wvfct,                ONLY : npwx, nbnd
  USE gvect,                ONLY : ngm
  USE control_acfdt,        ONLY : neigvchi0
  USE acfdt_scf,            ONLY : dv_g, dv_update, savedv_g, vxx_guess, &
                                   b_in, sum_q_dden, sum_q_pot
  !
  ALLOCATE ( dv_g(dfftp%nnr, nspin), dv_update(dfftp%nnr, nspin))
  ALLOCATE ( savedv_g(dfftp%nnr, nspin), vxx_guess(dfftp%nnr))
  ! 
  ALLOCATE ( sum_q_dden(dfftp%nnr, nspin), sum_q_pot (dfftp%nnr, nspin) )
  !
  ! ... 2. for OEP direct ... 
  IF (oep_method == 4) ALLOCATE ( b_in (neigvchi0) )
  !
  RETURN
  !
END SUBROUTINE allocate_oep
!
!---------------------------------------------------------------
SUBROUTINE deallocate_oep ( )
  !-------------------------------------------------------------
  USE acfdt_scf,      ONLY : dv_g, dv_update, savedv_g, vxx_guess, &
                             b_in, sum_q_pot, sum_q_dden
  !   
  DEALLOCATE (dv_g, dv_update)
  !
  DEALLOCATE (sum_q_pot, sum_q_dden)
  !
  DEALLOCATE (savedv_g, vxx_guess)
  !
  ! ... 2. for OEP direct ... 
  IF (ALLOCATED (b_in) ) DEALLOCATE ( b_in )
  !
  RETURN
  !
END SUBROUTINE deallocate_oep


!---------------------------------------------------------------
SUBROUTINE write_oep_status ( )  
!---------------------------------------------------------------
      !
      ! ... write scf-ACFDT infor
      !  
      USE io_files,      ONLY : prefix
      USE io_global,     ONLY : ionode 
      USE xml_io_base,   ONLY : create_directory
      USE save_ph,       ONLY : tmp_dir_save
      USE iotk_module
      USE acfdt_ener,    ONLY : acfdt_eband
      USE control_acfdt, ONLY : ex_exx, ecrpa 
      USE acfdt_scf,     ONLY : chi0_done, rpa_done, oep_done, &
                                iter_save, iter_oep, etot_old, line_search,&  
                                poly_search, print_result, lambda_opt, &
                                save_norm_1, save_norm_2, stopped_first_iter
      USE acfdtest,      ONLY : off_vrs_setup   
      !
      IMPLICIT NONE 
      !      
      CHARACTER(LEN=256) :: file_extension
      CHARACTER(LEN=256) :: dirname, file_base, filename
      CHARACTER(LEN=256) :: tmp_dir
      CHARACTER(LEN=256) :: ext
      INTEGER            :: ierr
      INTEGER, PARAMETER :: iforunit = 29
      !
      file_extension = 'xml'
      ext = '.'
      !
      tmp_dir = tmp_dir_save
      !
      dirname = TRIM( tmp_dir ) // TRIM( prefix ) // '.save'
      !
      CALL create_directory( dirname )
      !
      file_base = TRIM( dirname ) // '/saved_oep_ifor' // TRIM( ext )
      !
      filename = TRIM (file_base) // TRIM( file_extension )
      !
      IF ( ionode ) THEN
         CALL iotk_open_write( iforunit, FILE = filename, BINARY = .FALSE., IERR = ierr )
         CALL errore( 'write_oep_status', 'cannot open' // TRIM( filename ) // ' file for writing', ierr )
      END IF

      IF ( ionode ) THEN
      !     
      CALL iotk_write_begin(iforunit, "ACFDT" )
      !
      CALL iotk_write_dat(iforunit, "CHI0_done", chi0_done )
      CALL iotk_write_dat(iforunit, "RPA_done",  rpa_done )
      CALL iotk_write_dat(iforunit, "OEP_done", oep_done )
      ! 
      ! Saving control flags
      !
      call iotk_write_dat(iforunit, "stopped_first_iter", stopped_first_iter )
      call iotk_write_dat(iforunit, "iter_save", iter_save )
      call iotk_write_dat(iforunit, "iter_oep ", iter_oep  )
      call iotk_write_dat(iforunit, "etot_old ", etot_old  )
      call iotk_write_dat(iforunit, "line_search", line_search )
      call iotk_write_dat(iforunit, "poly_search", poly_search )
      call iotk_write_dat(iforunit, "print_result", print_result )
      call iotk_write_dat(iforunit, "lambda_opt", lambda_opt )
      call iotk_write_dat(iforunit, "save_norm_1", save_norm_1 )
      call iotk_write_dat(iforunit, "save_norm_2", save_norm_2 )
      call iotk_write_dat(iforunit, "off_vrs_setup", off_vrs_setup )
      !
      ! Saving energy component ... 
      ! 
      CALL iotk_write_dat(iforunit, "One_body_term", acfdt_eband )
      CALL iotk_write_dat(iforunit, "Fock_energy" , ex_exx )
      CALL iotk_write_dat(iforunit, "EcRPA", ecrpa )
      !
      CALL iotk_write_end(iforunit, "ACFDT" )
      ! 
      CALL iotk_close_write( iforunit )
      END IF
      ! 
      RETURN
      !
END SUBROUTINE write_oep_status

!------------------------------------------------------------------------
SUBROUTINE read_oep_status( )
!------------------------------------------------------------------------
      !
      ! ... read  scf-ACFDT infor variables
      !
      USE io_files,       ONLY : prefix
      USE io_global,      ONLY : ionode, ionode_id 
      USE mp_global,      ONLY : intra_image_comm     
      USE mp,             ONLY : mp_bcast
      USE save_ph,        ONLY : tmp_dir_save
      USE iotk_module 
      USE xml_io_base,    ONLY : check_file_exst
      USE acfdt_ener,     ONLY : acfdt_eband
      USE control_acfdt,  ONLY : ex_exx, ecrpa  
      USE acfdt_scf,      ONLY :  chi0_done, rpa_done, oep_done, &
                                  iter_save, iter_oep, etot_old, line_search,&
                                  poly_search, print_result, lambda_opt, & 
                                  save_norm_1, save_norm_2, stopped_first_iter
      USE acfdtest,      ONLY : off_vrs_setup   
      !
      IMPLICIT NONE

      CHARACTER(LEN=256) :: file_extension
      CHARACTER(LEN=256) :: dirname, file_base, filename
      CHARACTER(LEN=256) :: tmp_dir
      CHARACTER(LEN=256) :: ext
      LOGICAL            :: exst, found
      INTEGER            :: ierr
      INTEGER, PARAMETER :: iforunit = 29
      !
      ext = '.' 
      file_extension = 'xml'
      ! 
      tmp_dir = tmp_dir_save
      !
      dirname = TRIM( tmp_dir ) // TRIM( prefix ) // '.save'
      !
      file_base = TRIM( dirname ) // '/saved_oep_ifor' // TRIM( ext )
      !
      filename = TRIM (file_base) // TRIM( file_extension )
      !
      exst = check_file_exst( TRIM(filename) )
      !
      IF ( .NOT. exst ) CALL errore('read_oep_status_xml', 'searching for '//TRIM(filename), 10)
      !
      IF ( ionode ) THEN
         CALL iotk_open_read( iforunit, FILE = filename, IERR = ierr )
         CALL errore( 'read_oep_status_xml', 'cannot open ' // TRIM( filename ) // ' file for reading', ierr )
      END IF

      CALL mp_bcast( ierr, ionode_id, intra_image_comm )
      IF ( ierr > 0 ) RETURN
      ! 
      IF ( ionode ) THEN
         CALL iotk_scan_begin( iforunit, "ACFDT", FOUND = found )
      END IF
      CALL mp_bcast( found, ionode_id, intra_image_comm )
      !
      IF ( ionode ) THEN
         IF ( found ) THEN
            ! 
            CALL iotk_scan_dat(iforunit, "CHI0_done", chi0_done )
            CALL iotk_scan_dat(iforunit, "RPA_done", rpa_done )
            CALL iotk_scan_dat(iforunit, "OEP_done", oep_done )
            !
            CALL iotk_scan_dat(iforunit, "stopped_first_iter", stopped_first_iter)
            call iotk_scan_dat(iforunit, "iter_save", iter_save )
            CALL iotk_scan_dat(iforunit, "iter_oep ", iter_oep)
            CALL iotk_scan_dat(iforunit, "etot_old ", etot_old)
            CALL iotk_scan_dat(iforunit, "line_search", line_search)
            CALL iotk_scan_dat(iforunit, "poly_search", poly_search)
            CALL iotk_scan_dat(iforunit, "print_result", print_result)
            CALL iotk_scan_dat(iforunit, "lambda_opt", lambda_opt)
            CALL iotk_scan_dat(iforunit, "save_norm_1", save_norm_1)
            CALL iotk_scan_dat(iforunit, "save_norm_2", save_norm_2)
            CALL iotk_scan_dat(iforunit, "off_vrs_setup", off_vrs_setup)
            !
            ! Saving energy component ... 
            ! 
            CALL iotk_scan_dat(iforunit, "One_body_term", acfdt_eband )
            CALL iotk_scan_dat(iforunit, "Fock_energy", ex_exx )
            CALL iotk_scan_dat(iforunit, "EcRPA", ecrpa )
            !
            CALL iotk_scan_end( iforunit, "ACFDT" )
         END IF
         CALL iotk_close_read( iforunit )
      END IF
      !
      IF (.NOT.found ) RETURN
      !
      CALL mp_bcast( chi0_done, ionode_id, intra_image_comm )
      CALL mp_bcast( rpa_done, ionode_id, intra_image_comm )
      CALL mp_bcast( oep_done, ionode_id, intra_image_comm )
      !
      CALL mp_bcast( stopped_first_iter, ionode_id, intra_image_comm )
      CALL mp_bcast( iter_save, ionode_id, intra_image_comm )
      CALL mp_bcast( iter_oep, ionode_id, intra_image_comm )
      CALL mp_bcast( etot_old, ionode_id, intra_image_comm )
      CALL mp_bcast( line_search, ionode_id, intra_image_comm )
      CALL mp_bcast( poly_search, ionode_id, intra_image_comm )
      CALL mp_bcast( print_result, ionode_id, intra_image_comm )
      CALL mp_bcast( print_result, ionode_id, intra_image_comm )
      CALL mp_bcast( lambda_opt, ionode_id, intra_image_comm )
      CALL mp_bcast( save_norm_1, ionode_id, intra_image_comm )
      CALL mp_bcast( save_norm_2, ionode_id, intra_image_comm )
      CALL mp_bcast( off_vrs_setup, ionode_id, intra_image_comm )
      !
      RETURN
      !
END SUBROUTINE read_oep_status
