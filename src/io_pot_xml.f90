!
! Copyright (C) 2001-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
MODULE io_pot_xml
  !----------------------------------------------------------------------------
  !
  USE kinds,       ONLY : DP
  USE xml_io_base, ONLY : create_directory, write_pot_xml, read_pot_xml
  !
  PRIVATE
  !
  PUBLIC :: write_pot, read_pot
  !
  ! {read|write}_pot_only:    read or write the real space charge density
  ! {read|write}_pot_general: as above, plus read or write ldaU ns coeffs
  !                           and PAW becsum coeffs.

  INTERFACE write_pot
        MODULE PROCEDURE write_pot_only
  END INTERFACE

  INTERFACE read_pot
        MODULE PROCEDURE read_pot_only
  END INTERFACE

  CONTAINS

    !------------------------------------------------------------------------
    SUBROUTINE write_pot_only( pot, nspin, extension )
      !------------------------------------------------------------------------
      !
      ! ... this routine writes the charge-density in xml format into the
      ! ... '.save' directory
      ! ... the '.save' directory is created if not already present
      !
      USE io_files, ONLY : tmp_dir, prefix
      USE fft_base, ONLY : dfftp
      USE spin_orb, ONLY : domag
      USE io_global, ONLY : ionode
      USE mp_global, ONLY : intra_pool_comm, inter_pool_comm
      !
      IMPLICIT NONE
      !
      INTEGER,          INTENT(IN)           :: nspin
      REAL(DP),         INTENT(IN)           :: pot(dfftp%nnr,nspin)
      CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: extension
      !
      CHARACTER(LEN=256)    :: dirname, file_base
      CHARACTER(LEN=256)    :: ext
      REAL(DP), ALLOCATABLE :: potaux(:)
      !
      !
      ext = ' '
      !
      dirname = TRIM( tmp_dir ) // TRIM( prefix ) // '.save'
      !
      CALL create_directory( dirname )
      !
      IF ( PRESENT( extension ) ) ext = '.' // TRIM( extension )
      !
      file_base = TRIM( dirname ) // '/effective-potential' // TRIM( ext )
      !
      IF ( nspin == 1 ) THEN
         !
         CALL write_pot_xml( file_base, pot(:,1), dfftp%nr1, dfftp%nr2, &
                  dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
                  ionode, intra_pool_comm, inter_pool_comm )
         !
      ELSE IF ( nspin == 2 ) THEN
         !
         ALLOCATE( potaux( dfftp%nnr ) )
         !
        ! potaux(:) = pot(:,1) + pot(:,2)
         !
        ! CALL write_rho_xml( file_base, potaux, dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
        !          ionode, intra_pool_comm, inter_pool_comm )
         !
        ! file_base = TRIM( dirname ) // '/spin-polarization' // TRIM( ext )
         !
        ! rhoaux(:) = rho(:,1) - rho(:,2)
         !
        ! CALL write_rho_xml( file_base, rhoaux,  dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
        !          ionode, intra_pool_comm, inter_pool_comm )
         !
         DEALLOCATE( potaux )
         !
      ELSE IF ( nspin == 4 ) THEN
         !
       !  CALL write_rho_xml( file_base, rho(:,1), dfftp%nr1, dfftp%nr2, &
       !           dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
       !           ionode, intra_pool_comm, inter_pool_comm )
         !
       !  IF (domag) THEN
       !     file_base = TRIM( dirname ) // '/magnetization.x' // TRIM( ext )
            !
       !     CALL write_rho_xml( file_base, rho(:,2), dfftp%nr1, dfftp%nr2, &
       !           dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
       !           ionode, intra_pool_comm, inter_pool_comm )
            !
       !     file_base = TRIM( dirname ) // '/magnetization.y' // TRIM( ext )
            !
       !     CALL write_rho_xml( file_base, rho(:,3), dfftp%nr1, dfftp%nr2, &
       !           dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
       !           ionode, intra_pool_comm, inter_pool_comm )
            !
       !     file_base = TRIM( dirname ) // '/magnetization.z' // TRIM( ext )
            !
       !     CALL write_rho_xml( file_base, rho(:,4), dfftp%nr1, dfftp%nr2, &
       !           dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
       !           ionode, intra_pool_comm, inter_pool_comm )
       !  END IF
      END IF
      !
      RETURN
      !
    END SUBROUTINE write_pot_only
    !
    !------------------------------------------------------------------------
    SUBROUTINE read_pot_only( pot, nspin, extension )
      !------------------------------------------------------------------------
      !
      ! ... this routine reads the effective potential in xml format from the
      ! ... files saved into the '.save' directory
      !
      USE io_files, ONLY : tmp_dir, prefix
      USE fft_base, ONLY : dfftp
      USE spin_orb, ONLY : domag
      USE io_global, ONLY : ionode
      USE mp_global, ONLY : intra_pool_comm, inter_pool_comm
      !
      IMPLICIT NONE
      !
      INTEGER,          INTENT(IN)           :: nspin
      REAL(DP),         INTENT(OUT)          :: pot(dfftp%nnr,nspin)
      CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: extension
      !
      CHARACTER(LEN=256)    :: dirname, file_base
      CHARACTER(LEN=256)    :: ext
      REAL(DP), ALLOCATABLE :: potaux(:)
      !
      !
      ext = ' '
      !
      dirname = TRIM( tmp_dir ) // TRIM( prefix ) // '.save'
      !
      IF ( PRESENT( extension ) ) ext = '.' // TRIM( extension )
      !
      file_base = TRIM( dirname ) // '/effective-potential' // TRIM( ext )
      !
      IF ( nspin == 1 ) THEN
         !
         CALL read_pot_xml( file_base, pot(:,1), dfftp%nr1, dfftp%nr2, &
                  dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
                  ionode, intra_pool_comm, inter_pool_comm ) 
         !
      ELSE IF ( nspin == 2 ) THEN
         !
         ALLOCATE( potaux( dfftp%nnr ) )
         !
        ! CALL read_rho_xml( file_base, rhoaux, dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, & 
        !          ionode, intra_pool_comm, inter_pool_comm ) 
         !
        ! rho(:,1) = rhoaux(:)
        ! rho(:,2) = rhoaux(:)
         !
        ! file_base = TRIM( dirname ) // '/spin-polarization' // TRIM( ext )
         !
        ! CALL read_rho_xml( file_base, rhoaux, dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
        !          ionode, intra_pool_comm, inter_pool_comm )
         !
        ! rho(:,1) = 0.5D0*( rho(:,1) + rhoaux(:) )
        ! rho(:,2) = 0.5D0*( rho(:,2) - rhoaux(:) )
         !
         DEALLOCATE( potaux )
         !
      ELSE IF ( nspin == 4 ) THEN
         !
        ! CALL read_rho_xml( file_base, rho(:,1), dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
        !          ionode, intra_pool_comm, inter_pool_comm )
         !
        ! IF ( domag ) THEN
            !
        !    file_base = TRIM( dirname ) // '/magnetization.x' // TRIM( ext )
            !
        !    CALL read_rho_xml( file_base, rho(:,2), dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
        !          ionode, intra_pool_comm, inter_pool_comm )
            !
        !    file_base = TRIM( dirname ) // '/magnetization.y' // TRIM( ext )
            !
        !    CALL read_rho_xml( file_base, rho(:,3), dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
        !          ionode, intra_pool_comm, inter_pool_comm )
            !
        !    file_base = TRIM( dirname ) // '/magnetization.z' // TRIM( ext )
            !
        !    CALL read_rho_xml( file_base, rho(:,4), dfftp%nr1, dfftp%nr2, &
        !          dfftp%nr3, dfftp%nr1x, dfftp%nr2x, dfftp%ipp, dfftp%npp, &
        !          ionode, intra_pool_comm, inter_pool_comm )
            !
        ! ELSE
            !
        !    pot(:,2:4) = 0.D0
        !    !
        ! END IF
      END IF
      !
      RETURN
      !
    END SUBROUTINE read_pot_only
    !
END MODULE io_pot_xml
