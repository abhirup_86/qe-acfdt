!
! Copyright (C) 2001-2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
SUBROUTINE pw_rpa_plus_pot(vc_sr)
  !----------------------------------------------------------------------------
  !
  ! This routine is used to calculate short-range correlation potential, and energy
  ! in LDA/GGA no matter what functional was used in scf-ACFDT.
  ! Parametrization of LDA is the one proposed by Perdew and Wang in 1992.
  ! (for definition of short-range correlation energy see PRB, 59, 10461 (1999) )
  !
  USE kinds,            ONLY : DP
  USE constants,        ONLY : e2, eps8
  USE io_global,        ONLY : stdout
  USE lsda_mod,         ONLY : nspin
  USE cell_base,        ONLY : omega
  USE fft_base,         ONLY : dfftp
  USE scf,              ONLY : rho, rho_core, rhog_core
  USE mp_global,        ONLY : intra_pool_comm
  USE mp,               ONLY : mp_sum

  USE control_acfdt,    ONLY : ec_sr
  !
  IMPLICIT NONE
  !
  REAL(DP), INTENT(INOUT) :: vc_sr(dfftp%nnr, nspin)
  !
  ! ... local variables
  !
  REAL(DP) :: rhox, arhox, zeta, amag, vs, ex, vx(2), &
              ec, vc(2), vc_rpa(2), rhoneg(2)
    ! the total charge in each point
    ! the absolute value of the charge
    ! the absolute value of the charge
    ! local exchange energy
    ! local correlation energy
    ! local exchange potential
    ! local correlation potential
  REAL(DP) :: etc_lda, etc_rpa
  INTEGER :: ir, ipol
    ! counter on mesh points
    ! counter on nspin
  !
  REAL(DP), PARAMETER :: vanishing_charge = 1.D-10, &
                         vanishing_mag    = 1.D-20
  real(DP), parameter :: small = 1.d-10,  third = 1.d0 / 3.d0, &
                         pi34 = 0.6203504908994d0  ! pi34=(3/4pi)^(1/3)
  real(DP) :: rs
  !
  !
  CALL start_clock( 'pw_rpa_plus_pot' )
  !
  etc_lda = 0.D0
  etc_rpa = 0.D0
  rhoneg  = 0.D0
  vc_sr(:,:) = 0.D0
  !
  IF ( nspin == 1 ) THEN
     !
     ! ... spin-unpolarized case
     !
     DO ir = 1, dfftp%nnr
        !
        rhox = rho%of_r(ir,1) + rho_core(ir)
        !
        arhox = ABS( rhox )
        !
        IF ( arhox > vanishing_charge ) THEN
           !
           rs = pi34 / arhox**third ! rs as in the theory of metals: rs=(3/(4pi rho))^(1/3)
           !
           call pw(rs, 1, ec, vc(1))
           etc_lda = etc_lda + e2*ec*rhox
           !
           call pw_rpa_vc(rs, ec, vc_rpa(1))
           etc_rpa = etc_rpa + e2*ec*rhox
           !
           vc_sr (ir, nspin) = e2*( vc(1)- vc_rpa(1))
           !
        ENDIF
        !
        IF ( rho%of_r(ir,1) < 0.D0 ) rhoneg(1) = rhoneg(1) - rho%of_r(ir,1)
        !
     END DO
     !
  ELSE
     !
     CALL errore('ec_sr_lda','ONLY implemented for spin-unpolarized systems',1) 
     !
  ENDIF
  !
  CALL mp_sum(  rhoneg , intra_pool_comm )
  !
  rhoneg(:) = rhoneg(:) * omega / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  IF ( rhoneg(1) > eps8 .OR. rhoneg(2) > eps8 ) &
     WRITE( stdout,'(/,5X,"negative rho (up, down): ",2E10.3)') rhoneg
  !
  ! ... energy terms, local-density contribution
  !
  ec_sr = etc_lda - etc_rpa
  ec_sr = omega * ec_sr / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  ! ... add gradient corrections (if any)
  !
  !CALL gradcorr_ec_only( rho%of_r, rho%of_g, rho_core, rhog_core, ec_sr, vc_sr )
  !
  !
  CALL mp_sum( ec_sr, intra_pool_comm )
  !
  CALL stop_clock( 'pw_rpa_plus_pot' )
  !
  RETURN
  !
END SUBROUTINE pw_rpa_plus_pot 
!
!-----------------------------------------------------------------------
subroutine pw_rpa_vc (rs, ec, vc)
  !-----------------------------------------------------------------------
  !     RPA level of Ec in J.P. Perdew and Y. Wang, PRB 45, 13244 (1992)
  !     used for the so-called RPA+ scheme, see Perdew et. al, PRB, 59, 10461 (1999)
  !
  USE kinds
  implicit none
  real(DP) :: rs, ec, vc
  !
  real(DP) :: a, b1, b2, c0, c1, c2, c3, d0, d1
  parameter (a = 0.031091d0, b1 = 5.1486d0, b2 = 1.6483d0, c0 = a, &
       c1 = 0.070823d0, c2 = 0.00256d0, c3 = 0.00936d0, d0 = 0.4001d0, &
       d1 = 0.4590d0)
  real(DP) :: lnrs, rs12, rs32, rs74, om, dom, olog
  real(DP) :: a1 , b3 , b4
  data a1 / 0.082477d0 /, b3 / 0.23647d0 /, b4 / 0.20614d0 /
  !
  ! interpolation formula
  rs12 = sqrt (rs)
  rs32 = rs * rs12
  rs74 = rs * rs12 * sqrt(rs12)
  om  = 2.d0 * a * (b1 * rs12 + b2 * rs + b3 * rs32 + b4 * rs74)
  dom = 2.d0 * a * (0.5d0 * b1 * rs12 + b2 * rs + 1.5d0 * b3 * & 
            rs32 + 1.75d0 * b4 *  rs74)
  olog = log (1.d0 + 1.0d0 / om)
  ec = - 2.d0 * a * (1.d0 + a1 * rs) * olog
  vc = - 2.d0 * a * (1.d0 + 2.d0 / 3.d0 * a1 * rs) &
          * olog - 2.d0 / 3.d0 * a * (1.d0 + a1 * rs) * dom / &
          (om * (om + 1.d0) )
  !
  return
end subroutine pw_rpa_vc
