!
! Copyright (C) 2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE setup_q(auxdyn, do_band, do_iq, setup_pw, iq)
  !-----------------------------------------------------------------------
  !
  !  This routine is modified from PH/prepare_q.f90
  !
  !  This routine prepares a few variables that are needed to control
  !  the phonon run after the q point has been decided, but before
  !  doing the band calculation. In particular if ldisp=true it sets:
  !  xq : the q point for the phonon calculation
  !  fildyn : the name of the dynamical matrix
  !  lgamma : if this is a gamma point calculation
  !  epsil and zue : if epsil and zue need to be calculated
  !  In all cases it sets:
  !  current_iq : the current q point
  !  do_iq : if .true. q point has to be calculated
  !  setup_pw : if .true. the pw_setup has to be run
  !  do_band : if .true. the bands need to be calculated before phonon
  !
  USE control_flags,   ONLY : modenum
  USE io_global,       ONLY : stdout
  USE qpoint,          ONLY : xq
  USE disp,            ONLY : x_q, done_iq, comp_iq
  USE grid_irr_iq,     ONLY : irr_iq, done_irr_iq
  USE control_lr,      ONLY : lgamma
  USE control_ph,      ONLY : ldisp, start_irr, last_irr, current_iq, &
                              done_bands, tmp_dir_ph, tmp_dir_phq, lqdir
  USE io_files,        ONLY : prefix
  USE output,          ONLY : fildyn
  USE ph_restart,      ONLY : ph_writefile
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT(IN) :: iq
  LOGICAL, INTENT(OUT) :: do_band, do_iq, setup_pw
  CHARACTER (LEN=256), INTENT(IN) :: auxdyn
  CHARACTER (LEN=6), EXTERNAL :: int_to_char
  INTEGER :: irr, ierr
  !
  do_iq=.TRUE.
  !
  ! Case 1) This q point is not calculated because not requested in this run
  !
  IF ( .NOT. comp_iq(iq) ) THEN
     do_iq=.FALSE.
     RETURN
  ENDIF
  !
  !  Case 2) This q point is not calculated because it has too few
  !          representation and the starting representation is larger
  !          than the number of available representations
  !
  IF (start_irr>irr_iq(iq)) THEN
     WRITE(6,'(5x,"Exiting... start_irr,",i4,&
            & " > number of representations,",i4   )') &
            start_irr, irr_iq(iq)
     do_iq=.FALSE.
     RETURN
  ENDIF
  !
  current_iq = iq
  !
  tmp_dir_phq=tmp_dir_ph
  !
  IF ( ldisp ) THEN
     !
     ! ... set the name for the output file
     !
     fildyn = TRIM( auxdyn ) // TRIM( int_to_char( iq ) )
     !
     ! ... set the q point
     !
     xq(1:3)  = x_q(1:3,iq)
     !
     !  Check if it is lgamma
     !
     lgamma = ( xq(1) == 0.D0 .AND. xq(2) == 0.D0 .AND. xq(3) == 0.D0 )
     !
     ! ... each q /= gamma is saved on a different directory
     !
     IF (.NOT.lgamma.AND.lqdir) &
         tmp_dir_phq= TRIM (tmp_dir_ph) // TRIM(prefix) // '_q' &
                       & // TRIM(int_to_char(iq))//'/'
     !
     ! NB: setting proper value for calculation of the dielectric 
     !     constant and the Born eff. charges as in PH/prepare_q.f90
     !     has been removed (acfdt.x does not need them)
     !     Calculation of the head of the dielectric matrix maybe specified here
     !
  ENDIF
  !
  !  Save the current status of the run: all the flags, the list of q,
  !  and the current q, the fact that we are before the bands
  !
  CALL ph_writefile('init',0,0,ierr)
  !
  ! ... In the case of q != 0, we make first a non selfconsistent run
  !
  setup_pw = (.NOT.lgamma.OR.modenum /= 0).AND..NOT. done_bands

  do_band=.FALSE.
  DO irr=start_irr, MIN(ABS(last_irr),irr_iq(iq))
     IF (.NOT. done_irr_iq(irr,iq) ) THEN
        do_band=.TRUE.
        EXIT
     ENDIF
  ENDDO
!
!  There are two special cases. When start_irr=0 and last_irr=0 we generate only
!  the displacement patterns, and do not calculate the bands. If this q
!  has been already calculated we only diagonalize the dynamical matrix
!
  IF ( start_irr == 0 .AND. last_irr == 0 ) do_band=.FALSE.

  IF ( done_iq(iq) ) do_band=.FALSE.

  WRITE( stdout, '(/,5X,"Calculation of q = ",3F12.7)') xq

  RETURN
  !
END SUBROUTINE setup_q
