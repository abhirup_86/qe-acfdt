!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!#define DEBUG
!
!-----------------------------------------------------------------------
subroutine solve_dec_dv (uu)
  !-----------------------------------------------------------------------
  !
  !    Driver routine for the solution of the fucntional derivative of 
  !    response function which defines the change of the first order variation 
  !    wavefunction due to an external perturbation. It expanses according to
  !    3rd order energy of pertubation theory. 
  !
  !    It performs the following tasks:
  !     a) computes <\Delta psi(+) | \Delta psi(-) > 
  !     b) computes <\Delta psi(+) | sum \Delta psi(-) ><psi|psi>
  !     c) calls bicgstabsolve_all to solve the linear system
  !     d) computes 3 contributions
  !
  USE kinds,                 ONLY : DP
  USE io_global,             ONLY : stdout, ionode
  USE ions_base,             ONLY : nat
  USE io_files,              ONLY : diropn
  USE wavefunctions_module,  ONLY : evc, psic
!  USE cell_base,             ONLY : tpiba2
  USE klist,                 ONLY : lgauss, wk, xk, ngk, igk_k
  USE lsda_mod,              ONLY : lsda, nspin, current_spin, isk
  USE fft_base,              ONLY : dffts, dfftp
  USE fft_interfaces,        ONLY : fwfft, invfft
  USE gvect,                 ONLY : gstart, nl
  USE gvecs,                 ONLY : doublegrid, nls
  USE wvfct,                 ONLY : npwx, nbnd, et, current_k
  USE control_lr,            ONLY : nbnd_occ, lgamma
  USE control_ph,            ONLY : tr2_ph, reduce_io, flmixdpot, convt
  USE units_ph,              ONLY : iudrho, lrdrho, iudwf, lrdwf, iuwfc, lrwfc
  USE output,                ONLY : fildrho
  USE eqv,                   ONLY : dvpsi, evq, dpsi
  USE qpoint,                ONLY : nksq, ikks, ikqs
  USE uspp,                  ONLY : vkb, okvan
  USE uspp_param,            ONLY : nhm
  USE mp,                    ONLY : mp_sum
  USE mp_global,             ONLY : inter_pool_comm 
  USE control_acfdt,         ONLY : dvdpsi, dvgenc, summ_vc, summ_vc3, summ_vc12, dpsi_plus, dpsi_minus
  USE acfdtest,              ONLY : f1,f2,f3, acfdt_is_active, acfdt_term1, acfdt_term2, acfdt_term3
  USE noncollin_module,      ONLY : noncolin, npol, nspin_mag
  USE paw_variables,         ONLY : okpaw
  USE buffers,               ONLY : get_buffer
  !
  implicit none
  !
  integer, parameter :: npe = 1
  !
  real(DP), intent(in) :: uu
  ! input: the imaginary frequency
  !
  real(DP) , allocatable :: h_diag (:,:)
  ! h_diag: diagonal part of the Hamiltonian
  !
  COMPLEX (DP), ALLOCATABLE ::  dpsi_sum(:,:)
  ! variation wfc. at iu + (dpsi_plus)/- dpsi_minus
  ! dpsi_sum : summation of the 2 above variation wfcs
  COMPLEX (DP), ALLOCATABLE :: summ1(:,:), summ2(:,:), summ3(:,:) 
  real(DP) :: thresh, anorm 
  ! thresh: convergence threshold
  ! anorm : the norm of the error
  real(DP) :: dos_ef, wg1, w0g, wgp, weight, deltae, theta
  complex(DP) :: wwg
  !
  complex(DP), allocatable, target :: dvscfin(:,:,:)
  ! change of the scf potential 
  complex(DP), pointer :: dvscfins (:,:,:)
  ! change of the scf potential (smooth part only)
  COMPLEX(DP), allocatable :: summ0 (:,:,:), summh  (:,:,:)  
  COMPLEX(DP), allocatable :: summ03 (:,:,:), summh3  (:,:,:)  
  COMPLEX(DP), allocatable :: summ012 (:,:,:), summh12  (:,:,:)  
  !
  complex(DP), allocatable :: aux1 (:), aux2(:)
  !
  complex(DP), allocatable :: ldos (:,:), ldoss (:,:)
  !
  !complex(DP) :: ZDOTC
  ! the scalar product function
  REAL(DP), allocatable :: becsum1(:,:,:)

  logical :: conv_root,  & ! true if linear system is converged
             iuzero,     & ! true if freq uu==0
             exst,       & ! used to open the recover file
             lmetq0        ! true if xq=(0,0,0) in a metal

  integer :: kter,       & ! counter on iterations
             iter0,      & ! starting iteration
             ibnd, jbnd, & ! counter on bands
             iter,       & ! counter on iterations
             lter,       & ! counter on iterations of linear system
             lintercall, & ! average number of calls to cgsolve_all
             ik, ikk,    & ! counter on k points
             ikq,        & ! counter on k+q points
             ig,         & ! counter on G vectors
             ir,         & ! counter on mesh points
             is,         & ! counter on spin polarizations
             nrec, nrec1,& ! the record number for dvpsi and dpsi
             ios           ! integer variable for I/O control
  !          mode          ! mode index

  integer :: irr
  integer :: npw, npwq

  real(DP) :: tcpu, get_clock ! timing variables
  !
  external ch_psi_all,  cg_psi
  !
  call start_clock ('solve_dec_dv')
  !
  iuzero = abs(uu).lt.1.d-6
  !
  allocate (dvscfin ( dfftp%nnr , nspin , npe))    
  if (doublegrid) then
     allocate (dvscfins (dffts%nnr , nspin , npe))    
  else
     dvscfins => dvscfin
  endif
  allocate ( dpsi_sum   ( npwx*npol , nbnd))
  allocate ( dvdpsi     ( npwx*npol , nbnd))
  allocate (aux1 ( dffts%nnr))    
  allocate (aux2 ( dffts%nnr))
  allocate (h_diag ( npwx*npol, nbnd))    
  allocate (summ1 (dfftp%nnr  ,nspin))
  allocate (summ2 (dfftp%nnr  ,nspin))
  allocate (summ3 (dfftp%nnr  ,nspin))
  allocate (summ0 (dfftp%nnr  ,nspin,npe))
  allocate (summh (dfftp%nnr  ,nspin,npe))
  allocate (summ03 (dfftp%nnr  ,nspin,npe))
  allocate (summh3 (dfftp%nnr  ,nspin,npe))
  allocate (summ012 (dfftp%nnr  ,nspin,npe))
  allocate (summh12 (dfftp%nnr  ,nspin,npe))
  !
  summ0 (:,:,:) = (0.d0, 0.d0)
  summ03 (:,:,:) = (0.d0, 0.d0)
  summ012 (:,:,:) = (0.d0, 0.d0)
  summh (:,:,:) = (0.d0, 0.d0)
  summh3 (:,:,:) = (0.d0, 0.d0)
  summh12 (:,:,:) = (0.d0, 0.d0)
  !
  ! if q=0 for a metal: allocate and compute local DOS at Ef
  !
  lmetq0 = lgauss.and.lgamma
  if (lmetq0) then
     allocate ( ldos ( dfftp%nnr , nspin) )    
     allocate ( ldoss( dffts%nnr , nspin) )    
     allocate (becsum1 ( (nhm * (nhm + 1))/2 , nat , nspin_mag))
     call localdos_paw ( ldos , ldoss , becsum1, dos_ef )
     IF (.NOT.okpaw) deallocate(becsum1)
  endif
  !
  if (reduce_io) then
     flmixdpot = ' '
  else
     flmixdpot = 'mixd'
  endif
  !
  IF (ionode .AND. fildrho /= ' ') THEN
     INQUIRE (UNIT = iudrho, OPENED = exst)
     IF (exst) CLOSE (UNIT = iudrho, STATUS='keep')
     CALL DIROPN (iudrho, TRIM(fildrho)//'.u', lrdrho, exst)
  END IF
     !
!     if (nksq.gt.1) rewind (unit = iunigk)
     !
     do ik = 1, nksq
        !
!        if (nksq.gt.1) then
!           read (iunigk, err = 100, iostat = ios) npw, igk
!100        call errore ('solve_dviu', 'reading igk', abs (ios) )
!        endif
!        if (lgamma) npwq = npw
        ! 
        ikk = ikks(ik)
        ikq = ikqs(ik)
        !NsC >
        npw = ngk(ikk)
        npwq= ngk(ikq) 
        ! <
        current_k = ikq
        !
        if (lsda) current_spin = isk (ikk)
!        if (.not.lgamma.and.nksq.gt.1) then
!           read (iunigk, err = 200, iostat = ios) npwq, igkq
!200        call errore ('solve_dviu', 'reading igkq', abs (ios) )
!        endif
        !
        ! reads unperturbed wavefuctions psi(k) and psi(k+q)
        ! reads  pertubated wavefucntion dpsi (k) for +/- iu cases
        !
        if (nksq.gt.1) then
           if (lgamma) then
!              call davcio (evc, lrwfc, iuwfc, ikk, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikk)  ! NsC from version 283
           else
!              call davcio (evc, lrwfc, iuwfc, ikk, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikk)  ! NsC from version 283
!              call davcio (evq, lrwfc, iuwfc, ikq, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikq)  ! NsC from version 283
           endif
        endif
        !
        ! if number nksq > 1 , it will read from file, otherwise, it's stored
        ! in memories via dpsi_plus, dpsi_minus variables 
        if (nksq.gt.1) then
           call davcio (dpsi_plus, lrdwf, iudwf, ik,    -1 )
           nrec = nksq +ik
           call davcio (dpsi_minus, lrdwf, iudwf, nrec, -1 )
        endif
        !
        ! compute beta functions and kinetic energy for k-point ikq
        ! needed by h_psi, called by ch_psi_all, called by cgsolve_all
        !
        CALL init_us_2 (npwq, igk_k(1,ikq), xk (1, ikq), vkb)
        CALL g2_kin (ikq) 
        !
        ! compute preconditioning matrix h_diag used by cgsolve_all
        !
        CALL h_prec (ik, evq, h_diag)
        !
!
!        call init_us_2 (npwq, igkq, xk (1, ikq), vkb)
!        ! compute the kinetic energy
!        !
!        do ig = 1, npwq
!           g2kin (ig) = ( (xk (1,ikq) + g (1, igkq(ig)) ) **2 + &
!                          (xk (2,ikq) + g (2, igkq(ig)) ) **2 + &
!                          (xk (3,ikq) + g (3, igkq(ig)) ) **2 ) * tpiba2
!        enddo
!        ! 
!        h_diag=0.d0
!        do ibnd = 1, nbnd_occ (ikk)
!           do ig = 1, npwq
!              h_diag(ig,ibnd)=1.d0/max(1.0d0,g2kin(ig)/eprec(ibnd,ik))
!           enddo
!           IF (noncolin) THEN
!              do ig = 1, npwq
!                 h_diag(ig+npwx,ibnd)=1.d0/max(1.0d0,g2kin(ig)/eprec(ibnd,ik))
!              enddo
!           END IF
!        enddo
        !
        summ1 (:,:) = (0.d0, 0.d0)
        summ2 (:,:) = (0.d0, 0.d0)
        summ3 (:,:) = (0.d0, 0.d0)
        !
        weight = wk(ikk)
        ! Calculate <dspi+|dv|dpsi->:  Second in term Eq. (23) PRB.90.045138
        CALL dpsi_dpsi  (summ1(1, current_spin), weight, ik, dpsi_plus, dpsi_minus, 1)
 
        !
        ! Calculate <dpsi+|dpsi-><psi|dv|psi>: Third term in Eq. (23) PRB.90.045138 
        CALL incd_dpsi2 (summ2(1, current_spin), weight, ik, dpsi_plus, dpsi_minus) 
        !
        ! ... Calculate \tilde{psi}2 form Eq. 22 PRB.90.045138
        !
        ! Calculate complex conjugate of DV. At gamma point DV = real.
        ! dvgenc contains the RPA eigenpotential
        !
        if (lgamma) then
           dvscfin(:,current_spin,1) = CMPLX(DBLE(dvgenc(:,current_spin,1)), 0.d0, kind=DP)
        else
           dvscfin(:,current_spin,1) = CONJG(dvgenc(:,current_spin,1))  ! See Eq. (22) PRB.90.045138 . We forgot the CONGJ there ....
        endif
        !
        if (iuzero) then        
        dvscfin(:,current_spin,1) = dvgenc(:,current_spin,1)
        end if
        !
        if (doublegrid) &
           call cinterpolate (dvscfin(1,current_spin,1),dvscfins(1,current_spin,1),-1)
        !  
        ! |dvpsi> = (DV)*|psi>
        !
        dvpsi(:,:) = (0.d0, 0.d0)
        !
        do ibnd = 1, nbnd_occ (ik)
           aux1(:) = (0.d0, 0.d0)
           do ig = 1, npw
!              aux1 (nls(igk(ig))) = evc(ig,ibnd)
              aux1 (nls(igk_k(ig,ikk))) = evc(ig,ibnd)
           enddo
           CALL invfft ('Wave', aux1, dffts)
           do ir = 1, dffts%nnr
               aux1(ir) = aux1(ir) * dvscfins(ir,current_spin,1)
           enddo
           !
           CALL fwfft ('Wave', aux1, dffts)
           do ig = 1, npwq
!              dvpsi(ig,ibnd) = dvpsi(ig,ibnd) + aux1(nls(igkq(ig)))
              dvpsi(ig,ibnd) = dvpsi(ig,ibnd) + aux1(nls(igk_k(ig,ikq)))
           enddo
        enddo
        !
        !|dvdpsi> = (DV)*|dpsi>
        !
        dvdpsi  (:,:) = (0.d0, 0.d0)
        dpsi_sum(:,:) = (0.d0, 0.d0) 
        do ibnd = 1, nbnd_occ (ik)
           aux2(:) = (0.d0, 0.d0)
           do ig = 1, npwq
              dpsi_sum(ig,ibnd)    = dpsi_plus (ig,ibnd) + dpsi_minus(ig,ibnd) 
!              aux2 (nls(igkq(ig))) = dpsi_sum(ig,ibnd)
              aux2 (nls(igk_k(ig,ikq))) = dpsi_sum(ig,ibnd)
           enddo
           CALL invfft ('Wave', aux2, dffts)
           do ir = 1, dffts%nnr
               aux2(ir) = aux2(ir) * dvscfins(ir,current_spin,1)
           enddo
           !
           CALL fwfft ('Wave', aux2, dffts)
           do ig = 1, npw
!              dvdpsi(ig,ibnd)   = dvdpsi(ig,ibnd) + aux2(nls(igk(ig)))
              dvdpsi(ig,ibnd)   = dvdpsi(ig,ibnd) + aux2(nls(igk_k(ig,ikk)))
           enddo
        enddo
        ! 
        if (okvan) then
           call errore('solve_dviu', 'USPP not implemented yet', 1)
        endif
        !
        ! Calculate rhs of psi_2 eq.
        !
        CALL  psi_2_bare(dvdpsi, evc, ikk, ikq, dvpsi, dpsi_sum)
        !
        ! Apply -P_c^+. to |dvdpsi>
        ! |dvdpsi> =  -(|dvdpsi> - S|evq><evq|dvdpsi>)
        !
        CALL orthogonalize(dvdpsi, evq, ikk, ikq, dpsi, npwq, .false.)
        !
        ! iterative solution of the linear system (H-eS)*dpsi=dvpsi,
        ! dvpsi=-P_c^+ (dvbare+dvscf)*psi , dvscf fixed.
        !
        conv_root = .true.
        thresh = tr2_ph
        !
        CALL cgsolve_all (ch_psi_all, cg_psi, et(1,ikk), dvdpsi, dpsi, &
                             h_diag, npwx, npwq, thresh, ik, lter, conv_root, &
                             anorm, nbnd_occ(ikk), npol )
        !
        if (.not.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
        !
        ! Calculate <psi_v|dv|dpsi2_v> 
        weight = wk(ikk)
        CALL dpsi_dpsi  (summ3(1, current_spin), weight, ik, evc, dpsi, 2) 
        !
        ! Calculate all k points for derivative response fucntion (d_chi0/dv)
        !
!
!  ACFDT comment the line we dont want to see (there are 
!       
        !
        IF (acfdt_is_active.and.acfdt_term1) then
          summ0(1:dfftp%nnr, current_spin,1) = summ0(1:dfftp%nnr,current_spin,1) + &
                             f1*CMPLX(DBLE(summ1(1:dfftp%nnr, current_spin)),0.D0, kind=DP)
        !
        ELSEIF (acfdt_is_active.and.acfdt_term2) then
          summ0(1:dfftp%nnr, current_spin,1) = summ0(1:dfftp%nnr,current_spin,1) - &
                               f2*CMPLX(DBLE(summ2(1:dfftp%nnr, current_spin)),0.D0, kind=DP)
        !
        ELSEIF (acfdt_is_active.and.acfdt_term3) then
          summ0(1:dfftp%nnr, current_spin,1) = summ0(1:dfftp%nnr,current_spin,1) + &
                               f3*CMPLX(DBLE(summ3(1:dfftp%nnr, current_spin)),0.D0, kind=DP)
        ! 
        ELSE
         summ0(1:dfftp%nnr, current_spin,1) = summ0(1:dfftp%nnr,current_spin,1) +  &
               ( &
                   + f1*CMPLX(DBLE(summ1(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &
                   - f2*CMPLX(DBLE(summ2(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &
                   + f3*CMPLX(DBLE(summ3(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &  
               )
         ! 
         summ03(1:dfftp%nnr, current_spin,1) = summ03(1:dfftp%nnr,current_spin,1) +  &
               (     CMPLX(DBLE(summ3(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  )
         !
         summ012(1:dfftp%nnr, current_spin,1) = summ012(1:dfftp%nnr,current_spin,1) +  &
               ( &
                     CMPLX(DBLE(summ1(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &
                   - CMPLX(DBLE(summ2(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  )
 
        ENDIF
        !
     enddo ! on k-points
     !
     if (doublegrid) then
        do is = 1, nspin
           call cinterpolate (summh(1,is,1), summ0(1,is,1), 1)
           call cinterpolate (summh3(1,is,1), summ03(1,is,1), 1)
           call cinterpolate (summh12(1,is,1), summ012(1,is,1), 1)
        enddo
     else
        call ZCOPY (npe*nspin*dfftp%nnr, summ0, 1, summh, 1)
        call ZCOPY (npe*nspin*dfftp%nnr, summ03, 1, summh3, 1)
        call ZCOPY (npe*nspin*dfftp%nnr, summ012, 1, summh12, 1)
     endif
     ! 
     ! if q=0, make sure that charge conservation is guaranteed
     ! 
     if ( lgamma ) then
        psic(:) = summh(:, nspin, npe)
        CALL fwfft ('Dense', psic, dfftp)
        if ( gstart==2) psic(nl(1)) = (0.d0, 0.d0)
        CALL invfft ('Dense', psic, dfftp)
        summh(:, nspin, npe) = psic(:)
     endif
     !
#if defined __MPI
     !
     !   Reduce the summ0 and summh across pools
     !
     call mp_sum ( summ0, inter_pool_comm )
     call mp_sum ( summ03, inter_pool_comm )
     call mp_sum ( summ012, inter_pool_comm )
     call mp_sum ( summh, inter_pool_comm )
     call mp_sum ( summh3, inter_pool_comm )
     call mp_sum ( summh12, inter_pool_comm )
     !
#endif
     ! 
     ! here we pass summ to summ_vc to used as output
     !
     convt = .true.
     if ( convt ) then
        summ_vc(:,:) = summh(:,:,1)
        summ_vc3(:,:) = summh3(:,:,1)
        summ_vc12(:,:) = summh12(:,:,1)
        goto 300
     endif
     !
  300 CONTINUE
  deallocate ( h_diag)
  deallocate ( dpsi_sum)
  deallocate ( dvdpsi )
  deallocate ( aux1)
  deallocate ( aux2)
  if (doublegrid) deallocate (dvscfins)
  deallocate (dvscfin)
  if (lmetq0) deallocate (ldoss)
  if (lmetq0) deallocate (ldos)
  deallocate (summ1)
  deallocate (summ2)
  deallocate (summ3)
  deallocate (summ0)
  deallocate (summ03)
  deallocate (summ012)
  deallocate (summh)
  deallocate (summh3)
  deallocate (summh12)
  call stop_clock ('solve_dec_dv')
  return
end subroutine solve_dec_dv
