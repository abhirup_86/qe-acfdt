!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!#define DEBUG
!
!-----------------------------------------------------------------------
SUBROUTINE solve_dec_dv_rpax (uu)
  !-----------------------------------------------------------------------
  !
  !    Driver routine for the solution of the fucntional derivative of 
  !    response function which defines the change of the first order variation 
  !    wavefunction due to an external perturbation. It expanses according to
  !    3rd order energy of pertubation theory. 
  !
  !    It performs the following tasks:
  !     a) computes <\Delta psi(+) | \Delta psi(-) > 
  !     b) computes <\Delta psi(+) | sum \Delta psi(-) ><psi|psi>
  !     c) calls bicgstabsolve_all to solve the linear system
  !     d) computes 3 contributions
  !
  USE kinds,                 ONLY : DP
  USE io_global,             ONLY : stdout, ionode
  USE io_files,              ONLY : prefix, diropn
  USE ions_base,             ONLY : nat
  USE wavefunctions_module,  ONLY : evc, psic
  USE klist,                 ONLY : lgauss, nkstot, wk, xk, ngk, igk_k
  USE lsda_mod,              ONLY : lsda, nspin, current_spin, isk
  USE fft_base,              ONLY : dffts, dfftp
  USE fft_interfaces,        ONLY : fwfft, invfft
  USE gvect,                 ONLY : gstart, nl
  USE gvecs,                 ONLY : doublegrid
  USE wvfct,                 ONLY : npwx, nbnd, et, current_k
  USE control_lr,            ONLY : nbnd_occ, lgamma
  USE control_ph,            ONLY : reduce_io, convt, tr2_ph, flmixdpot
  USE units_ph,              ONLY : iudrho, lrdrho, iudwf, lrdwf, iuwfc, lrwfc
  USE output,                ONLY : fildrho
  USE eqv,                   ONLY : dpsi, evq
  USE qpoint,                ONLY : nksq, ikks, ikqs
  USE uspp,                  ONLY : vkb
  USE uspp_param,            ONLY : nhm
  USE mp,                    ONLY : mp_sum
  USE mp_global,             ONLY : inter_pool_comm 
  USE control_acfdt,         ONLY : dvgenc, summ_vc, summ_vc3, summ_vc12, &
                                    dpsi_minus, dpsi_plus, dpsi_nl,dpsiq_nl
  USE noncollin_module,      ONLY : noncolin, npol, nspin_mag
  USE paw_variables,         ONLY : okpaw
  USE buffers,               ONLY : get_buffer
  !
  IMPLICIT NONE
  !
  INTEGER, PARAMETER :: npe = 1
  !
  REAL(DP), INTENT(IN) :: uu
  ! input: the imaginary frequency
  !
  COMPLEX(DP), ALLOCATABLE :: h_diag (:,:,:), etiu (:,:,:)
  ! h_diag: diagonal part of the Hamiltonian +/- iu
  ! etiu  : eigenvalues +/- iu 
  !
  COMPLEX(DP), ALLOCATABLE :: dpsi2(:,:) 
  ! working array for dvpsi and dpsi for (+) and (-) iu
  !
  COMPLEX (DP), ALLOCATABLE ::  dpsi_sum(:,:), dv2(:,:,:)
  ! variation wfc. at iu + (dpsi_plus)/- dpsi_minus
  ! dpsi_sum : summation of the 2 above variation wfcs 
  !
  COMPLEX (DP), ALLOCATABLE :: summ1(:,:), summ2(:,:), summ3(:,:), summ4(:,:)
  ! store the summ over band
  !
  REAL(DP) :: thresh, anorm 
  ! thresh: convergence threshold
  ! anorm : the norm of the error 
  !
  REAL(DP) :: dos_ef, wg1, w0g, wgp, weight, deltae, theta
  COMPLEX(DP) :: wwg
  !
  COMPLEX(DP), ALLOCATABLE, TARGET :: dvscfin(:,:,:)
  ! change of the scf potential 
  !
  COMPLEX(DP), POINTER :: dvscfins (:,:,:)
  ! change of the scf potential (smooth part only)
  !
  COMPLEX(DP), ALLOCATABLE :: summ0 (:,:,:), summh  (:,:,:)  
  COMPLEX(DP), ALLOCATABLE :: summ03 (:,:,:), summh3  (:,:,:)  
  COMPLEX(DP), ALLOCATABLE :: summ012 (:,:,:), summh12  (:,:,:)  
  !
  COMPLEX(DP), ALLOCATABLE :: aux1 (:), aux2(:)
  !
  COMPLEX(DP), ALLOCATABLE :: ldos (:,:), ldoss (:,:)

  LOGICAL :: conv_root,  & ! true if linear system is converged
             iuzero,     & ! true if freq uu==0
             exst,       & ! used to open the recover file
             lmetq0        ! true if xq=(0,0,0) in a metal

  INTEGER :: kter,       & ! counter on iterations
             iter0,      & ! starting iteration
             ibnd, jbnd, & ! counter on bands
             iter,       & ! counter on iterations
             lter,       & ! counter on iterations of linear system
             lintercall, & ! average number of calls to cgsolve_all
             ik, ikk,    & ! counter on k points
             ikq,        & ! counter on k+q points
             ig,         & ! counter on G vectors
             ir,         & ! counter on mesh points
             is,         & ! counter on spin polarizations
             nrec, nrec1,& ! the record number for dvpsi and dpsi
             ios           ! integer variable for I/O control
  !
  INTEGER :: irr
  INTEGER :: npw, npwq
  !
  REAL(DP) :: tcpu, get_clock ! timing variables
  !
  EXTERNAL ch_psi_all,  cg_psi
  EXTERNAL cch_psi_all, ccg_psi
  REAL(DP), allocatable :: becsum1(:,:,:)
  !
  CALL start_clock ('solve_dec_dv')
  !
  iuzero = abs(uu).lt.1.d-6
  ! 
  ALLOCATE ( h_diag(npwx, nbnd, 2) )
  ALLOCATE ( dpsi2(npwx,nbnd) )
  ALLOCATE ( etiu(nbnd, nkstot, 2) )
  ALLOCATE ( dv2(npwx,nbnd,2) )
  etiu(:,:,1) = CMPLX( et(:,:), uu, kind=DP )
  etiu(:,:,2) = CMPLX( et(:,:),-uu, kind=DP )
  !
  ALLOCATE (dvscfin ( dfftp%nnr , nspin , npe))    
  IF (doublegrid) THEN
     ALLOCATE (dvscfins (dffts%nnr , nspin , npe))    
  ELSE
     dvscfins => dvscfin
  ENDIF
  !
  ALLOCATE ( dpsi_sum   ( npwx , nbnd))
  ALLOCATE (aux1 ( dffts%nnr))    
  ALLOCATE (aux2 ( dffts%nnr))
  ALLOCATE (summ1 (dfftp%nnr  ,nspin))
  ALLOCATE (summ2 (dfftp%nnr  ,nspin))
  ALLOCATE (summ3 (dfftp%nnr  ,nspin))
  ALLOCATE (summ4 (dfftp%nnr  ,nspin))
  ALLOCATE (summ0 (dfftp%nnr  ,nspin,npe))
  ALLOCATE (summh (dfftp%nnr  ,nspin,npe))
  ALLOCATE (summ03 (dfftp%nnr  ,nspin,npe))
  ALLOCATE (summh3 (dfftp%nnr  ,nspin,npe))
  ALLOCATE (summ012 (dfftp%nnr  ,nspin,npe))
  ALLOCATE (summh12 (dfftp%nnr  ,nspin,npe))
  !
  summ0 (:,:,:) = (0.d0, 0.d0)
  summ03 (:,:,:) = (0.d0, 0.d0)
  summ012 (:,:,:) = (0.d0, 0.d0)
  summh (:,:,:) = (0.d0, 0.d0)
  summh3 (:,:,:) = (0.d0, 0.d0)
  summh12 (:,:,:) = (0.d0, 0.d0)
  !
  ! if q=0 for a metal: allocate and compute local DOS at Ef
  !
  lmetq0 = lgauss.and.lgamma
  IF (lmetq0) THEN
     allocate ( ldos ( dfftp%nnr , nspin) )    
     allocate ( ldoss( dffts%nnr , nspin) )    
     allocate (becsum1 ( (nhm * (nhm + 1))/2 , nat , nspin_mag))
     call localdos_paw ( ldos , ldoss , becsum1, dos_ef )
     IF (.NOT.okpaw) deallocate(becsum1)
  ENDIF
  !
  IF (reduce_io) THEN
     flmixdpot = ' '
  ELSE
     flmixdpot = 'mixd'
  ENDIF
  !
  IF (ionode .AND. fildrho /= ' ') THEN
     INQUIRE (UNIT = iudrho, OPENED = exst)
     IF (exst) CLOSE (UNIT = iudrho, STATUS='keep')
     CALL DIROPN (iudrho, TRIM(fildrho)//'.u', lrdrho, exst)
  END IF
     !
!     IF (nksq .GT. 1) REWIND (UNIT = iunigk)
     !
     DO ik = 1, nksq
        !
!        IF (nksq .GT. 1) THEN
!           READ (iunigk, err = 100, IOSTAT = ios) npw, igk
!100        CALL errore ('solve_dviu', 'reading igk', ABS (ios) )
!        ENDIF
!        IF (lgamma) npwq = npw
        ! 
        ikk = ikks(ik)
        ikq = ikqs(ik)
        ! NsC > 
        npw = ngk(ikk)
        npwq= ngk(ikq)
        !
        current_k = ikq
        !
        IF (lsda) current_spin = isk (ikk)
!        IF ( .NOT. lgamma .AND. nksq .GT. 1) THEN
!           READ (iunigk, err = 200, IOSTAT = ios) npwq, igkq
!200        CALL errore ('solve_dviu', 'reading igkq', ABS (ios) )
!        ENDIF
!        CALL init_us_2 (npwq, igkq, xk (1, ikq), vkb)
        
        CALL init_us_2 (npwq, igk_k(1,ikq), xk (1, ikq), vkb)
        !
        ! reads unperturbed wavefuctions psi(k) and psi(k+q)
        ! reads  pertubated wavefucntion dpsi (k) for +/- iu cases
        !
        IF (nksq .GT. 1) THEN
           IF (lgamma) THEN
!              CALL davcio (evc, lrwfc, iuwfc, ikk, - 1)
              CALL get_buffer (evc, lrwfc, iuwfc, ikk) ! NsC from version 283
           ELSE
!              CALL davcio (evc, lrwfc, iuwfc, ikk, - 1)
              CALL get_buffer (evc, lrwfc, iuwfc, ikk) ! NsC from version 283
!              CALL davcio (evq, lrwfc, iuwfc, ikq, - 1)
              CALL get_buffer (evc, lrwfc, iuwfc, ikq) ! NsC from version 283
           ENDIF
        ENDIF
        !
        ! if number nksq > 1 , it will read from file, otherwise, it's stored
        ! in memories via dpsi_plus, dpsi_minus variables 
        !
        IF (nksq .GT. 1) THEN
           CALL davcio (dpsi_plus, lrdwf, iudwf, ik,    -1 )
           nrec = nksq +ik
           CALL davcio (dpsi_minus, lrdwf, iudwf, nrec, -1 )
           !
           ! Read dpsi_nl and if q/=0 dpsiq_nl as well
           !
           nrec = nrec+nksq
           CALL davcio (dpsi_nl, lrdwf, iudwf, nrec, -1 )
           IF ( .NOT. lgamma ) THEN
              nrec = nrec + nksq
              CALL davcio ( dpsiq_nl, lrdwf, iudwf, nrec, -1 )
           ENDIF       
        ENDIF
        !
        summ1 (:,:) = (0.d0, 0.d0)
        summ2 (:,:) = (0.d0, 0.d0)
        summ3 (:,:) = (0.d0, 0.d0)
        summ4 (:,:) = (0.d0, 0.d0)
        !
        dpsi_sum (:,:) = (0.D0, 0.D0)
        dpsi_sum (:,:) = dpsi_plus(:,:) + dpsi_minus(:,:)
        !
        ! Calculate summ1 = \ summ_v { dpsi_nl_v(r)^* x [dspi_v+(r)+dpsi_v-(r)] }
        !   and     summ2 = \summ_vv' <dpsiq_nl_v |[dpsi+_v' + dpsi-_v']> psi_v'(r) * psiq_v(r)
        ! NB. A factor 2 to the weight is added in dpsi_dpsi subroutine
        !
        weight = wk(ikk)/2.D0
        CALL dpsi_dpsi  (summ1(1, current_spin), weight, ik, dpsi_nl, dpsi_sum, 1)
!        write(stdout,*) 'summ1'
!        write(stdout,*) summ1(1:3,current_spin)
        !
        CALL incd_dpsi2_rpax (summ2(1, current_spin), weight, ik, dpsiq_nl, dpsi_sum) 
!        write(stdout,*) 'summ2'
!        write(stdout,*) summ2(1:3,current_spin)
        !
        !
        dvscfin(:,current_spin,1) = dvgenc(:,current_spin,1)
!!!        IF (lgamma ) dvscfin(:,current_spin,1) = CMPLX(DBLE(dvgenc(:,current_spin,1)), 0.d0, kind=DP)
        !
        IF (doublegrid) CALL cinterpolate (dvscfin(1,current_spin,1),dvscfins(1,current_spin,1),-1)
        !
        !!!=================================================!!!
        !!!       Solve Linear system for D2phi             !!!
        !!!=================================================!!!
        !
        CALL g2_kin (ikq)
        CALL ch_prec (ik, evq, h_diag, uu)
!        ! compute the kinetic energy at k+q
!        !
!        DO ig = 1, npwq
!           g2kin (ig) = ( (xk (1,ikq) + g (1, igkq(ig)) ) **2 + &
!                          (xk (2,ikq) + g (2, igkq(ig)) ) **2 + &
!                          (xk (3,ikq) + g (3, igkq(ig)) ) **2 ) * tpiba2
!        ENDDO
!        ! 
!        h_diag(:,:,:) = (0.d0, 0.d0)
!        DO ibnd = 1, nbnd_occ (ikk)
!           DO ig = 1, npwq
!               h_diag(ig,ibnd,1) = CMPLX(1.d0, 0.d0, kind=DP) / &
!                   CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik),-uu, kind=DP )
!               h_diag(ig,ibnd,2) = CMPLX(1.d0, 0.d0, kind=DP) / &
!                   CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik), uu, kind=DP )
!           ENDDO
!        ENDDO
        !
        ! compute rhs of linear system for d2phi
        ! input: dpsi_plus, dpsi_minus, psi, dpsi_nl, DV, flag
        ! output: dv2 = rhs of linear system
        !
        dv2(:,:,:) = (0.d0, 0.d0)
        CALL rhs_dchi_rpax (ikk, ikq, evc, evq, dpsi_plus, dpsi_minus, dpsi_nl, dpsiq_nl, dvscfins, dv2)
!        write(stdout,*) 'rhs'
!        write(stdout,*) dv2(1:3,1,1)
!         write(stdout,*) dv2(1:3,1,2)
        !
        ! orthogonalize to valence band (dpsi is used as workpsace in the subroutine)
        ! output in dv2
        !
        CALL orthogonalize_uu(dv2(1,1,1), evq, ikk, ikq, dpsi, npwq,uu)
        CALL orthogonalize_uu(dv2(1,1,2), evq, ikk, ikq, dpsi, npwq,-uu)
        !
        ! Imaginary Freq +iu
        !
        conv_root = .true.
        thresh = tr2_ph 
        dpsi(:,:) = (0.D0, 0.D0)
        !
        CALL bicgstabsolve_all (cch_psi_all, ccg_psi, etiu(1,ikk,1), dv2(1,1,1), dpsi, &
                             h_diag(1,1,1), npwx, npwq, thresh, ik, lter, conv_root, &
                             anorm, nbnd_occ(ikk) ) 
        !
        IF (.NOT.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
        !
        ! Imaginary Freq -iu  
        !
        conv_root = .true.
        dpsi2(:,:) = (0.D0, 0.D0)
        !
        CALL bicgstabsolve_all (cch_psi_all, ccg_psi, etiu(1,ikk,2), dv2(1,1,2), dpsi2, &
                             h_diag(1,1,2), npwx, npwq, thresh, ik, lter, conv_root, &
                             anorm, nbnd_occ(ikk) )

        IF (.NOT.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                 &              " solve_linter: root not converged ",e10.3)') &
                 &              ik , ibnd, anorm
        !
        ! Only the sum of +iu and -iu solutions is needed
        !
        dpsi(:,:) = dpsi(:,:) + dpsi2(:,:)
        !
        !
        ! Calculate \sum_v [ psi_v(r)^* x d2phi_v(r) ] 
        ! NB. A factor 2 is added to the weight in dpsi_dpsi subroutine
        !
        weight = wk(ikk)/2.D0
        CALL dpsi_dpsi  (summ3(1, current_spin), weight, ik, evc, dpsi, 1)
!        write(stdout,*) 'summ3'
!        write(stdout,*) summ3(1:3,current_spin)
!        WRITE(*,*)
!        WRITE(*,*) 'summ3' , summ3(1:3,current_spin)
        !
        !!!=================================================!!!
        !!!       Solve Linear system for D2psi             !!!
        !!!=================================================!!!
        !
        ! compute the kinetic energy at k
        !
        CALL g2_kin (ikq)
        CALL ch_prec (ik, evq, h_diag, uu)
!        DO ig = 1, npw
!           g2kin (ig) = ( (xk (1,ikk) + g (1, igk(ig)) ) **2 + &
!                          (xk (2,ikk) + g (2, igk(ig)) ) **2 + &
!                          (xk (3,ikk) + g (3, igk(ig)) ) **2 ) * tpiba2
!        ENDDO
!        ! 
!        h_diag(:,:,:) = (0.d0, 0.d0)
!        DO ibnd = 1, nbnd_occ (ikk)
!           DO ig = 1, npw
!               h_diag(ig,ibnd,1) = CMPLX(1.d0, 0.d0, kind=DP) / &
!                   CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik),-uu, kind=DP )
!               h_diag(ig,ibnd,2) = CMPLX(1.d0, 0.d0, kind=DP) / &
!                   CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik), uu, kind=DP )
!           ENDDO
!        ENDDO
        !
        ! compute rhs of linear system for d2psi
        ! input: dpsi_plus, dpsi_minus, psi, dpsi_nl, DV, flag
        ! output: dv2 = rhs of linear system
        !
        dv2(:,:,:) = (0.d0, 0.d0)
        CALL rhs2_dchi_rpax (ikk, ikq, evc, evq, dpsi_plus, dpsi_minus, dv2)
        !
        ! Orthogonalize to valence sates (dpsi is used as workspace in the subroutine)
        ! OUTPUT in dv2
        !
        CALL orthogonalize_uu(dv2(1,1,1), evc, ikk, ikk, dpsi, npw, uu)
        CALL orthogonalize_uu(dv2(1,1,2), evc, ikk, ikk, dpsi, npw, -uu)
        !
        ! Imagianry Freq  +iu
        !
        conv_root = .TRUE.
        thresh = tr2_ph 
        dpsi(:,:) = (0.D0, 0.D0)
        !
        CALL bicgstabsolve_all (cch_psi_all, ccg_psi, etiu(1,ikq,1), dv2(1,1,1), dpsi, &
                             h_diag(1,1,1), npwx, npw, thresh, ik, lter, conv_root, &
                             anorm, nbnd_occ(ikk) ) 
        !
        IF (.NOT.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
        !
        ! Imaginary Ferq -iu  
        !
        conv_root = .TRUE.
        dpsi2(:,:) = (0.D0, 0.D0)
        !
        CALL bicgstabsolve_all (cch_psi_all, ccg_psi, etiu(1,ikq,2), dv2(1,1,2), dpsi2, &
                             h_diag(1,1,2), npwx, npw, thresh, ik, lter, conv_root, &
                             anorm, nbnd_occ(ikk) )
        !
        IF (.NOT.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
        !
        ! Only the sum of +iu and -iu solutions is needed
        !
        dpsi(:,:) = dpsi(:,:) + dpsi2(:,:)
        !
        ! Calculate \sum_v [ d2psi_v(r)^* x psi_v(r) ]
        !
        CALL dpsi_dpsi  (summ4(1, current_spin), weight, ik, dpsi, evq, 1)
!        write(stdout,*) 'summ4' 
!        write(stdout,*) summ4(1:3,current_spin)
        !      
        ! Accumulate over kpoint
        !
!        summ0(1:dfftp%nnr, current_spin,1) = summ0(1:dfftp%nnr,current_spin,1) +  &
!              ( &
!                  + f1*CMPLX(DBLE(summ1(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &
!                  - f2*CMPLX(DBLE(summ2(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &
!                  + f3*CMPLX(DBLE(summ3(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &  
!                  + 1.D0*CMPLX(DBLE(summ4(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &
!              )


        summ0(1:dfftp%nnr, current_spin,1) = summ0(1:dfftp%nnr,current_spin,1) +  &
              ( &
                  + summ1(1:dfftp%nnr, current_spin)  &
                  - summ2(1:dfftp%nnr, current_spin)  &
                  + summ3(1:dfftp%nnr, current_spin)  &  
                  + summ4(1:dfftp%nnr, current_spin)  &
              )
        ! 
        summ03(1:dfftp%nnr, current_spin,1) = summ03(1:dfftp%nnr,current_spin,1) +  &
              (     CMPLX(DBLE(summ3(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  )
        !
        summ012(1:dfftp%nnr, current_spin,1) = summ012(1:dfftp%nnr,current_spin,1) +  &
              ( &
                    CMPLX(DBLE(summ1(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  &
                  - CMPLX(DBLE(summ2(1:dfftp%nnr, current_spin)),0.D0, kind=DP)  )
        ! 
     ENDDO ! on k-points
     !
     IF (doublegrid) THEN
        DO is = 1, nspin
           CALL cinterpolate (summh(1,is,1), summ0(1,is,1), 1)
           CALL cinterpolate (summh3(1,is,1), summ03(1,is,1), 1)
           CALL cinterpolate (summh12(1,is,1), summ012(1,is,1), 1)
        ENDDO
     else
        CALL ZCOPY (npe*nspin*dfftp%nnr, summ0, 1, summh, 1)
        CALL ZCOPY (npe*nspin*dfftp%nnr, summ03, 1, summh3, 1)
        CALL ZCOPY (npe*nspin*dfftp%nnr, summ012, 1, summh12, 1)
     endif
     ! 
     ! if q=0, make sure that charge conservation is guaranteed
     ! 
     IF ( lgamma ) THEN
        psic(:) = summh(:, nspin, npe)
        CALL fwfft ('Dense', psic, dfftp)
        IF ( gstart==2) psic(nl(1)) = (0.d0, 0.d0)
        CALL invfft ('Dense', psic, dfftp)
        summh(:, nspin, npe) = psic(:)
     ENDIF
     !
#if defined __MPI
     !
     !   Reduce the summ0 and summh across pools
     !
     CALL mp_sum ( summ0, inter_pool_comm )
     CALL mp_sum ( summ03, inter_pool_comm )
     CALL mp_sum ( summ012, inter_pool_comm )
     CALL mp_sum ( summh, inter_pool_comm )
     CALL mp_sum ( summh3, inter_pool_comm )
     CALL mp_sum ( summh12, inter_pool_comm )
     !
#endif
     ! 
     ! here we pass summ to summ_vc to used as output
     !
     convt = .TRUE.
     IF ( convt ) THEN
        summ_vc(:,:) = summh(:,:,1)
        summ_vc3(:,:) = summh3(:,:,1)
        summ_vc12(:,:) = summh12(:,:,1)
        GOTO 300
     ENDIF
     !
  300 CONTINUE
  DEALLOCATE ( h_diag)
  DEALLOCATE (dpsi2, etiu)
  DEALLOCATE ( dpsi_sum)
  DEALLOCATE ( aux1)
  DEALLOCATE ( aux2)
  IF (doublegrid) DEALLOCATE (dvscfins)
  DEALLOCATE (dvscfin)
  IF (lmetq0) DEALLOCATE (ldoss)
  IF (lmetq0) DEALLOCATE (ldos)
  DEALLOCATE (dv2)
  DEALLOCATE (summ1)
  DEALLOCATE (summ2)
  DEALLOCATE (summ3)
  DEALLOCATE (summ4)
  DEALLOCATE (summ0)
  DEALLOCATE (summ03)
  DEALLOCATE (summ012)
  DEALLOCATE (summh)
  DEALLOCATE (summh3)
  DEALLOCATE (summh12)
  call stop_clock ('solve_dec_dv')
  RETURN
  !
END SUBROUTINE solve_dec_dv_rpax
