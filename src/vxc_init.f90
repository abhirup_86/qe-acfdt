!
! Copyright (C) 2001-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE vxc_init( rho, rho_core, rhog_core, etx_init, vtx_init, etc_init, vtc_init, vx_init, vc_init )
  !----------------------------------------------------------------------------
  !
  ! ... Exchange-Correlation potential Vxc(r) from n(r)
  ! ... This routine will calculate the initial Vx(r) or Vc(r) from converged n0(r)
  !
  USE kinds,            ONLY : DP
  USE constants,        ONLY : e2, eps8
  USE io_global,        ONLY : stdout
  USE gvect,            ONLY : ngm
  USE lsda_mod,         ONLY : nspin
  USE cell_base,        ONLY : omega
  USE fft_base,         ONLY : dfftp
  USE spin_orb,         ONLY : domag
  USE funct,            ONLY : xc, xc_spin
  USE scf,              ONLY : scf_type
  USE mp_global,        ONLY : intra_pool_comm
  USE mp,               ONLY : mp_sum
  !
  IMPLICIT NONE
  !
  TYPE (scf_type), INTENT(IN) :: rho
  REAL(DP), INTENT(IN) :: rho_core(dfftp%nnr)
    ! the core charge
  COMPLEX(DP), INTENT(IN) :: rhog_core(ngm)
    ! input: the core charge in reciprocal space
  REAL(DP), INTENT(OUT) :: vtx_init, etx_init
  REAL(DP), INTENT(OUT) :: vtc_init, etc_init
  REAL(DP), INTENT(OUT) :: vc_init(dfftp%nnr,nspin), vx_init(dfftp%nnr,nspin)
    ! V_xc potential
    ! integral V_xc * rho
    ! E_xc energy
  !
  ! ... local variables
  !
  REAL(DP) :: v(dfftp%nnr,nspin), vtxc, etxc
  REAL(DP) :: rhox, arhox, zeta, amag, vs, ex, ec, vx(2), vc(2), rhoneg(2)
    ! the total charge in each point
    ! the absolute value of the charge
    ! the absolute value of the charge
    ! local exchange energy
    ! local correlation energy
    ! local exchange potential
    ! local correlation potential
  INTEGER :: ir, ipol
    ! counter on mesh points
    ! counter on nspin
  !
  REAL(DP), PARAMETER :: vanishing_charge = 1.D-10, &
                         vanishing_mag    = 1.D-20
  !
  !
  CALL start_clock( 'v_xc' )
  !
  etxc   = 0.D0
  vtxc   = 0.D0
  etx_init   = 0.D0
  etc_init   = 0.D0
  vtx_init   = 0.D0
  vtc_init   = 0.D0
  v(:,:) = 0.D0
  vc_init(:,:) = 0.D0
  vx_init(:,:) = 0.D0 
  rhoneg = 0.D0
  !
  IF ( nspin == 1 .OR. ( nspin == 4 .AND. .NOT. domag ) ) THEN
     !
     ! ... spin-unpolarized case
     !
!$omp parallel do private( rhox, arhox, ex, ec, vx, vc ), &
!$omp             reduction(+:etxc,vtxc), reduction(-:rhoneg)
     DO ir = 1, dfftp%nnr
        !
        rhox = rho%of_r(ir,1) + rho_core(ir)
        !
        arhox = ABS( rhox )
        !
        IF ( arhox > vanishing_charge ) THEN
           !
           CALL xc( arhox, ex, ec, vx(1), vc(1) )
           !
           v(ir,1) = e2*( vx(1) + vc(1) )
           vc_init(ir,1) = e2* vc(1)
           vx_init(ir,1) = e2* vx(1)
           !
           etxc = etxc + e2*( ex + ec ) * rhox
           !
           vtxc = vtxc + v(ir,1) * rho%of_r(ir,1)
           !  
           etx_init = etx_init + e2* ex * rhox
           etc_init = etc_init + e2* ec * rhox
           !
           vtx_init = vtx_init + vx_init(ir,1) * rho%of_r(ir,1)
           vtc_init = vtc_init + vc_init(ir,1) * rho%of_r(ir,1)
           !
        ENDIF
        !
        IF ( rho%of_r(ir,1) < 0.D0 ) rhoneg(1) = rhoneg(1) - rho%of_r(ir,1)
        !
     END DO
!$omp end parallel do
     !
  ELSE IF ( nspin == 2 ) THEN
     !
     ! ... spin-polarized case
     !
!$omp parallel do private( rhox, arhox, zeta, ex, ec, vx, vc ), &
!$omp             reduction(+:etxc,vtxc), reduction(-:rhoneg)
     DO ir = 1, dfftp%nnr
        !
        rhox = rho%of_r(ir,1) + rho%of_r(ir,2) + rho_core(ir)
        !
        arhox = ABS( rhox )
        !
        IF ( arhox > vanishing_charge ) THEN
           !
           zeta = ( rho%of_r(ir,1) - rho%of_r(ir,2) ) / arhox
           !
           IF ( ABS( zeta ) > 1.D0 ) zeta = SIGN( 1.D0, zeta )
           !
           IF ( rho%of_r(ir,1) < 0.D0 ) rhoneg(1) = rhoneg(1) - rho%of_r(ir,1)
           IF ( rho%of_r(ir,2) < 0.D0 ) rhoneg(2) = rhoneg(2) - rho%of_r(ir,2)
           !
           CALL xc_spin( arhox, zeta, ex, ec, vx(1), vx(2), vc(1), vc(2) )
           !
           v(ir,:) = e2*( vx(:) + vc(:) )
           vc_init(ir,:) = e2* vc(1)
           vx_init(ir,:) = e2* vx(1)
           !
           etxc = etxc + e2*( ex + ec ) * rhox
           !
           vtxc = vtxc + v(ir,1) * rho%of_r(ir,1) + v(ir,2) * rho%of_r(ir,2)
           ! 
           etx_init = etx_init + e2* ex * rhox
           etc_init = etc_init + e2* ec * rhox
           !
           vtx_init = vtx_init + vx_init(ir,1) * rho%of_r(ir,1) + vx_init(ir,2) * rho%of_r(ir,2)
           vtx_init = vtx_init + vc_init(ir,1) * rho%of_r(ir,1) + vc_init(ir,2) * rho%of_r(ir,2)
           !
        END IF
        !
     END DO
!$omp end parallel do
     !
  ELSE IF ( nspin == 4 ) THEN
     !
     ! ... noncolinear case
     !
     DO ir = 1, dfftp%nnr
        !
        amag = SQRT( rho%of_r(ir,2)**2 + rho%of_r(ir,3)**2 + rho%of_r(ir,4)**2 )
        !
        rhox = rho%of_r(ir,1) + rho_core(ir)
        !
        IF ( rho%of_r(ir,1) < 0.D0 )  rhoneg(1) = rhoneg(1) - rho%of_r(ir,1)
        !
        arhox = ABS( rhox )
        !
        IF ( arhox > vanishing_charge ) THEN
           !
           zeta = amag / arhox
           !
           IF ( ABS( zeta ) > 1.D0 ) THEN
              !
              rhoneg(2) = rhoneg(2) + 1.D0 / omega
              !
              zeta = SIGN( 1.D0, zeta )
              !
           END IF
           !
           CALL xc_spin( arhox, zeta, ex, ec, vx(1), vx(2), vc(1), vc(2) )
           !
           vs = 0.5D0*( vx(1) + vc(1) - vx(2) - vc(2) )
           !
           v(ir,1) = e2*( 0.5D0*( vx(1) + vc(1) + vx(2) + vc(2 ) ) )
           vc_init(ir,:) = e2*( 0.5D0*( vc(1) + vc(2 ) ) )
           vx_init(ir,:) = e2*( 0.5D0*( vx(1) + vx(2 ) ) )
           !
           IF ( amag > vanishing_mag ) THEN
              !
              DO ipol = 2, 4
                 !
                 v(ir,ipol) = e2 * vs * rho%of_r(ir,ipol) / amag
                 !
                 vtxc = vtxc + v(ir,ipol) * rho%of_r(ir,ipol)
                 vtx_init = vtx_init + vx_init(ir,ipol) * rho%of_r(ir,ipol)
                 vtc_init = vtc_init + vc_init(ir,ipol) * rho%of_r(ir,ipol)
                 !
              END DO
              !
           END IF
           !
           etxc = etxc + e2*( ex + ec ) * rhox
           etx_init = etx_init + e2* ex * rhox
           etc_init = etc_init + e2* ec * rhox
           vtxc = vtxc + v(ir,1) * rho%of_r(ir,1)
           vtx_init = vtx_init + vx_init(ir,1) * rho%of_r(ir,1)
           vtc_init = vtc_init + vc_init(ir,1) * rho%of_r(ir,1)
           !
        END IF
        !
     END DO
     !
  END IF
  !
  CALL mp_sum(  rhoneg , intra_pool_comm )
  !
  rhoneg(:) = rhoneg(:) * omega / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  IF ( rhoneg(1) > eps8 .OR. rhoneg(2) > eps8 ) &
     WRITE( stdout,'(/,5X,"negative rho (up, down): ",2E10.3)') rhoneg
  !
  ! ... energy terms, local-density contribution
  !
  vtxc = omega * vtxc / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  vtx_init = omega * vtx_init / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  vtc_init = omega * vtc_init / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  etxc = omega * etxc / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  etx_init = omega * etx_init / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  etc_init = omega * etc_init / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  ! ... add gradient corrections (if any)
  !
  CALL gradcorr_init( rho%of_r, rho%of_g, rho_core, rhog_core, etxc, vtxc, v , & 
                      etx_init, etc_init, vtx_init, vtc_init, vx_init, vc_init )
  !
  !if (dft_is_vdW()) then
  !   
  !   CALL xc_vdW_DF(rho%of_r, rho_core, etxc, vtxc, v)
  !
  !end if

  !
  CALL mp_sum(  vtxc, intra_pool_comm )
  CALL mp_sum(  vtx_init, intra_pool_comm )
  CALL mp_sum(  vtc_init, intra_pool_comm )
  CALL mp_sum(  etxc, intra_pool_comm )
  CALL mp_sum(  etx_init, intra_pool_comm )
  CALL mp_sum(  etc_init, intra_pool_comm )
  !
  CALL stop_clock( 'v_xc' )
  !
  RETURN
  !
END SUBROUTINE vxc_init
