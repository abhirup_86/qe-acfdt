!
! Copyright (C) 2001-2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE xc_rpa_plus ( )
  !----------------------------------------------------------------------------
  !
  !    This routine is the main driver for calculations of exchange and
  !    correlation energy with a LDA or GGA functional and local density
  !    correction for short-range correlation in RPA correlation energy.
  !
  !
  USE io_global,     ONLY : stdout
  USE control_acfdt, ONLY : etx, etc, ec_sr
  USE scf,           ONLY : rho, rho_core, rhog_core
  USE ener,          ONLY : etxc, etxcc
  !
  IMPLICIT NONE
  !
  !
  WRITE( stdout, '(/,5x,"Some info on exchange and correlation energy")')
  !
  CALL e_xc( rho, rho_core, rhog_core )
  WRITE( stdout, '(/,7x,"xc energy (= ex + ec)     =",F15.8," Ry")' ) (etxc - etxcc)
  WRITE( stdout, '(  7x,"               !ex        =",F15.8," Ry")' ) etx 
  WRITE( stdout, '(  7x,"               !ec        =",F15.8," Ry")' ) etc
  !
  CALL ec_sr_lda ( )
  WRITE( stdout, '(/,7x,"short-range corr. energy  =",F15.8," Ry")' ) ec_sr
  !
  RETURN
  !
END SUBROUTINE xc_rpa_plus


SUBROUTINE e_xc( rho, rho_core, rhog_core )
  !----------------------------------------------------------------------------
  !
  ! ... Exchange and correlation energies from n(r)
  !
  USE kinds,            ONLY : DP
  USE constants,        ONLY : e2, eps8
  USE io_global,        ONLY : stdout
  USE gvect,            ONLY : ngm
  USE lsda_mod,         ONLY : nspin
  USE cell_base,        ONLY : omega
  USE funct,            ONLY : xc, xc_spin
  USE scf,              ONLY : scf_type
  USE mp_global,        ONLY : intra_pool_comm
  USE mp,               ONLY : mp_sum
  USE fft_base,         ONLY : dfftp

  USE control_acfdt,    ONLY : etx, etc
  USE ener,             ONLY : etxc
  !
  IMPLICIT NONE
  !
  TYPE (scf_type), INTENT(IN) :: rho
  REAL(DP), INTENT(IN) :: rho_core(dfftp%nnr)
    ! the core charge
  COMPLEX(DP), INTENT(IN) :: rhog_core(ngm)
    ! input: the core charge in reciprocal space
  !
  ! ... local variables
  !
  REAL(DP) :: rhox, arhox, zeta, amag, vs, ex, ec, vx(2), vc(2), rhoneg(2)
    ! the total charge in each point
    ! the absolute value of the charge
    ! the absolute value of the charge
    ! local exchange energy
    ! local correlation energy
    ! local exchange potential
    ! local correlation potential
  INTEGER :: ir, ipol
    ! counter on mesh points
    ! counter on nspin
  !
  REAL(DP), PARAMETER :: vanishing_charge = 1.D-10
  !
  !
  CALL start_clock( 'e_xc' )
  !
  etx    = 0.D0
  etc    = 0.D0
  etxc   = 0.D0
  rhoneg = 0.D0
  !
  IF ( nspin == 1 ) THEN
     !
     ! ... spin-unpolarized case
     !
     DO ir = 1, dfftp%nnr
        !
        rhox = rho%of_r(ir,1) + rho_core(ir)
        arhox = ABS( rhox )
        !
        IF ( arhox > vanishing_charge ) THEN
           !
           CALL xc( arhox, ex, ec, vx(1), vc(1) )
           etx  = etx  + e2*( ex      ) * rhox
           etc  = etc  + e2*(      ec ) * rhox
           etxc = etxc + e2*( ex + ec ) * rhox
           !
        ENDIF
        !
        IF ( rho%of_r(ir,1) < 0.D0 ) rhoneg(1) = rhoneg(1) - rho%of_r(ir,1)
        !
     END DO
     !
  ELSE
     !
     CALL errore('e_xc','ONLY implemented for spin-unpolarized systems',1) 
     !
  ENDIF
  !
  CALL mp_sum(  rhoneg , intra_pool_comm )
  !
  rhoneg(:) = rhoneg(:) * omega / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  IF ( rhoneg(1) > eps8 .OR. rhoneg(2) > eps8 ) &
     WRITE( stdout,'(/,5X,"negative rho (up, down): ",2E10.3)') rhoneg
  !
  ! ... energy terms, local-density contribution
  !
  etx  = omega * etx  / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  etc  = omega * etc  / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  etxc = omega * etxc / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  ! ... add gradient corrections (if any)
  !
  CALL e_gradcorr( rho%of_r, rho%of_g, rho_core, rhog_core )
  CALL mp_sum(  etx  , intra_pool_comm )
  CALL mp_sum(  etc  , intra_pool_comm )
  CALL mp_sum(  etxc , intra_pool_comm )
  !
  CALL stop_clock( 'e_xc' )
  !
  RETURN
  !
END SUBROUTINE e_xc
!
!----------------------------------------------------------------------------
SUBROUTINE e_gradcorr( rho, rhog, rho_core, rhog_core )
  !----------------------------------------------------------------------------
  !
  ! This routine is adapted from the one in PW/gradcorr.f90
  ! Only work for spin-unpolarized systems
  !
  USE constants,            ONLY : e2
  USE kinds,                ONLY : DP
  USE gvect,                ONLY : nl, ngm, g
  USE lsda_mod,             ONLY : nspin
  USE cell_base,            ONLY : omega, alat
  USE funct,                ONLY : gcxc, gcx_spin, gcc_spin, &
                                   gcc_spin_more, dft_is_gradient, get_igcc
  USE noncollin_module,     ONLY : ux
  USE wavefunctions_module, ONLY : psic
  USE fft_base,             ONLY : dfftp
  USE fft_interfaces,       ONLY : fwfft

  USE ener,                 ONLY : etxc
  USE control_acfdt,        ONLY : etx, etc
  ! NB: part of contribution to etx, etc were calculated before in e_xc
  !     here we just add the gradient correction, so it shound not be reinitialized
  !
  IMPLICIT NONE
  !
  REAL(DP),    INTENT(IN)    :: rho(dfftp%nnr,nspin), rho_core(dfftp%nnr)
  COMPLEX(DP), INTENT(IN)    :: rhog(ngm,nspin), rhog_core(ngm)
  !
  INTEGER :: k, ipol, is, nspin0, ir, jpol
  !
  REAL(DP),    ALLOCATABLE :: grho(:,:,:), h(:,:,:), dh(:)
  REAL(DP),    ALLOCATABLE :: rhoout(:,:), segni(:), vgg(:,:), vsave(:,:)
  REAL(DP),    ALLOCATABLE :: gmag(:,:,:)

  COMPLEX(DP), ALLOCATABLE :: rhogsum(:,:)
  !
  LOGICAL  :: igcc_is_lyp
  REAL(DP) :: grho2(2), sx, sc, v1x, v2x, v1c, v2c, &
              v1xup, v1xdw, v2xup, v2xdw, v1cup, v1cdw , &
              etxcgc, vtxcgc, segno, arho, fac, zeta, rh, grh2, amag, &
              etxgc , etcgc
  REAL(DP) :: v2cup, v2cdw,  v2cud, rup, rdw, &
              grhoup, grhodw, grhoud, grup, grdw, seg
  !
  REAL(DP), PARAMETER :: epsr = 1.D-6, epsg = 1.D-10
  !
  !
  IF ( .NOT. dft_is_gradient() ) RETURN

  igcc_is_lyp = (get_igcc() == 3 .or. get_igcc() == 7)
  !
  etxgc  = 0.D0
  etcgc  = 0.D0
  etxcgc = 0.D0
  vtxcgc = 0.D0
  !
  nspin0=nspin
  fac = 1.D0 / DBLE( nspin0 )
  !
  ALLOCATE(    h( 3, dfftp%nnr, nspin0) )
  ALLOCATE( grho( 3, dfftp%nnr, nspin0) )
  ALLOCATE( rhoout( dfftp%nnr, nspin0) )
  !
  ALLOCATE( rhogsum( ngm, nspin0 ) )
  !
  ! ... calculate the gradient of rho + rho_core in real space
  !
  rhoout(:,1:nspin0)  = rho(:,1:nspin0)
  rhogsum(:,1:nspin0) = rhog(:,1:nspin0)
  !
  DO is = 1, nspin0
     !
     rhoout(:,is)  = fac * rho_core(:)  + rhoout(:,is)
     rhogsum(:,is) = fac * rhog_core(:) + rhogsum(:,is)
     !
     CALL gradrho( dfftp%nnr, rhogsum(1,is), ngm, g, nl, grho(1,1,is) )
     !
  END DO
  !
  DEALLOCATE( rhogsum )
  !
  IF ( nspin0 == 1 ) THEN
     !
     ! ... This is the spin-unpolarised case
     !
     DO k = 1, dfftp%nnr
        !
        arho = ABS( rhoout(k,1) )
        !
        IF ( arho > epsr ) THEN
           !
           grho2(1) = grho(1,k,1)**2 + grho(2,k,1)**2 + grho(3,k,1)**2
           !
           IF ( grho2(1) > epsg ) THEN
              !
              segno = SIGN( 1.D0, rhoout(k,1) )
              !
              CALL gcxc( arho, grho2(1), sx, sc, v1x, v2x, v1c, v2c )
              !
              ! ... first term of the gradient correction : D(rho*Exc)/D(rho)
              !
!              v(k,1) = v(k,1) + e2 * ( v1x + v1c )
              !
              ! ... h contains :
              !
              ! ...    D(rho*Exc) / D(|grad rho|) * (grad rho) / |grad rho|
              !
!              h(:,k,1) = e2 * ( v2x + v2c ) * grho(:,k,1)
              !
!              vtxcgc = vtxcgc+e2*( v1x + v1c ) * ( rhoout(k,1) - rho_core(k) )
              etxgc  = etxgc +e2*( sx      ) * segno
              etcgc  = etcgc +e2*(      sc ) * segno
              etxcgc = etxcgc+e2*( sx + sc ) * segno
              !
           ELSE
              h(:,k,1)=0.D0
           END IF
           !
        ELSE
           !
           h(:,k,1) = 0.D0
           !
        END IF
        !
     END DO
     !
  ELSE
     !
     CALL errore('e_gradcorr','ONLY implemented for spin-unpolarized systems',1) 
     !
  ENDIF
  !
  DO is = 1, nspin0
     !
     rhoout(:,is) = rhoout(:,is) - fac * rho_core(:)
     !
  END DO
  !
  DEALLOCATE( grho )
  !
!  ALLOCATE( dh( dfftp%nnr ) )    
  !
  ! ... second term of the gradient correction :
  ! ... \sum_alpha (D / D r_alpha) ( D(rho*Exc)/D(grad_alpha rho) )
  !
!  DO is = 1, nspin0
!     CALL grad_dot( dfftp%nnr, h(1,1,is), ngm, g, nl, alat, dh )
!     vtxcgc = vtxcgc - SUM( dh(:) * rhoout(:,is) )
!  END DO
  !
  etx  = etx  + omega * etxgc  / ( dfftp%nr1 * dfftp%nr2 * dfftp%nr3 )
  etc  = etc  + omega * etcgc  / ( dfftp%nr1 * dfftp%nr2 * dfftp%nr3 )
  etxc = etxc + omega * etxcgc / ( dfftp%nr1 * dfftp%nr2 * dfftp%nr3 )
  !
!  DEALLOCATE( dh )
  DEALLOCATE( h )
  DEALLOCATE( rhoout )
  !
  RETURN
  !
END SUBROUTINE e_gradcorr
!
!----------------------------------------------------------------------------
SUBROUTINE ec_sr_lda( )
  !----------------------------------------------------------------------------
  !
  ! This routine uses the output of scf calculation to calculate short-range 
  ! correlation energy in LDA no matter what functional was used in scf.
  ! Parametrization of LDA is the one proposed by Perdew and Wang in 1992.
  ! (for definition of short-range correlation energy see PRB, 59, 10461 (1999) )
  !
  USE kinds,            ONLY : DP
  USE constants,        ONLY : e2, eps8
  USE io_global,        ONLY : stdout
  USE lsda_mod,         ONLY : nspin
  USE cell_base,        ONLY : omega
  USE fft_base,         ONLY : dfftp
  USE funct,            ONLY : xc, xc_spin
  USE scf,              ONLY : rho, rho_core, rhog_core
  USE mp_global,        ONLY : intra_pool_comm
  USE mp,               ONLY : mp_sum
  !USE ener,             ONLY : etx, etc

  USE control_acfdt,    ONLY : ec_sr
  !
  IMPLICIT NONE
  !
  ! ... local variables
  !
  REAL(DP) :: rhox, arhox, zeta, amag, vs, ex, vx(2), ec, vc(2), rhoneg(2)
    ! the total charge in each point
    ! the absolute value of the charge
    ! the absolute value of the charge
    ! local exchange energy
    ! local correlation energy
    ! local exchange potential
    ! local correlation potential
  REAL(DP) :: etc_lda, etc_rpa
  INTEGER :: ir, ipol
    ! counter on mesh points
    ! counter on nspin
  !
  REAL(DP), PARAMETER :: vanishing_charge = 1.D-10, &
                         vanishing_mag    = 1.D-20
  real(DP), parameter :: small = 1.d-10,  third = 1.d0 / 3.d0, &
                         pi34 = 0.6203504908994d0  ! pi34=(3/4pi)^(1/3)
  real(DP) :: rs
  !
  !
  CALL start_clock( 'ec_sr_lda' )
  !
  etc_lda = 0.D0
  etc_rpa = 0.D0
  rhoneg  = 0.D0
  !
  IF ( nspin == 1 ) THEN
     !
     ! ... spin-unpolarized case
     !
     DO ir = 1, dfftp%nnr
        !
        rhox = rho%of_r(ir,1) + rho_core(ir)
        !
        arhox = ABS( rhox )
        !
        IF ( arhox > vanishing_charge ) THEN
           !
           rs = pi34 / arhox**third ! rs as in the theory of metals: rs=(3/(4pi rho))^(1/3)
           !
           call pw(rs, 1, ec, vc(1))
           etc_lda = etc_lda + e2*ec*rhox
           !
           call pw_rpa(rs, ec)
           etc_rpa = etc_rpa + e2*ec*rhox
           !
        ENDIF
        !
        IF ( rho%of_r(ir,1) < 0.D0 ) rhoneg(1) = rhoneg(1) - rho%of_r(ir,1)
        !
     END DO
     !
  ELSE
     !
     CALL errore('ec_sr_lda','ONLY implemented for spin-unpolarized systems',1) 
     !
  ENDIF
  !
  CALL mp_sum(  rhoneg , intra_pool_comm )
  !
  rhoneg(:) = rhoneg(:) * omega / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  IF ( rhoneg(1) > eps8 .OR. rhoneg(2) > eps8 ) &
     WRITE( stdout,'(/,5X,"negative rho (up, down): ",2E10.3)') rhoneg
  !
  ! ... energy terms, local-density contribution
  !
  ec_sr = etc_lda - etc_rpa
  ec_sr = omega * ec_sr / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
  !
  CALL mp_sum( ec_sr, intra_pool_comm )
  !
  CALL stop_clock( 'ec_sr_lda' )
  !
  RETURN
  !
END SUBROUTINE ec_sr_lda
!
!-----------------------------------------------------------------------
subroutine pw_rpa (rs, ec)
  !-----------------------------------------------------------------------
  !     RPA level of Ec in J.P. Perdew and Y. Wang, PRB 45, 13244 (1992)
  !     used for the so-called RPA+ scheme, see Perdew et. al, PRB, 59, 10461 (1999)
  !
  USE kinds
  implicit none
  real(DP) :: rs, ec
  !
  real(DP) :: a, b1, b2, c0, c1, c2, c3, d0, d1
  parameter (a = 0.031091d0, b1 = 5.1486d0, b2 = 1.6483d0, c0 = a, &
       c1 = 0.070823d0, c2 = 0.00256d0, c3 = 0.00936d0, d0 = 0.4001d0, &
       d1 = 0.4590d0)
  real(DP) :: lnrs, rs12, rs32, rs74, om, dom, olog
  real(DP) :: a1 , b3 , b4
  data a1 / 0.082477d0 /, b3 / 0.23647d0 /, b4 / 0.20614d0 /
  !
  ! interpolation formula
  rs12 = sqrt (rs)
  rs32 = rs * rs12
  rs74 = rs * rs12 * sqrt(rs12)
  om = 2.d0 * a * (b1 * rs12 + b2 * rs + b3 * rs32 + b4 * rs74)
!  dom = 2.d0 * a * (0.5d0 * b1 * rs12 + b2 * rs + 1.5d0 * b3 ( &
!       iflag) * rs32 + 2.d0 * b4 (iflag) * rs2)
  olog = log (1.d0 + 1.0d0 / om)
  ec = - 2.d0 * a * (1.d0 + a1 * rs) * olog
!  vc = - 2.d0 * a * (1.d0 + 2.d0 / 3.d0 * a1 (iflag) * rs) &
!       * olog - 2.d0 / 3.d0 * a * (1.d0 + a1 (iflag) * rs) * dom / &
!       (om * (om + 1.d0) )
  !
  return
end subroutine pw_rpa

